package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	@Mock
	private AuthorServiceImpl authorService;
	
	@Test
	public void addAuthorTest() throws ServiceException {
		AuthorTO author = new AuthorTO();
		Long expectedId = 1L;
		Mockito.when(authorService.addAuthor(author)).thenReturn(expectedId);
		Long actualId = authorService.addAuthor(author);
		Mockito.verify(authorService, Mockito.times(1)).addAuthor(author);
		assertEquals(actualId,expectedId);
	}
	
	@Test
	public void expireAuthorByIdTest() throws ServiceException {
		Long authorId = 1L;
		authorService.expireAuthorById(authorId);
		Mockito.verify(authorService, Mockito.times(1)).expireAuthorById(authorId);
	}
	
	@Test
	public void attachAuthorToNewsByIdTest() throws ServiceException {
		authorService.attachAuthorToNewsById(1L, 1L);
		Mockito.verify(authorService, Mockito.times(1)).attachAuthorToNewsById(1L, 1L);
	}
	
	@Test
	public void detachAuthorFromNewsByNewsIdTest() throws ServiceException {
		authorService.expireAuthorById(1L);
		Mockito.verify(authorService, Mockito.times(1)).expireAuthorById(1L);
	}
	
	@Test
	public void getAuthorByNewsIdTest() throws ServiceException {
		AuthorTO expectedAuthor = new AuthorTO();
		Mockito.when(authorService.getAuthorByNewsId(1L)).thenReturn(expectedAuthor);
		AuthorTO actualAuthor = authorService.getAuthorByNewsId(1L);
		Mockito.verify(authorService, Mockito.times(1)).getAuthorByNewsId(1L);
		assertEquals(expectedAuthor,actualAuthor);
	}

}
