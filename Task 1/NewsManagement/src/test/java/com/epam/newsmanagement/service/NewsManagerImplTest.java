package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import com.epam.newsmanagement.service.impl.TagServiceImpl;
import com.epam.newsmanagement.util.SearchCriteria;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagerImplTest {
	
	@Mock
	private NewsServiceImpl newsService;
	
	@Mock
	private AuthorServiceImpl authorService;
	
	@Mock
	private CommentServiceImpl commentService;
	
	@Mock
	private TagServiceImpl tagService;
	
	@InjectMocks
	private NewsManagerImpl newsManager;
	
	@Test(expected=ServiceException.class)
	public void addNewsTest() throws ServiceException {
		NewsVO news = new NewsVO();
		news.setTagList(new ArrayList<TagTO>());
		news.setNewsTO(new NewsTO());
		news.setAuthor(new AuthorTO());
		Mockito.doThrow(ServiceException.class).when(tagService).attachTagListToNews(Mockito.anyList(), Mockito.anyLong());
		Long expectedNewsId = newsManager.addNews(news);
		Mockito.verify(newsService, Mockito.times(1)).addNews(news.getNewsTO());
		Mockito.verify(tagService, Mockito.times(1)).attachTagListToNews(Mockito.anyList(), Mockito.eq(expectedNewsId));
		Mockito.verify(authorService, Mockito.never()).attachAuthorToNewsById(news.getAuthor().getAuthorId(), expectedNewsId);
	}
	
	@Test
	public void editNewsTest() throws ServiceException {
		NewsTO news = new NewsTO();
		newsManager.editNews(news);
		Mockito.verify(newsService, Mockito.times(1)).editNews(news);
	}
	
	@Test(expected=ServiceException.class)
	public void deleteNewsTest() throws ServiceException{
		Mockito.doThrow(new ServiceException("")).when(commentService).deleteAllNewsCommentsByNewsId(1L);
		newsManager.deleteNews(1L);
		Mockito.verify(tagService, Mockito.times(1)).detachAllTagsFromNewsById(1L);
		Mockito.verify(commentService, Mockito.times(1)).deleteAllNewsCommentsByNewsId(1L);
		Mockito.verify(authorService, Mockito.never()).detachAuthorFromNewsByNewsId(1L);
		Mockito.verify(newsService, Mockito.never()).deleteNewsById(1L);
	}
	
	@Test
	public void getNewsTest() throws ServiceException {
		int expectedSize = (newsManager.getNews().size());
		Mockito.verify(newsService, Mockito.times(1)).getNews();
		Mockito.verify(authorService, Mockito.times(expectedSize)).getAuthorByNewsId(Mockito.anyLong());
		Mockito.verify(commentService, Mockito.times(expectedSize)).getCommentsByNewsId(Mockito.anyLong());
		Mockito.verify(tagService, Mockito.times(expectedSize)).getTagListByNewsId(Mockito.anyLong());
	}
	
	@Test
	public void getNewsByIdTest() throws ServiceException {
		Long expectedId = 1L;
		NewsTO expectedNews = new NewsTO();
		expectedNews.setNewsId(expectedId);
		Mockito.when(newsService.getNewsById(expectedId)).thenReturn(expectedNews);
		newsManager.getNewsById(expectedId);
		Mockito.verify(newsService, Mockito.times(1)).getNewsById(expectedId);
		Mockito.verify(authorService, Mockito.times(1)).getAuthorByNewsId(expectedId);
		Mockito.verify(commentService, Mockito.times(1)).getCommentsByNewsId(expectedId);
		Mockito.verify(tagService, Mockito.times(1)).getTagListByNewsId(expectedId);
	}
	
	@Test
	public void countNewsTest() throws ServiceException {
		newsManager.countNews();
		Mockito.verify(newsService, Mockito.times(1)).countNews();
	}
	
	@Test
	public void addAuthorTest() throws ServiceException {
		AuthorTO author = new AuthorTO();
		newsManager.addAuthor(author);
		Mockito.verify(authorService, Mockito.times(1)).addAuthor(author);
	}
	
	@Test
	public void expireAuthorByIdTest() throws ServiceException {
		newsManager.expireAuthorById(1L);
		Mockito.verify(authorService, Mockito.times(1)).expireAuthorById(1L);
	}
	
	@Test
	public void addCommentTest() throws ServiceException {
		CommentTO comment = new CommentTO();
		newsManager.addComment(comment, 1L);
		Mockito.verify(commentService, Mockito.times(1)).addComment(comment);
	}
	
	@Test
	public void deleteCommentTest() throws ServiceException {
		newsManager.deleteComment(1L);
		Mockito.verify(commentService, Mockito.times(1)).deleteCommentById(1L);
	}
	
	@Test
	public void editCommentTest() throws ServiceException {
		CommentTO comment = new CommentTO();
		newsManager.editComment(comment);
		Mockito.verify(commentService, Mockito.times(1)).editComment(comment);
	}
	
	@Test
	public void addTagTest() throws ServiceException {
		TagTO tag = new TagTO();
		newsManager.addTag(tag);
		Mockito.verify(tagService, Mockito.times(1)).addTag(tag);
	}
	
	@Test
	public void detachTagFromNewsByIdTest() throws ServiceException {
		newsManager.detachTagFromNewsById(1L, 2L);
		Mockito.verify(tagService, Mockito.times(1)).detachTagFromNewsById(1L, 2L);;
	}
	
	@Test
	public void searchNewsTest() throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		List<Long> idTagList = new ArrayList<Long>(Arrays.asList(1L,2L));
		criteria.setTagIdList(idTagList);
		criteria.setAuthorId(1L);
		newsManager.searchNews(criteria);
		Mockito.verify(newsService, Mockito.times(1)).searchNews(criteria);
	}
}
