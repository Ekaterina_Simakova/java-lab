package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.AuthorDaoImpl;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("authorDaoDataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class AuthorDaoImplTest {

	@Autowired
	@Qualifier("testDataSource")
	private BasicDataSource dataSource;

	@Autowired
	private AuthorDaoImpl authorDao;
	
	@Before
	public void init() {
		authorDao.setDataSource(dataSource);
	}

	@Test
	public void addAuthorTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorName("testAuthor");
		Long authorId = authorDao.addAuthor(expectedAuthor);
		expectedAuthor.setAuthorId(authorId);
		AuthorTO actualAuthor = authorDao.getAuthorById(authorId);
		assertEquals(expectedAuthor, actualAuthor);
	}

	@Test
	public void editAuthorTest() throws DaoException {
		AuthorTO expectedAuthor = authorDao.getAuthorById(1L);
		expectedAuthor.setAuthorName("editName");
		authorDao.editAuthor(expectedAuthor);
		AuthorTO actualAuthor = authorDao.getAuthorById(expectedAuthor.getAuthorId());
		assertEquals(expectedAuthor, actualAuthor);
	}

	@Test
	public void getAuthorByIdTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("getId");
		AuthorTO actualAuthor = authorDao.getAuthorById(expectedAuthor.getAuthorId());
		assertEquals(expectedAuthor, actualAuthor);
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED,value = "expected/author/linkNewsAuthorExpected.xml")
	public void attachAuthorToNewsByIdTest() throws DaoException {
		authorDao.attachAuthorToNewsById(1L, 1L);
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "expected/author/unlinkNewsAuthorExpected.xml")
	public void detachAuthorFromNewsByIdTest() throws DaoException {
		authorDao.detachAuthorFromNewsById(2L);
	}

	@Test
	public void getAuthorByNewsIdTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("getId");
		AuthorTO actualAuthor = authorDao.getAuthorByNewsId(2L);
		assertEquals(expectedAuthor, actualAuthor);
	}

}
