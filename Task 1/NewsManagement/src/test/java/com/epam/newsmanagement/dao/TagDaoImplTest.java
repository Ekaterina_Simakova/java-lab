package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.TagDaoImpl;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("tagDaoDataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class TagDaoImplTest {
	
	@Autowired
	@Qualifier("testDataSource")
	private BasicDataSource dataSource;

	@Autowired
	private TagDaoImpl tagDao;
	
	@Before
	public void init() {
		tagDao.setDataSource(dataSource);
	}

	@Test
	public void addTagTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagName("tag");
		expectedTag.setTagId(tagDao.addTag(expectedTag));
		TagTO actualTag = tagDao.getTagById(expectedTag.getTagId());
		assertEquals(expectedTag,actualTag);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/tag/deleteTagExpected.xml")
	public void deleteTagByIdTest() throws DaoException {
		tagDao.deleteTagById(2L);
	}
	
	@Test
	public void editTagTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagId(1L);
		expectedTag.setTagName("editTag");
		tagDao.editTag(expectedTag);
		TagTO actualTag = tagDao.getTagById(expectedTag.getTagId());
		assertEquals(expectedTag,actualTag);
	}
	
	@Test
	public void getTagByIdTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagId(1L);
		expectedTag.setTagName("firstTag");
		TagTO actualTag = tagDao.getTagById(1L);
		assertEquals(expectedTag,actualTag);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED,value = "expected/tag/linkTagNewsExpected.xml")
	public void attachTagToNewsById() throws DaoException {
		tagDao.attachTagToNewsById(2L, 1L);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/tag/unlinkTagNewsExpected.xml")
	public void detachTagFromNewsByIdTest() throws DaoException {
		tagDao.detachTagFromNewsById(1L, 1L);
	}
	
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "expected/tag/linkTagNewsListExpected.xml")
	public void attachTagListToNewsByIdTest() throws DaoException {
		List<Long> idTagList = new ArrayList<Long>(Arrays.asList(2L,4L));
		tagDao.attachTagListToNewsById(idTagList, 1L);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "expected/tag/unlinkTagNewsListExpected.xml")
	public void detachAllTagsFromNews() throws DaoException {
		tagDao.detachAllTagsFromNews(1L);
	}

}
