package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.NewsDaoImpl;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("newsDaoDataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class NewsDaoImplTest {
	
	@Autowired
	@Qualifier("testDataSource")
	private BasicDataSource dataSource;

	@Autowired
	private NewsDaoImpl newsDao;
	
	@Before
	public void init() {
		newsDao.setDataSource(dataSource);
	}

	@Test
	public void addNewsTest() throws DaoException {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setTitle("title");
		expectedNews.setShortText("shortText");
		expectedNews.setFullText("fullText");
		expectedNews.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		expectedNews.setModificationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		expectedNews.setNewsId(newsDao.addNews(expectedNews));
		NewsTO actualNews = newsDao.getNewsById(expectedNews.getNewsId());
		assertEquals(expectedNews,actualNews);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/news/deleteNewsExpected.xml")
	public void deleteNewsByIdTest() throws DaoException {
		newsDao.deleteNewsById(1L);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/news/editNewsExpected.xml")
	public void editNewsTest() throws DaoException {
		NewsTO news = new NewsTO();
		news.setNewsId(1L);
		news.setTitle("editTitle");
		news.setShortText("editShort");
		news.setFullText("editFull");
		news.setCreationDate(Timestamp.valueOf("2011-02-15 19:00:00"));
		news.setModificationDate(Timestamp.valueOf("2011-02-15 19:00:00"));
		newsDao.editNews(news);
	}
	
	@Test
	public void searchNews() throws DaoException {
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthorId(1L);
		criteria.setTagIdList(new ArrayList<Long>(Arrays.asList(1L)));
		List<Long> expectedNewsId = new ArrayList<Long>(Arrays.asList(2L));
		List<Long> actualNewsId = newsDao.searchNews(criteria);
		assertEquals(expectedNewsId,actualNewsId);
	}
	
	@Test
	public void getNewsListTest() throws DaoException {
		List<NewsTO> expectedList = new ArrayList<NewsTO>();
		NewsTO news1 = new NewsTO();
		news1.setNewsId(2L);
		news1.setTitle("secondNews");
		news1.setShortText("short");
		news1.setFullText("full");
		news1.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		news1.setModificationDate(Timestamp.valueOf("2011-02-15 19:00:00"));
		expectedList.add(news1);
		NewsTO news2 = new NewsTO();
		news2.setNewsId(3L);
		news2.setTitle("thirdNews");
		news2.setShortText("short");
		news2.setFullText("full");
		news2.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		news2.setModificationDate(Timestamp.valueOf("2010-02-20 19:00:00"));
		expectedList.add(news2);
		NewsTO news3 = new NewsTO();
		news3.setNewsId(1L);
		news3.setTitle("firstNews");
		news3.setShortText("short");
		news3.setFullText("full");
		news3.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		news3.setModificationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		expectedList.add(news3);

		List<NewsTO> actualList = newsDao.getNewsList();
		assertEquals(expectedList,actualList);
	}
	
	@Test
	public void getNewsByIdTest() throws DaoException {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setNewsId(1L);
		expectedNews.setTitle("firstNews");
		expectedNews.setShortText("short");
		expectedNews.setFullText("full");
		expectedNews.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		expectedNews.setModificationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		NewsTO actualNews = newsDao.getNewsById(1L);
		assertEquals(expectedNews,actualNews);
	}
	
	@Test
	public void countNewsTest() throws DaoException {
		Long expectedCount = 3L;
		Long actualCount = newsDao.countNews();
		assertEquals(expectedCount,actualCount);
	}
	
	@Test
	public void getNewsListByIdTest() throws DaoException {
		List<NewsTO> expectedList = new ArrayList<NewsTO>();
		NewsTO news1 = new NewsTO();
		news1.setNewsId(2L);
		news1.setTitle("secondNews");
		news1.setShortText("short");
		news1.setFullText("full");
		news1.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		news1.setModificationDate(Timestamp.valueOf("2011-02-15 19:00:00"));
		expectedList.add(news1);
		NewsTO news2 = new NewsTO();
		news2.setNewsId(1L);
		news2.setTitle("firstNews");
		news2.setShortText("short");
		news2.setFullText("full");
		news2.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		news2.setModificationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		expectedList.add(news2);
		List<NewsTO> actualList = newsDao.getNewsListById(new ArrayList<Long>(Arrays.asList(1L,2L)));
		assertTrue(actualList.containsAll(expectedList) && expectedList.containsAll(actualList));
	}

}
