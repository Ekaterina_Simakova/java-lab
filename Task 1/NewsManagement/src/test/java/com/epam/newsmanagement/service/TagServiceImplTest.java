package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	@Mock
	private TagServiceImpl tagService;

	@Test
	public void addTagTest() throws ServiceException {
		TagTO tag = new TagTO();
		Long expectedId = 1L;
		Mockito.when(tagService.addTag(tag)).thenReturn(expectedId);
		Long actualId = tagService.addTag(tag);
		Mockito.verify(tagService, Mockito.times(1)).addTag(tag);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void editTagTest() throws ServiceException {
		TagTO tag = new TagTO();
		tagService.editTag(tag);
		Mockito.verify(tagService, Mockito.times(1)).editTag(tag);
	}
	
	@Test
	public void getTagListByNewsIdTest() throws ServiceException {
		List<TagTO> expectedList = new ArrayList<TagTO>();
		Mockito.when(tagService.getTagListByNewsId(1L)).thenReturn(expectedList);
		List<TagTO> actualList = tagService.getTagListByNewsId(1L);
		Mockito.verify(tagService, Mockito.times(1)).getTagListByNewsId(1L);
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void attachTagToNewsByIdTest() throws ServiceException {
		tagService.attachTagToNewsById(1L, 1L);
		Mockito.verify(tagService, Mockito.times(1)).attachTagToNewsById(1L, 1L);
	}
	
	@Test
	public void attachTagListToNews() throws ServiceException {
		List<Long> tagList = new ArrayList<Long>();
		tagService.attachTagListToNews(tagList, 1L);
		Mockito.verify(tagService, Mockito.times(1)).attachTagListToNews(tagList, 1L);
	}
	
	@Test
	public void detachAllTagsFromNewsByIdTest() throws ServiceException {
		tagService.detachAllTagsFromNewsById(1L);
		Mockito.verify(tagService, Mockito.times(1)).detachAllTagsFromNewsById(1L);
	}
	
	@Test
	public void detachTagFromNewsByIdTest() throws ServiceException {
		tagService.detachTagFromNewsById(1L, 1L);
		Mockito.verify(tagService, Mockito.times(1)).detachTagFromNewsById(1L, 1L);
	}
	
}
