package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import com.epam.newsmanagement.util.SearchCriteria;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {
	
	@Mock
	private NewsServiceImpl newsService;

	@Test
	public void addNewsTest() throws ServiceException {
		Long expectedId = 1L;
		NewsTO news = new NewsTO();
		Mockito.when(newsService.addNews(news)).thenReturn(expectedId);
		Long actualId = newsService.addNews(news);
		Mockito.verify(newsService, Mockito.times(1)).addNews(news);
		assertEquals(expectedId,actualId);
	}
	
	@Test
	public void editNewsTest() throws ServiceException {
		NewsTO news = new NewsTO();
		newsService.editNews(news);
		Mockito.verify(newsService, Mockito.times(1)).editNews(news);
	}
	
	@Test
	public void deleteNewsByIdTest() throws ServiceException {
		newsService.deleteNewsById(1L);
		Mockito.verify(newsService, Mockito.times(1)).deleteNewsById(1L);;
	}
	
	@Test
	public void getNewsByIdTest() throws ServiceException {
		NewsTO expectedNews = new NewsTO();
		Mockito.when(newsService.getNewsById(1L)).thenReturn(expectedNews);
		NewsTO actualNews = newsService.getNewsById(1L);
		Mockito.verify(newsService, Mockito.times(1)).getNewsById(1L);
		assertEquals(expectedNews,actualNews);
	}
	
	@Test
	public void countNewsTest() throws ServiceException {
		newsService.countNews();
		Mockito.verify(newsService, Mockito.times(1)).countNews();
	}
	
	@Test
	public void getNewsTest() throws ServiceException {
		newsService.getNews();
		Mockito.verify(newsService, Mockito.times(1)).getNews();
	}
	
	@Test
	public void getNewsListByIdTest() throws ServiceException {
		List<Long> idList = new ArrayList<Long>();
		List<NewsTO> expectedList = new ArrayList<NewsTO>();
		Mockito.when(newsService.getNewsListById(idList)).thenReturn(expectedList);
		List<NewsTO> actualList = newsService.getNewsListById(idList);
		Mockito.verify(newsService, Mockito.times(1)).getNewsListById(idList);
		assertEquals(expectedList,actualList);
	}
	
	@Test
	public void searchNews() throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		newsService.searchNews(criteria);
		Mockito.verify(newsService, Mockito.times(1)).searchNews(criteria);
	}
	
}
