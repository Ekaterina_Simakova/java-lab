package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.CommentDaoImpl;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("commentDaoDataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class CommentDaoImplTest {
	
	@Autowired
	@Qualifier("testDataSource")
	private BasicDataSource dataSource;

	@Autowired
	private CommentDaoImpl commentDao;

	@Before
	public void init() {
		commentDao.setDataSource(dataSource);
	}
	
	@Test
	public void addCommentTest() throws DaoException {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setCommentId(1L);
		expectedComment.setCommentText("testComment");
		expectedComment.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		expectedComment.setNewsId(1L);
		Long commentId = commentDao.addComment(expectedComment);
		expectedComment.setCommentId(commentId);
		CommentTO actualComment = commentDao.getCommentById(commentId);
		assertEquals(expectedComment,actualComment);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/comment/deleteCommentExpected.xml")
	public void deleteCommentByIdTest() throws DaoException {
		commentDao.deleteCommentById(1L);
	}
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/comment/editCommentExpected.xml")
	public void editCommentTest() throws DaoException {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setCommentId(1L);
		expectedComment.setCommentText("editComment");
		commentDao.editComment(expectedComment);
	}
	
	@Test
	public void getCommentByIdTest() throws DaoException {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setCommentId(1L);
		expectedComment.setNewsId(1L);
		expectedComment.setCommentText("firstComment");
		expectedComment.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		CommentTO actualComment = commentDao.getCommentById(1L);
		assertEquals(expectedComment,actualComment);
	}
	
	@Test
	public void getCommentListByNewsIdTest() throws DaoException {
		CommentTO comment1 = new CommentTO();
		comment1.setCommentId(1L);
		comment1.setNewsId(1L);
		comment1.setCommentText("firstComment");
		comment1.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		CommentTO comment2 = new CommentTO();
		comment2.setCommentId(2L);
		comment2.setNewsId(1L);
		comment2.setCommentText("secondComment");
		comment2.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		CommentTO comment3 = new CommentTO();
		comment3.setCommentId(3L);
		comment3.setNewsId(1L);
		comment3.setCommentText("thirdComment");
		comment3.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		List<CommentTO> expectedList = new ArrayList<CommentTO>(Arrays.asList(comment1,comment2,comment3));
		List<CommentTO> actualList = commentDao.getCommentListByNewsId(1L);
		assertTrue(expectedList.containsAll(actualList) && actualList.containsAll(expectedList));
	} 
	
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,value = "expected/comment/deleteAllCommentsExpected.xml")
	public void deleteAllNewsCommentsTest() throws DaoException {
		commentDao.deleteAllNewsComments(1L);
	}
	
	

}
