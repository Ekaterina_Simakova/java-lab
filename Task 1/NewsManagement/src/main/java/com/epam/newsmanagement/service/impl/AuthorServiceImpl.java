package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * @see AuthorService
 * @author Katsiaryna_Simakova
 *
 */
public class AuthorServiceImpl implements AuthorService {
	
	private AuthorDao authorDao;
	
	/**
	 * Sets the data access object.
	 * 
	 * @param authorDao AuthorDao implementation
	 */
	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	@Override
	public Long addAuthor(AuthorTO author) throws ServiceException {
		try {
			return authorDao.addAuthor(author);
		} catch (DaoException e) {
			throw new ServiceException("Adding the author failed",e);
		}
	}

	@Override
	public void expireAuthorById(Long authorId) throws ServiceException {
		try {
			authorDao.expireAuthorById(authorId);
		} catch (DaoException e) {
			throw new ServiceException("Authors expiration failed",e);
		}
	}

	@Override
	public void detachAuthorFromNewsByNewsId(Long newsId) throws ServiceException {
		try {
			authorDao.detachAuthorFromNewsById(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Unbinding author of news failed",e);
		}		
	}

	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException {
		try {
			return authorDao.getAuthorByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Getting author failed",e);
		}
	}

	@Override
	public void attachAuthorToNewsById(Long authorId, Long newsId) throws ServiceException {
		try {
			authorDao.attachAuthorToNewsById(authorId, newsId);
		} catch (DaoException e) {
			throw new ServiceException("Binding author with news failed",e);
		}
	}

}
