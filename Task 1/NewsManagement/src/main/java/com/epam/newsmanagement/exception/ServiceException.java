package com.epam.newsmanagement.exception;

public class ServiceException extends Exception {
	
	public ServiceException(String message) {
		super(message);
	}
	
	public ServiceException(String message, Exception hidden) {
		super(message, hidden);
	}

}
