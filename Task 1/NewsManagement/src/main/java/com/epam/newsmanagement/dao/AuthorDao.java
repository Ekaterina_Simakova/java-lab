package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;

public interface AuthorDao {
	
	/**
	 * Adds new AuthorTO object and return its generated id.
	 * 
	 * @param author AuthorTO object that will be added
	 * @return id of added AuthorTO object
	 * @throws DaoException
	 */
	Long addAuthor(AuthorTO author) throws DaoException;
	
	/**
	 * Replases deleting author by its hidding by id.
	 * 
	 * @param authorId id of author that will be hidden
	 * @throws DaoException
	 */
	void expireAuthorById(Long authorId) throws DaoException;
	
	/**
	 * Edits information about the author.
	 * 
	 * @param author AuthorTO object that contains new information
	 * @throws DaoException
	 */
	void editAuthor(AuthorTO author) throws DaoException;
	
	/**
	 * Gets information about author by id.
	 * 
	 * @param authorId id of expected author
	 * @return expected <code>AuthorTO</code> object
	 * @throws DaoException
	 */
	AuthorTO getAuthorById(Long authorId) throws DaoException;
	
	/**
	 * Attaches author to news by ids.
	 * 
	 * @param authorId authors id
	 * @param newsId news id
	 * @throws DaoException
	 */
	void attachAuthorToNewsById(Long authorId, Long newsId) throws DaoException;
	
	/**
	 * Detaches the author from the news by ids.
	 * 
	 * @param authorId authors id
	 * @param newsId news id
	 * @throws DaoException
	 */
	void detachAuthorFromNewsById(Long newsId) throws DaoException;
	
	/**
	 * Searchs the author by news id and gets info about him.
	 * 
	 * @param newsId news id
	 * @return AuthorTO object
	 * @throws DaoException
	 */
	AuthorTO getAuthorByNewsId(Long newsId) throws DaoException;

}
