package com.epam.newsmanagement.util;

import java.util.List;

public class SearchCriteria {

	private List<Long> tagIdList;
	private Long authorId;

	public List<Long> getTagIdList() {
		return tagIdList;
	}
	
	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}
	
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	
}
