package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * @see TagService
 * @author Katsiaryna_Simakova
 *
 */
public class TagServiceImpl implements TagService {

	private TagDao tagDao;

	/**
	 * Sets the data access object.
	 * 
	 * @param tagDao TagDao implementation
	 */
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}
	
	@Override
	public Long addTag(TagTO tag) throws ServiceException {
		try {
			return tagDao.addTag(tag);
		} catch (DaoException e) {
			throw new ServiceException("Transaction of adding tag is rolled back", e);
		}
	}

	@Override
	public void editTag(TagTO tag) throws ServiceException {
		try {
			tagDao.editTag(tag);
		} catch (DaoException e) {
			throw new ServiceException("Editing tag failed", e);
		}
	}

	@Override
	public List<TagTO> getTagListByNewsId(Long newsId) throws ServiceException {
		try {
			return tagDao.getTagListByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Getting tag list failed", e);
		}
	}

	@Override
	public void detachAllTagsFromNewsById(Long newsId) throws ServiceException {
		try {
			tagDao.detachAllTagsFromNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Unbinding tags of news failed", e);
		}
	}

	@Override
	public void detachTagFromNewsById(Long tagId, Long newsId) throws ServiceException {
		try {
			tagDao.detachTagFromNewsById(tagId, newsId);
		} catch (DaoException e) {
			throw new ServiceException("Unbinding tag of news failed",e);
		}
	}

	@Override
	public void attachTagListToNews(List<Long> idTagList, Long newsId) throws ServiceException {
		try {
			tagDao.attachTagListToNewsById(idTagList, newsId);
		} catch (DaoException e) {
			throw new ServiceException("Attaching tag list failed", e);
		}
	}

	@Override
	public void attachTagToNewsById(Long tagId, Long newsId) throws ServiceException {
		try {
			tagDao.attachTagToNewsById(tagId, newsId);
		} catch (DaoException e) {
			throw new ServiceException("Attaching tag id " + tagId + " to news id " + newsId + " failed", e);
		}
		
	}
	
}