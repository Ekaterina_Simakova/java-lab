package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * @see CommentService
 * @author Katsiaryna_Simakova
 *
 */
public class CommentServiceImpl implements CommentService {
	
	private CommentDao commentDao;
	
	/**
	 * Sets the data access object.
	 * 
	 * @param commentDao CommentDao implementation
	 */
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@Override
	public Long addComment(CommentTO comment) throws ServiceException {
		try {
			return commentDao.addComment(comment);
		} catch (DaoException e) {
			throw new ServiceException("Adding comment failed",e);
		}
	}

	@Override
	public void deleteCommentById(Long commentId) throws ServiceException {
		try {
			commentDao.deleteCommentById(commentId);
		} catch (DaoException e) {
			throw new ServiceException("Deleting comment failed",e);
		}
	}

	@Override
	public void deleteAllNewsCommentsByNewsId(Long newsId) throws ServiceException {
		try {
			commentDao.deleteAllNewsComments(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Comments deleting failed",e);
		}
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws ServiceException {
		try {
			return commentDao.getCommentListByNewsId(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Getting comments failed",e);
		}
	}

	@Override
	public void editComment(CommentTO comment) throws ServiceException {
		try {
			commentDao.editComment(comment);
		} catch (DaoException e) {
			throw new ServiceException("Editing comment failed",e);
		}
	}

}
