package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.util.SearchCriteria;

/**
 * @see NewsService
 * @author Katsiaryna_Simakova
 *
 */
public class NewsServiceImpl implements NewsService {
	
	private NewsDao newsDao;
	
	/**
	 * Sets the data access object.
	 * 
	 * @param newsDao NewsDao implementation
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	@Override
	public Long addNews(NewsTO news) throws ServiceException {
		try {
			return newsDao.addNews(news);
		} catch (DaoException e) {
			throw new ServiceException("Adding news failed",e);
		}
	}

	@Override
	public void editNews(NewsTO news) throws ServiceException {
		try {
			newsDao.editNews(news);
		} catch (DaoException e) {
			throw new ServiceException("Editing news failed",e);
		}
	}

	@Override
	public void deleteNewsById(Long newsId) throws ServiceException {
		try {
			newsDao.deleteNewsById(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Deleting news failed",e);
		}
	}

	@Override
	public NewsTO getNewsById(Long newsId) throws ServiceException {
		try {
			return newsDao.getNewsById(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Getting news failed",e);
		}
	}

	@Override
	public Long countNews() throws ServiceException {
		try {
			return newsDao.countNews();
		} catch (DaoException e) {
			throw new ServiceException("Counting news failed",e);
		}
	}

	@Override
	public List<NewsTO> getNews() throws ServiceException {
		try {
			return newsDao.getNewsList();
		} catch (DaoException e) {
			throw new ServiceException("Getting news list failed",e);
		}
	}

	@Override
	public List<NewsTO> getNewsListById(List<Long> idNewsList) throws ServiceException {
		try {
			return newsDao.getNewsListById(idNewsList);
		} catch (DaoException e) {
			throw new ServiceException("Getting news list failed",e);
		}
	}

	@Override
	public List<Long> searchNews(SearchCriteria criteria) throws ServiceException {
		try {
			return newsDao.searchNews(criteria);
		} catch (DaoException e) {
			throw new ServiceException("Searching news failed",e);
		}
	}

}
