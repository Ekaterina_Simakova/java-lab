package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.DBUtil;

/**
 * @see AuthorDao
 * @author Katsiaryna Simakova
 *
 */
public class AuthorDaoImpl implements AuthorDao {

	private BasicDataSource dataSource;

	private static final String COLUMN_AUTHOR_ID = "AUTHOR_ID";
	private static final String COLUMN_AUTHOR_NAME = "AUTHOR_NAME";
	private static final String COLUMN_EXPIRED = "EXPIRED";

	private static final String SQL_INSERT_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) "
			+ "VALUES(AUTHOR_ID_SEQ.NEXTVAL,?,?)";
	private static final String SQL_EXPIRE_AUTHOR_BY_ID = "UPDATE AUTHOR SET EXPIRED=NOW() WHERE AUTHOR_ID=?";
	private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME=?, EXPIRED=? WHERE AUTHOR_ID=?";
	private static final String SQL_GET_AUTHOR_BY_ID = "SELECT AUTHOR_NAME, EXPIRED FROM Author WHERE AUTHOR_ID=?";
	private static final String SQL_ATTACH_AUTHOR_TO_NEWS = "MERGE INTO NEWS_AUTHOR USING DUAL ON (NEWS_AUTHOR.NEWS_ID=?) "
			+ "WHEN MATCHED THEN UPDATE SET NEWS_AUTHOR.AUTHOR_ID=? "
			+ "WHEN NOT MATCHED THEN INSERT (NEWS_AUTHOR.NEWS_ID, NEWS_AUTHOR.AUTHOR_ID) VALUES (?,?)";
	private static final String SQL_DETACH_AUTHOR_FROM_NEWS = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID=?";
	private static final String SQL_GET_AUTHOR_BY_NEWS_ID = "SELECT NA.AUTHOR_ID, A.AUTHOR_NAME, A.EXPIRED FROM AUTHOR A "
			+ "INNER JOIN NEWS_AUTHOR NA ON A.AUTHOR_ID=NA.AUTHOR_ID WHERE NA.NEWS_ID=?";
	
	/**
	 * Sets the <code>BasicDataSource</code> object for access to the database.
	 * 
	 * @param dataSource <code>BasicDataSource</code> object
	 * @see org.apache.commons.dbcp.BasicDataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long addAuthor(AuthorTO author) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			Long authorId = null;
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_INSERT_AUTHOR, new String[] { COLUMN_AUTHOR_ID });
			pst.setString(1, author.getAuthorName());
			pst.setTimestamp(2, author.getExpired());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs.next()) {
				authorId = rs.getLong(1);
			}
			return authorId;
		} catch (SQLException e) {
			throw new DaoException("Inserting author failed, " + author, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public void expireAuthorById(Long authorId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_EXPIRE_AUTHOR_BY_ID);
			pst.setLong(1, authorId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Updating author failed, id is " + authorId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public void editAuthor(AuthorTO author) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_UPDATE_AUTHOR);
			pst.setString(1, author.getAuthorName());
			pst.setTimestamp(2, author.getExpired());
			pst.setLong(3, author.getAuthorId());
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Updating author failed " + author, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public AuthorTO getAuthorById(Long authorId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			AuthorTO author = new AuthorTO();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_AUTHOR_BY_ID);
			pst.setLong(1, authorId);
			rs = pst.executeQuery();
			author.setAuthorId(authorId);
			if (rs.next()) {
				author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
				author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
			}
			return author;
		} catch (SQLException e) {
			throw new DaoException("Selecting author by id failed, author id is " + authorId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public void attachAuthorToNewsById(Long authorId, Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_ATTACH_AUTHOR_TO_NEWS);
			pst.setLong(1, newsId);
			pst.setLong(2, authorId);
			pst.setLong(3, newsId);
			pst.setLong(4, authorId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Inserting author id " + authorId + " to news id " + newsId + " failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public void detachAuthorFromNewsById(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_DETACH_AUTHOR_FROM_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Deleting author from news id " + newsId + " failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}
	
	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			AuthorTO author = new AuthorTO();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_AUTHOR_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if (rs.next()) {
				author.setAuthorId(rs.getLong(COLUMN_AUTHOR_ID));
				author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
				author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
			}
			return author;
		} catch (SQLException e) {
			throw new DaoException("Selecting author by news id " + newsId + " failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

}
