package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsService {

	/**
	 * Adds news.
	 * 
	 * @param news NewsTO object
	 * @return news id
	 * @throws ServiceException
	 */
	Long addNews(NewsTO news) throws ServiceException;

	/**
	 * Edits news data.
	 * 
	 * @param news updated NewsTO object
	 * @throws ServiceException
	 */
	void editNews(NewsTO news) throws ServiceException;

	/**
	 * Deletes news by id.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void deleteNewsById(Long newsId) throws ServiceException;

	/**
	 * Gets news by id.
	 * 
	 * @param newsId news id
	 * @return NewsTO object
	 * @throws ServiceException
	 */
	NewsTO getNewsById(Long newsId) throws ServiceException;
	
	/**
	 * Gets the list of news by ids.
	 * 
	 * @param idNewsList the list of news id
	 * @return the list of NewsTO objects
	 * @throws ServiceException
	 */
	List<NewsTO> getNewsListById(List<Long> idNewsList) throws ServiceException;

	/**
	 * Gets the count of news.
	 * 
	 * @return the count of news
	 * @throws ServiceException
	 */
	Long countNews() throws ServiceException;
	
	/**
	 * Gets sorted by modification date and number of comments the list of news.
	 * 
	 * @return the list of NewsTO objects
	 * @throws ServiceException
	 */
	List<NewsTO> getNews() throws ServiceException; 

	/**
	 * Gets news ids according to SearchCriteria.
	 * 
	 * @param criteria
	 * @return list of news ids
	 * @throws ServiceException
	 */
	List<Long> searchNews(SearchCriteria criteria) throws ServiceException;

}
