package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsDao {
	
	/**
	 * Adds news, i.e. <code>NewsTO</code> object.
	 * 
	 * @param news <code>NewsTO</code> object
	 * @return generated news id
	 * @throws DaoException
	 */
	Long addNews(NewsTO news) throws DaoException;
	
	/**
	 * Delete news by its id.
	 * 
	 * @param newsId news id
	 * @throws DaoException
	 */
	void deleteNewsById(Long newsId) throws DaoException;
	
	/**
	 * Edits news information.
	 * 
	 * @param news updated <code>NewsTO</code> object
	 * @throws DaoException
	 */
	void editNews(NewsTO news) throws DaoException;
	
	/**
	 * Gets list of news sorted by modification date and number of comments.
	 * 
	 * @return list of <code>NewsTO</code> objects
	 * @throws DaoException
	 */
	List<NewsTO> getNewsList() throws DaoException;
	
	/**
	 * Gets list of news that is appropriate list of news ids.
	 * 
	 * @param idNewsList list of required news ids.
	 * @return list of <code>NewsTO</code> objects
	 * @throws DaoException
	 */
	List<NewsTO> getNewsListById(List<Long> idNewsList) throws DaoException;
	
	/**
	 * Gets news data by its id.
	 * 
	 * @param id required news id
	 * @return <code>NewsTO</code> object
	 * @throws DaoException when getting will be failed
	 */
	NewsTO getNewsById(Long id) throws DaoException;
	
	/**
	 * Gets the number of news.
	 * 
	 * @return the number of news
	 * @throws DaoException
	 */
	Long countNews() throws DaoException;
	
	/**
	 * Gets the list of news ids according to  SearchCriteria.
	 * 
	 * @param criteria
	 * @return
	 * @throws DaoException
	 */
	List<Long> searchNews(SearchCriteria criteria) throws DaoException;
	
}
