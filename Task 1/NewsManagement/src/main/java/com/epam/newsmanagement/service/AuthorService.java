package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;


public interface AuthorService {

	/**
	 * Adds new author.
	 * 
	 * @param author AuthorTO object
	 * @return authors id
	 * @throws ServiceException
	 */
	Long addAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Makes author expired, it replaces removing him.
	 * 
	 * @param authorId author id
	 * @throws ServiceException
	 */
	void expireAuthorById(Long authorId) throws ServiceException;
	
	/**
	 * Attaches author to news by ids.
	 * 
	 * @param authorId author id
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void attachAuthorToNewsById(Long authorId, Long newsId) throws ServiceException;
	
	/**
	 * Detachs author from news by news id.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void detachAuthorFromNewsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Gets author by news id.
	 * 
	 * @param newsId news id
	 * @return AuthorTO object
	 * @throws ServiceException
	 */
	AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException;

}
