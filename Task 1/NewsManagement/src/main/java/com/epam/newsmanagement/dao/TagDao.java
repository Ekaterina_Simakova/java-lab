package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;

public interface TagDao {
	
	/**
	 * Adds new tag.
	 * 
	 * @param tag TagTO object
	 * @return generated tags id
	 * @throws DaoException
	 */
	Long addTag(TagTO tag) throws DaoException;
	
	/**
	 * Deletes tag by id.
	 * 
	 * @param tagId tag id
	 * @throws DaoException
	 */
	void deleteTagById(Long tagId) throws DaoException;
	
	/**
	 * Edits tag data.
	 * 
	 * @param tag updated TagTO object
	 * @throws DaoException
	 */
	void editTag(TagTO tag) throws DaoException;
	
	/**
	 * Gets tag by id.
	 * 
	 * @param tagId tag id
	 * @return TagTO object
	 * @throws DaoException
	 */
	TagTO getTagById(Long tagId) throws DaoException;

	/**
	 * Attach tag to news by ids.
	 * 
	 * @param tagId tags id
	 * @param newsId news id
	 * @throws DaoException
	 */
	void attachTagToNewsById(Long tagId, Long newsId) throws DaoException;
	
	/**
	 * Attach tag list to news by ids.
	 * 
	 * @param tagList list of tags ids
	 * @param newsId news id
	 * @throws DaoException
	 */
	void attachTagListToNewsById(List<Long> tagList, Long newsId) throws DaoException;
	
	/**
	 * Detach tag from the news by ids.
	 * 
	 * @param tagId tag id
	 * @param newsId news id
	 * @throws DaoException
	 */
	void detachTagFromNewsById(Long tagId, Long newsId) throws DaoException;
	
	/**
	 * Detach all tags from the news by news id.
	 * 
	 * @param newsId news id
	 * @throws DaoException
	 */
	void detachAllTagsFromNews(Long newsId) throws DaoException;
	
	/**
	 * Gets all tags of news by news id.
	 * 
	 * @param newsId news id
	 * @return list of <code>TagTO</code> objects
	 * @throws DaoException
	 */
	List<TagTO> getTagListByNewsId(Long newsId) throws DaoException;

}
