package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.SearchCriteria;

/**
 * @see NewsManager
 * @author Katsiaryna_Simakova
 *
 */
public class NewsManagerImpl implements NewsManager {
	
	private static final Logger logger = Logger.getLogger("serviceLogger");
	
	private AuthorService authorService;
	private CommentService commentService;
	private NewsService newsService;
	private TagService tagService;

	public void setAuthorService(AuthorServiceImpl authorService) {
		this.authorService = authorService;
	}
	
	public void setCommentService(CommentServiceImpl commentService) {
		this.commentService = commentService;
	}
	
	public void setNewsService(NewsServiceImpl newsService) {
		this.newsService = newsService;
	}
	
	public void setTagService(TagServiceImpl tagService) {
		this.tagService = tagService;
	}

	/**
	 * Gets tags and author by news id and composes new NewsVO object.
	 * 
	 * @param newsTO NewsTO object
	 * @return NewsVO object
	 * @throws ServiceException if some Service operations failed
	 */
	private NewsVO composeNews(Long newsId) throws ServiceException {
		List<TagTO> tagList = tagService.getTagListByNewsId(newsId);
		List<CommentTO> commentList = commentService.getCommentsByNewsId(newsId);
		AuthorTO author = authorService.getAuthorByNewsId(newsId);
		NewsVO newsVO = new NewsVO();
		newsVO.setAuthor(author);
		newsVO.setTagList(tagList);
		newsVO.setCommentList(commentList);
		return newsVO;
	}
	
	/**
	 * Conducts the transaction. Transaction contains adding news, attaching tags 
	 * and author to this news.
	 * 
	 * @exception ServiceException if some Service operations failed
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=ServiceException.class)
	public Long addNews(NewsVO news) throws ServiceException {
		try {
			List<Long> idTagList = new ArrayList<Long>();
			for(TagTO tag : news.getTagList()) {
				idTagList.add(tag.getTagId());
			}
			Long newsId = newsService.addNews(news.getNewsTO());
			tagService.attachTagListToNews(idTagList, newsId);
			authorService.attachAuthorToNewsById(news.getAuthor().getAuthorId(), newsId);
			return newsId;
		} catch(ServiceException e) {
			logger.error("Adding news with tags and author failed", e);
			throw e;
		}
	}

	/**
	 * @see NewsService#editNews(NewsTO)
	 */
	@Override
	public void editNews(NewsTO news) throws ServiceException {
		try {
			newsService.editNews(news);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Conducts the transaction. Transaction contains deleting news, unbinding tags 
	 * and author of this news.
	 * 
	 * @exception ServiceException if some Service operations failed
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=ServiceException.class)
	public void deleteNews(Long newsId) throws ServiceException {
		try {
			tagService.detachAllTagsFromNewsById(newsId);
			commentService.deleteAllNewsCommentsByNewsId(newsId);
			authorService.detachAuthorFromNewsByNewsId(newsId);
			newsService.deleteNewsById(newsId);
		} catch(ServiceException e) {
			logger.error("Deleting news failed", e);
			throw e;
		}
	}

	@Override
	public List<NewsVO> getNews() throws ServiceException {
		try {
			List<NewsVO> newsVOList = new ArrayList<NewsVO>();
			List<NewsTO> newsTOList = newsService.getNews();
			for(NewsTO newsTO : newsTOList) {
				NewsVO newsVO = composeNews(newsTO.getNewsId());
				newsVO.setNewsTO(newsTO);
				newsVOList.add(newsVO);
			}
			return newsVOList;
		} catch(ServiceException e) {
			logger.error("Getting news list failed", e);
			throw e;
		}
	}

	@Override
	public NewsVO getNewsById(Long newsId) throws ServiceException {
		try {
			NewsTO newsTO = newsService.getNewsById(newsId);
			NewsVO newsVO = composeNews(newsTO.getNewsId());
			newsVO.setNewsTO(newsTO);
			return newsVO;
		} catch(ServiceException e) {
			logger.error("Getting news failed", e);
			throw e;
		}
	}

	/**
	 * @see NewsService#countNews()
	 */ 
	@Override
	public Long countNews() throws ServiceException {
		try {
			return newsService.countNews();
		} catch(ServiceException e) {
			logger.error("Counting news failed", e);
			throw e;
		}
	}

	/**
	 * @see AuthorService#addAuthor(AuthorTO)
	 */ 
	@Override
	public Long addAuthor(AuthorTO author) throws ServiceException {
		try {
			return authorService.addAuthor(author);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see AuthorService#expireAuthorById(Long)
	 */ 
	@Override
	public void expireAuthorById(Long authorId) throws ServiceException {
		try {
			authorService.expireAuthorById(authorId);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see CommentService#addComment(CommentTO)
	 */ 
	@Override
	public Long addComment(CommentTO comment, Long newsId) throws ServiceException {
		try {
			return commentService.addComment(comment);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see CommentService#deleteCommentById(Long)
	 */ 
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentService.deleteCommentById(commentId);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see CommentService#editComment(CommentTO)
	 */ 
	@Override
	public void editComment(CommentTO comment) throws ServiceException {
		try {
			commentService.editComment(comment);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see TagService#addTag(TagTO)
	 */ 
	@Override
	public Long addTag(TagTO tag) throws ServiceException {
		try {
			return tagService.addTag(tag);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Gets news list according to search criteria. 
	 * If search criteria contains author and tag list searching will be conducted by
	 * both parameters. If one of them will be null searching will be conducted by
	 * existing parameter. 
	 */
	@Override
	public List<NewsVO> searchNews(SearchCriteria criteria) throws ServiceException {
		try {
			List<Long> idNewsList = new ArrayList<Long>();
			List<NewsVO> newsList = new ArrayList<NewsVO>();
			idNewsList = newsService.searchNews(criteria);
			for(Long newsId : idNewsList) {
				newsList.add(getNewsById(newsId));
			}
			return newsList;
		} catch(ServiceException e) {
			logger.error("Searching news failed", e);
			throw e;
		}
	}

	/**
	 * @see TagService#detachTagFromNewsById(Long, Long)
	 */
	@Override
	public void detachTagFromNewsById(Long tagId, Long newsId) throws ServiceException {
		try {
			tagService.detachTagFromNewsById(tagId, newsId);
		} catch(ServiceException e) {
			logger.error("Unbinding tag of news failed", e);
			throw e;
		}
	}

	@Override
	public void attachTagToNewsById(Long tagId, Long newsId) throws ServiceException {
		try {
			tagService.attachTagToNewsById(tagId, newsId);
		} catch(ServiceException e) {
			logger.error("Attaching tag to news failed", e);
			throw e;
		}
	}

	@Override
	public void editTag(TagTO tag) throws ServiceException {
		try {
			tagService.editTag(tag);
		} catch(ServiceException e) {
			logger.error("Editing tag failed", e);
			throw e;
		}
	}

}
