package com.epam.newsmanagement.exception;

public class DaoException extends Exception {
	
	public DaoException(String message) {
		super(message);
	}
	
	public DaoException(String message, Exception hidden) {
		super(message,hidden);
	}
}
