package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsManager {

	/**
	 * Adds news with author and the list of tags.
	 * 
	 * @param news NewsVO object
	 * @return news id
	 * @throws ServiceException
	 */
	Long addNews(NewsVO news) throws ServiceException;

	/**
	 * Edits news data.
	 * 
	 * @param news updated NewsTO object
	 * @throws ServiceException
	 */
	void editNews(NewsTO news) throws ServiceException;

	/**
	 * Deletes news by its id.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void deleteNews(Long newsId) throws ServiceException;

	/**
	 * Gets the list of news sorted by modification date and number of comments.
	 * 
	 * @return the list of NewsVO objects
	 * @throws ServiceException
	 */
	List<NewsVO> getNews() throws ServiceException;
	
	/**
	 * Search news according to the search criteria data.
	 * 
	 * @param criteria SearchCriteria object
	 * @return the list of NewsVO objects
	 * @throws ServiceException
	 */
	List<NewsVO> searchNews(SearchCriteria criteria) throws ServiceException;

	/**
	 * Gets full data of required news by its id.
	 * 
	 * @param newsId news id
	 * @return NewsVO object
	 * @throws ServiceException
	 */
	NewsVO getNewsById(Long newsId) throws ServiceException;
	
	/**
	 * Gets the count of news.
	 * 
	 * @return the count of news
	 * @throws ServiceException
	 */
	Long countNews() throws ServiceException;

	/**
	 * Adds new author.
	 * 
	 * @param author AuthorTO object
	 * @return authors id
	 * @throws ServiceException
	 */
	Long addAuthor(AuthorTO author) throws ServiceException;

	/**
	 * Makes required author expireded, it replaces removing him.
	 * 
	 * @param authorId author id
	 * @throws ServiceException
	 */
	void expireAuthorById(Long authorId) throws ServiceException;

	/**
	 * Adds new comment to required news.
	 * 
	 * @param comment CommentTO object
	 * @param newsId news id
	 * @return comments id
	 * @throws ServiceException
	 */
	Long addComment(CommentTO comment, Long newsId) throws ServiceException;

	/**
	 * Deletes required comment by its id.
	 * 
	 * @param commentId comment id
	 * @throws ServiceException
	 */
	void deleteComment(Long commentId) throws ServiceException;
	
	/**
	 * Edits comment data.
	 * 
	 * @param comment updated CommentTO object
	 * @throws ServiceException
	 */
	void editComment(CommentTO comment) throws ServiceException;

	/**
	 * Adds tag.
	 * 
	 * @param tag TagTO object
	 * @param newsId news id
	 * @return tags id
	 * @throws ServiceException
	 */
	Long addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Attaches tag to news by ids.
	 * 
	 * @param tagId tag id
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void attachTagToNewsById(Long tagId, Long newsId) throws ServiceException;
	
	/**
	 * Detach tag from news by ids.
	 * 
	 * @param tagId tag id
	 * @param NewsId news id
	 * @throws ServiceException
	 */
	void detachTagFromNewsById(Long tagId, Long newsId) throws ServiceException;
	
	/**
	 * Edits tag.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	void editTag(TagTO tag) throws ServiceException;

}
