package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.DBUtil;
import com.epam.newsmanagement.util.SearchCriteria;

/**
 * @see NewsDao
 * @author Katsiaryna_Simakova
 *
 */
public class NewsDaoImpl implements NewsDao {

	private BasicDataSource dataSource;

	private static final String COLUMN_ID = "NEWS_ID";
	private static final String COLUMN_TITLE = "TITLE";
	private static final String COLUMN_SHORT_TEXT = "SHORT_TEXT";
	private static final String COLUMN_FULL_TEXT = "FULL_TEXT";
	private static final String COLUMN_CREATION_DATE = "CREATION_DATE";
	private static final String COLUMN_MODIFICATION_DATE = "MODIFICATION_DATE";
	private static final String COLUMN_COUNT = "COUNT";

	private static final String SQL_INSERT_NEWS = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, "
			+ "FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_ID_SEQ.NEXTVAL,?,?,?,?,?)";
	private static final String SQL_DELETE_NEWS_BY_ID = "DELETE FROM News WHERE NEWS_ID=?";
	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE=?, SHORT_TEXT=?, FULL_TEXT=?, "
			+ "CREATION_DATE=?, MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private static final String SQL_GET_NEWS_BY_ID = "SELECT TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, "
			+ "MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=?";
	private static final String SQL_GET_ALL_NEWS = "SELECT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, "
			+ "N.MODIFICATION_DATE FROM NEWS N LEFT JOIN (SELECT COUNT(COMMENT_ID) AS NUMB, NEWS_ID FROM COMMENTS "
			+ "GROUP BY NEWS_ID) COM ON N.NEWS_ID=COM.NEWS_ID ORDER BY N.MODIFICATION_DATE DESC, COM.NUMB DESC";
	private static final String SQL_COUNT_ALL_NEWS = "SELECT COUNT(NEWS_ID) AS COUNT FROM NEWS";
	
	private static final String SQL_SUBQUERY_GET_NEWS_LIST_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, "
			+ "CREATION_DATE, MODIFICATION_DATE FROM News WHERE NEWS_ID IN(";
	
	private static final String SQL_SUBQUERY_GET_ALL_NEWS_START = "SELECT DISTINCT N.NEWS_ID FROM NEWS N LEFT JOIN "
			+ "NEWS_AUTHOR NA ON N.NEWS_ID=NA.NEWS_ID LEFT JOIN NEWS_TAG NT ON N.NEWS_ID=NT.NEWS_ID "
			+ "WHERE NA.AUTHOR_ID=? AND NT.TAG_ID IN(";
	private static final String SQL_SUBQUERY_GET_ALL_NEWS_END =	") GROUP BY N.NEWS_ID HAVING COUNT(NT.TAG_ID)=?";
	
	/**
	 * Sets the <code>BasicDataSource</code> object for access to the database.
	 * 
	 * @param dataSource <code>BasicDataSource</code> object
	 * @see org.apache.commons.dbcp.BasicDataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long addNews(NewsTO news) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			Long newsId = null;
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_INSERT_NEWS,new String[] { COLUMN_ID });
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setTimestamp(4, news.getCreationDate());
			pst.setTimestamp(5, news.getModificationDate());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs.next()) {
				newsId = rs.getLong(1);
			}
			return newsId;
		} catch (SQLException e) {
			throw new DaoException("Inserting news failed, " + news, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public void deleteNewsById(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_DELETE_NEWS_BY_ID);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Deleting news failed, id is " + newsId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public void editNews(NewsTO news) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_UPDATE_NEWS);
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setTimestamp(4, news.getCreationDate());
			pst.setTimestamp(5, news.getModificationDate());
			pst.setLong(6, news.getNewsId());
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Updating news failed, " + news, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public List<NewsTO> getNewsList() throws DaoException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			List<NewsTO> newsList = new ArrayList<NewsTO>();
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			rs = st.executeQuery(SQL_GET_ALL_NEWS);
			while (rs.next()) {
				NewsTO news = new NewsTO();
				news.setNewsId(rs.getLong(COLUMN_ID));
				news.setTitle(rs.getString(COLUMN_TITLE));
				news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
				news.setFullText(rs.getString(COLUMN_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
				news.setModificationDate(rs.getTimestamp(COLUMN_MODIFICATION_DATE));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Selecting all news failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, st, rs);
		}
	}

	@Override
	public NewsTO getNewsById(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			NewsTO news = new NewsTO();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_NEWS_BY_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if (rs.next()) {
				news.setTitle(rs.getString(COLUMN_TITLE));
				news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
				news.setFullText(rs.getString(COLUMN_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
				news.setModificationDate(rs.getTimestamp(COLUMN_MODIFICATION_DATE));
				news.setNewsId(newsId);
			}
			return news;
		} catch (SQLException e) {
			throw new DaoException("Selecting news failed, id is " + newsId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public Long countNews() throws DaoException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			Long count = null;
			conn = DataSourceUtils.getConnection(dataSource);
			//st = conn.createStatement();
			//rs = st.executeQuery(SQL_COUNT_ALL_NEWS);
			/*if (rs.next()) {
				count = rs.getLong(COLUMN_COUNT);
			}*/
			return count;
		/*} catch (SQLException e) {
			throw new DaoException("Counting news failed", e);*/
		} finally {
			DBUtil.closeResources(dataSource, conn, st, rs);
		}
	}

	@Override
	public List<NewsTO> getNewsListById(List<Long> idNewsList) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuilder query = new StringBuilder().append(SQL_SUBQUERY_GET_NEWS_LIST_BY_ID);
		for (Long idNews : idNewsList) {
			query.append(idNews).append(",");
		}
		if (query.length() != SQL_SUBQUERY_GET_NEWS_LIST_BY_ID.length()) {
			query.setCharAt(query.length() - 1, ')');
		} else {
			query.append(')');
		}
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(query.toString());
			rs = pst.executeQuery();
			List<NewsTO> newsList = new ArrayList<NewsTO>();
			while (rs.next()) {
				NewsTO news = new NewsTO();
				news.setNewsId(rs.getLong(COLUMN_ID));
				news.setTitle(rs.getString(COLUMN_TITLE));
				news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
				news.setFullText(rs.getString(COLUMN_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
				news.setModificationDate(rs.getTimestamp(COLUMN_MODIFICATION_DATE));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Selecting news by id list failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}

	}

	@Override
	public List<Long> searchNews(SearchCriteria criteria) throws DaoException {
		List<Long> idNewsList = new ArrayList<Long>();
		StringBuilder builder = new StringBuilder().append(SQL_SUBQUERY_GET_ALL_NEWS_START);
		for(Long idTag : criteria.getTagIdList()) {
			builder.append(idTag).append(",");
		}
		if(criteria.getTagIdList().size() != 0) {
			builder.deleteCharAt(builder.length()-1);
		}
		builder.append(SQL_SUBQUERY_GET_ALL_NEWS_END);
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(builder.toString());
			pst.setLong(1, criteria.getAuthorId());
			pst.setLong(2, criteria.getTagIdList().size());
			rs = pst.executeQuery();
			while(rs.next()) {
				idNewsList.add(rs.getLong(COLUMN_ID));
			}
			return idNewsList;
		} catch (SQLException e) {
			throw new DaoException("Searching news ids by criteria " + criteria + "failed", e);
		}
	}
}
