package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

public interface TagService {
	
	/**
	 * Adds tag.
	 * 
	 * @param tag TagTO object
	 * @return tags id
	 * @throws ServiceException
	 */
	Long addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Edits tag data.
	 * 
	 * @param tag updated TagTO object
	 * @throws ServiceException
	 */
	void editTag(TagTO tag) throws ServiceException;
	
	/**
	 * Attaches tag to news by id.
	 * 
	 * @param tagId
	 * @param newsId
	 * @throws DaoException
	 */
	void attachTagToNewsById(Long tagId, Long newsId) throws ServiceException;
	
	/**
	 * Attaches tag list to news by ids.
	 * 
	 * @param idTagList list of tag ids
	 * @param newsId news id
	 */
	void attachTagListToNews(List<Long> idTagList, Long newsId) throws ServiceException;
	
	/**
	 * Detach tag from news by id.
	 * 
	 * @param tagId tag id
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void detachTagFromNewsById(Long tagId, Long newsId) throws ServiceException;
	
	/**
	 * Detaches the list of tags from news by news id.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void detachAllTagsFromNewsById(Long newsId) throws ServiceException;
	
	/**
	 * Gets all tags by news id.
	 * 
	 * @param newsId news id
	 * @return the list of TagTO objects
	 * @throws ServiceException
	 */
	List<TagTO> getTagListByNewsId(Long newsId) throws ServiceException;

}
