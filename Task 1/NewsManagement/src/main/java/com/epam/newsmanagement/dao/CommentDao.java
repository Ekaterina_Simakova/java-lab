package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;

public interface CommentDao {
	
	/**
	 * Adds new comment.
	 * 
	 * @param CommentTO object
	 * @return generated comments id
	 * @throws DaoException
	 */
	Long addComment(CommentTO comment) throws DaoException;
	
	/**
	 * Deletes comment by id.
	 * 
	 * @param commentId comments id
	 * @throws DaoException
	 */
	void deleteCommentById(Long commentId) throws DaoException;
	
	/**
	 * Deletes all comments by news id.
	 * 
	 * @param newsId news id
	 * @throws DaoException
	 */
	void deleteAllNewsComments(Long newsId) throws DaoException;
	
	/**
	 * Edits comments info.
	 * 
	 * @param updated CommentTO object
	 * @throws DaoException
	 */
	void editComment(CommentTO comment) throws DaoException;
	
	/**
	 * Gets comment by id.
	 * 
	 * @param commentId comments id
	 * @return CommentTO object
	 * @throws DaoException
	 */
	CommentTO getCommentById(Long commentId) throws DaoException;
	
	/**
	 * Gets all comments by news id.
	 * 
	 * @param newsId news id
	 * @return list of CommentTO objects
	 * @throws DaoException
	 */
	List<CommentTO> getCommentListByNewsId(Long newsId) throws DaoException;

}
