package com.epam.newsmanagement.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Resource(name="userDaoImpl")
	private UserDao userDao;

	@Override
	public UserTO getUserByName(String name, String password) {
		return userDao.getUserByName(name, password);
	}

}
