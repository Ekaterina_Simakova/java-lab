package com.epam.newsmanagement.dao.impl.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.UserTO;

public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public UserTO getUserByName(String name, String password) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UserTO> critQuery = critBuilder.createQuery(UserTO.class);
		Root<UserTO> root = critQuery.from(UserTO.class);
		critQuery.where(critBuilder.equal(root.get("name"), name));
		critQuery.where(critBuilder.equal(root.get("password"), password));
		TypedQuery<UserTO> query = entityManager.createQuery(critQuery);
		return query.getSingleResult();
	}

}
