package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;


public interface AuthorService {

	/**
	 * Adds new author.
	 * 
	 * @param author AuthorTO object
	 * @return authors id
	 * @
	 */
	Long addAuthor(AuthorTO author) ;
	
	/**
	 * Makes author expired, it replaces removing him.
	 * 
	 * @param authorId author id
	 * @
	 */
	void expireAuthorById(Long authorId) ;
	
	/**
	 * Gets all authors.
	 * 
	 * @return the list of authors
	 * @
	 */
	List<AuthorTO> getAllAuthors() ;
	
	/**
	 * It edits AuthorTO object.
	 * 
	 * @param author
	 * @
	 */
	void editAuthor(AuthorTO author) ;
	
	Long countAuthors();
	
	List<AuthorTO> getAuthors(long start, long count);

}
