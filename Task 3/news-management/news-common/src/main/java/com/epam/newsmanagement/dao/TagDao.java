package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

public interface TagDao {
	
	/**
	 * Adds new tag.
	 * 
	 * @param tag TagTO object
	 * @return generated tags id
	 * @
	 */
	Long addTag(TagTO tag);
	
	/**
	 * Deletes tag by id.
	 * 
	 * @param tagId tag id
	 * @
	 */
	void deleteTagById(Long tagId);
	
	/**
	 * Edits tag data.
	 * 
	 * @param tag updated TagTO object
	 * @
	 */
	void editTag(TagTO tag);
	
	List<TagTO> getTags(long start, long count);
	
	/**
	 * Gets all tags.
	 * 
	 * @return the list of tags
	 * @throws ServiceException
	 */
	List<TagTO> getAllTags();
	
	Long countTags();

}
