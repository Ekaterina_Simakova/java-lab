package com.epam.newsmanagement.dao.impl.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;

/*@Component*/
/*@Profile("jpa")*/
public class TagDaoImpl implements TagDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long addTag(TagTO tag) {
		entityManager.persist(tag);
		return tag.getTagId();
	}

	@Override
	public void deleteTagById(Long tagId) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TagTO> critQuery = critBuilder.createQuery(TagTO.class);
		Root<TagTO> root = critQuery.from(TagTO.class);
		critQuery.where(critBuilder.equal(root.get("tagId"), tagId));
		TypedQuery<TagTO> query = entityManager.createQuery(critQuery);
		TagTO tag = query.getSingleResult();
		entityManager.remove(tag);		
	}

	@Override
	public void editTag(TagTO tag) {
		entityManager.merge(tag);
	}

	@Override
	public List<TagTO> getAllTags() {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TagTO> critQuery = critBuilder.createQuery(TagTO.class);
		Root<TagTO> root = critQuery.from(TagTO.class);
		critQuery.select(root);
		TypedQuery<TagTO> query = entityManager.createQuery(critQuery);
		return query.getResultList();
	}

	@Override
	public List<TagTO> getTags(long start, long count) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TagTO> critQuery = critBuilder.createQuery(TagTO.class);
		Root<TagTO> root = critQuery.from(TagTO.class);
		critQuery.select(root);
		TypedQuery<TagTO> query = entityManager.createQuery(critQuery);
		query.setFirstResult((int) start);
		query.setMaxResults((int) count);
		return query.getResultList();
	}

	@Override
	public Long countTags() {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> critQuery = critBuilder.createQuery(Long.class);
		Root<TagTO> root = critQuery.from(TagTO.class);
		critQuery.select(critBuilder.count(root));
		TypedQuery<Long> query = entityManager.createQuery(critQuery);
		return query.getSingleResult();
	}

}
