package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.UserTO;

public interface UserService {
	
	UserTO getUserByName(String name, String password);

}
