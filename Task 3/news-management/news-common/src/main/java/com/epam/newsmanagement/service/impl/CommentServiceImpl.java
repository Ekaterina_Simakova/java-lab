package com.epam.newsmanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.service.CommentService;

/**
 * @see CommentService
 * @author Katsiaryna_Simakova
 *
 */
@Service
@Transactional
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	private CommentDao commentDao;

	@Override
	public Long addComment(CommentTO comment)  {
		return commentDao.addComment(comment);
	}

	@Override
	public void deleteCommentById(Long commentId)  {
		commentDao.deleteCommentById(commentId);
	}

}
