package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.CommentTO;

public interface CommentService {
	
	/**
	 * Adds new comment.
	 * 
	 * @param comment CommentTO object
	 * @return comment id
	 * @
	 */
	Long addComment(CommentTO comment) ;
	
	/**
	 * Deletes comment by id.
	 * 
	 * @param commentId comment id
	 * @
	 */
	void deleteCommentById(Long commentId) ;
	
}
