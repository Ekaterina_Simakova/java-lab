package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;

public interface AuthorDao {
	
	/**
	 * Adds new AuthorTO object and return its generated id.
	 * 
	 * @param author AuthorTO object that will be added
	 * @return id of added AuthorTO object
	 * @
	 */
	Long addAuthor(AuthorTO author);
	
	/**
	 * Replases deleting author by its hidding by id.
	 * 
	 * @param authorId id of author that will be hidden
	 * @
	 */
	void expireAuthorById(Long authorId);
	
	/**
	 * Edits information about the author.
	 * 
	 * @param author AuthorTO object that contains new information
	 * @
	 */
	void editAuthor(AuthorTO author);
	
	/**
	 * Gets all authors.
	 * 
	 * @return the list of authors
	 * @
	 */
	List<AuthorTO> getAllAuthors() ;
	
	List<AuthorTO> getAuthors(long start, long count);
	
	Long countAuthors();

}
