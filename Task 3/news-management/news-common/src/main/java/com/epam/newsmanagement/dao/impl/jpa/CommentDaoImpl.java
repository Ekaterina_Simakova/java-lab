package com.epam.newsmanagement.dao.impl.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;

public class CommentDaoImpl implements CommentDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long addComment(CommentTO comment) {
		entityManager.persist(comment);
		return comment.getCommentId();
	}

	@Override
	public void deleteCommentById(Long commentId) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CommentTO> critQuery = critBuilder.createQuery(CommentTO.class);
		Root<CommentTO> root = critQuery.from(CommentTO.class);
		critQuery.where(critBuilder.equal(root.get("commentId"), commentId));
		TypedQuery<CommentTO> query = entityManager.createQuery(critQuery);
		CommentTO comment = query.getSingleResult();
		entityManager.remove(comment);
	}

}
