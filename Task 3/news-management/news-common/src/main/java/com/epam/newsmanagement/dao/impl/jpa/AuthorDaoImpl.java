package com.epam.newsmanagement.dao.impl.jpa;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;

/*@Component*/
/*@Profile("jpa")*/
public class AuthorDaoImpl implements AuthorDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long addAuthor(AuthorTO author) {
		entityManager.persist(author);
		return author.getAuthorId();
	}

	@Override
	public void expireAuthorById(Long authorId) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<AuthorTO> critQuery = critBuilder.createQuery(AuthorTO.class);
		Root<AuthorTO> root = critQuery.from(AuthorTO.class);
		critQuery.where(critBuilder.equal(root.get("authorId"), authorId));
		TypedQuery<AuthorTO> query = entityManager.createQuery(critQuery);
		AuthorTO author = query.getSingleResult();
		author.setExpired(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		entityManager.merge(author);
	}

	@Override
	public void editAuthor(AuthorTO author) {
		entityManager.merge(author);
	}

	@Override
	public List<AuthorTO> getAllAuthors()  {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<AuthorTO> critQuery = critBuilder.createQuery(AuthorTO.class);
		Root<AuthorTO> root = critQuery.from(AuthorTO.class);
		critQuery.where(critBuilder.isNull(root.get("expired")));
		TypedQuery<AuthorTO> query = entityManager.createQuery(critQuery);
		return query.getResultList();
	}

	@Override
	public List<AuthorTO> getAuthors(long start, long count) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<AuthorTO> critQuery = critBuilder.createQuery(AuthorTO.class);
		Root<AuthorTO> root = critQuery.from(AuthorTO.class);
		critQuery.select(root);
		TypedQuery<AuthorTO> query = entityManager.createQuery(critQuery);
		query.setFirstResult((int) start);
		query.setMaxResults((int) count);
		return query.getResultList();
	}

	@Override
	public Long countAuthors() {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> critQuery = critBuilder.createQuery(Long.class);
		Root<AuthorTO> root = critQuery.from(AuthorTO.class);
		critQuery.select(critBuilder.count(root));
		TypedQuery<Long> query = entityManager.createQuery(critQuery);
		return query.getSingleResult();
	}
}
