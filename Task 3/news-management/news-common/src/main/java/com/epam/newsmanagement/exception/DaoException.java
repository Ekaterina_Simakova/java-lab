package com.epam.newsmanagement.exception;

public class DaoException extends Exception {
	
	private static final long serialVersionUID = -5150040556657816439L;

	public DaoException(String message) {
		super(message);
	}
	
	public DaoException(String message, Exception hidden) {
		super(message,hidden);
	}
}
