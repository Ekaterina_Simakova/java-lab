package com.epam.newsmanagement.dao.impl.hibernate;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;

/*@Component*/
/*@Profile("hibernate")*/
public class AuthorDaoImpl implements AuthorDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<AuthorTO> getAllAuthors()  {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AuthorTO.class);
		criteria.add(Restrictions.isNull("expired"));
		return criteria.list();
	}

	@Override
	public Long addAuthor(AuthorTO author) {
		Session session = sessionFactory.getCurrentSession();
		return (Long) session.save(author);
	}

	@Override
	public void expireAuthorById(Long authorId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AuthorTO.class);
		criteria.add(Restrictions.eq("authorId", authorId));
		AuthorTO author = (AuthorTO) criteria.uniqueResult();
		Timestamp currentTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
		author.setExpired(currentTime);
		session.update(author);
	}

	@Override
	public void editAuthor(AuthorTO author) {
		Session session = sessionFactory.getCurrentSession();
		session.update(author);
		
	}

	@Override
	public List<AuthorTO> getAuthors(long start, long count) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AuthorTO.class);
		criteria.setFirstResult((int) start);
		criteria.setMaxResults((int) count);
		return criteria.list();
	}

	@Override
	public Long countAuthors() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AuthorTO.class);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

}
