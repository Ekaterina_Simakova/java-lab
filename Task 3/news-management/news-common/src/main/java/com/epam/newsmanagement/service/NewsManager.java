package com.epam.newsmanagement.service;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.entity.UserTO;

public interface NewsManager {

	/**
	 * Adds news with author and the list of tags.
	 * 
	 * @param news NewsVO object
	 * @return news id
	 * @ 
	 */
	Long addNews(NewsVO news);

	/**
	 * Edits news data.
	 * 
	 * @param news updated NewsTO object
	 * @ 
	 */
	void editNews(NewsVO news);

	/**
	 * Deletes news by ids.
	 * 
	 * @param newsId news id
	 * @ 
	 */
	void deleteNewsList(List<Long> newsIdList);
//
	/**
	 * Gets the list of news sorted accoding to SearchCriteria object and the number of page.
	 * 
	 * @return the list of NewsVO objects
	 * @ 
	 */
	List<NewsVO> getNews(SearchCriteria criteria, Long start, Long count);

	/**
	 * Gets full data of required news by its id.
	 * 
	 * @param newsId news id
	 * @return NewsVO object
	 * @ 
	 */
	NewsVO getNewsById(Long newsId);
	
	/**
	 * Gets the number of pages according to number of news at one page.
	 * 
	 * @param newsOnPage number of news at one page
	 * @return number of pages
	 * @ 
	 */
	 Long countNews(SearchCriteria criteria);

	/**
	 * Adds new author.
	 * 
	 * @param author AuthorTO object
	 * @return authors id
	 * @ 
	 */
	Long addAuthor(AuthorTO author);

	/**
	 * Makes required author expireded, it replaces removing him.
	 * 
	 * @param authorId author id
	 * @ 
	 */
	void expireAuthorById(Long authorId);
//
	/**
	 * Adds new comment.
	 * 
	 * @param comment CommentTO object
	 * @param newsId news id
	 * @return comments id
	 * @ 
	 */
	Long addComment(CommentTO comment);

	/**
	 * Deletes required comment by its id.
	 * 
	 * @param commentId comment id
	 * @ 
	 */
	void deleteComment(Long commentId);

	/**
	 * Adds tag.
	 * 
	 * @param tag TagTO object
	 * @param newsId news id
	 * @return tags id
	 * @ 
	 */
	Long addTag(TagTO tag);
	
	/**
	 * Edits tag.
	 * 
	 * @param tag
	 * @ 
	 */
	void editTag(TagTO tag);
	
	/**
	 * Gets all tags.
	 * 
	 * @return the list of tags
	 * @ 
	 */
	List<TagTO> getAllTags();
	
	List<TagTO> getTags(Long start, Long count);
	
	Long countTags();
	
	/**
	 * Gets all authors.
	 * 
	 * @return the list of authors
	 * @ 
	 */
	List<AuthorTO> getAllAuthors();
	
	/**
	 * Checks whether there are a next news and a previous news.
	 * 
	 * @param newsId
	 * @return result of checking in map
	 * @ 
	 */
	Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria);
	
	/**
	 * It edits author.
	 * 
	 * @param author AuthorTO object
	 * @ 
	 */
	void editAuthor(AuthorTO author);
	
	/**
	 * It deletes tag by id.
	 * 
	 * @param tagId
	 * @ 
	 */
	void deleteTagById(Long tagId);
	
	Long countAuthors();
	
	List<AuthorTO> getAuthors(long start, long count);
	
	UserTO getUserByName(String name, String password);

}
