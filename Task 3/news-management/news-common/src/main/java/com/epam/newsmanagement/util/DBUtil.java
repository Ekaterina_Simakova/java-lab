package com.epam.newsmanagement.util;

import java.util.logging.Logger;

public class DBUtil {
	
	private static final Logger logger = Logger.getLogger("connectionUtilLogger");

	/**
	 * Closes statement and releases connection from data source.
	 * 
	 * @param dataSource BasicDataSource object
	 * @param conn Connection object
	 * @param st Statement object
	 */
//	public static void closeResources(BasicDataSource dataSource, Connection conn, Statement st) {
//		if(st != null) {
//			try {
//				st.close();
//			} catch (SQLException e) {
//				logger.info("Closing Connection object failed", e);
//			}
//		}
//		if(conn != null) {
//			DataSourceUtils.releaseConnection(conn, dataSource);
//		}
//	}
	
	/**
	 * Closes statement, result set and releases connection from data source.
	 * 
	 * @param dataSource BasicDataSource object
	 * @param conn Connection object
	 * @param st Statement object
	 * @param rs ResultSet object
	 */
//	public static void closeResources(BasicDataSource dataSource, Connection conn, Statement st, ResultSet rs) {
//		if(rs != null) {
//			try {
//				rs.close();
//			} catch (SQLException e) {
//				logger.info("Closing ResultSet object failed", e);
//			}
//		}
//		closeResources(dataSource, conn, st);
//	}
}
