package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;

public interface CommentDao {
	
	/**
	 * Adds new comment.
	 * 
	 * @param CommentTO object
	 * @return generated comments id
	 * @
	 */
	Long addComment(CommentTO comment);
	
	/**
	 * Deletes comment by id.
	 * 
	 * @param commentId comments id
	 * @
	 */
	void deleteCommentById(Long commentId);

}
