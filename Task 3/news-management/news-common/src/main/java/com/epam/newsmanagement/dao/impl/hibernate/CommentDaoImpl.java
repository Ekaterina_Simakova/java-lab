package com.epam.newsmanagement.dao.impl.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;

/*@Component*/
/*@Profile("hibernate")*/
public class CommentDaoImpl implements CommentDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Long addComment(CommentTO comment) {
		Session session = sessionFactory.getCurrentSession();
		return (Long) session.save(comment);
	}

	@Override
	public void deleteCommentById(Long commentId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CommentTO.class);
		criteria.add(Restrictions.eq("commentId", commentId));
		CommentTO comment = (CommentTO) criteria.uniqueResult();
		session.delete(comment);
	}
}
