package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.PositionName;
import com.epam.newsmanagement.util.QueryBuilder;

public class NewsDaoImpl implements NewsDao {
	
	@Autowired
	private SessionFactory sessionFactory; 	
	
	@Override
	public Long addNews(NewsVO news)  {
		Session session = sessionFactory.getCurrentSession();
		return (Long) session.save(news);
	}

	@Override
	public void deleteNewsListByIds(List<Long> newsIdList)  {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(NewsVO.class);
		criteria.add(Restrictions.in("newsId", newsIdList));
		List<NewsVO> list = criteria.list();
		for(NewsVO news : list) {
			session.delete(news);
		}
	}

	@Override
	public void editNews(NewsVO news)  {
		Session session = sessionFactory.getCurrentSession();
		session.update(news);
	}

	@Override
	public List<NewsVO> getNews(SearchCriteria searchCriteria, long start, long count)  {
		Session session = sessionFactory.getCurrentSession();
		String queryString = QueryBuilder.buildQuerySearchNews(searchCriteria);
		Query query = session.createQuery(queryString);
		query.setFirstResult((int) start);
		query.setMaxResults((int) count);
		List<NewsVO> newsList = query.list();
		return newsList;
	}

	@Override
	public NewsVO getNewsById(Long id)  {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(NewsVO.class);
		criteria.add(Restrictions.like("newsId", id));
		return (NewsVO) criteria.uniqueResult();
	}

	@Override
	public Long countNews(SearchCriteria searchCriteria)  {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(NewsVO.class);
		if(searchCriteria != null) {
			if(searchCriteria.getAuthorId() != null) {
				criteria.add(Restrictions.eq("author.authorId", searchCriteria.getAuthorId()));
			}
			if(searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
				criteria.createAlias("tagList", "tagList");
				criteria.add(Restrictions.in("tagList.tagId", searchCriteria.getTagIdList()));
			}
		}
		criteria.setProjection((Projections.distinct(Projections.property("newsId"))));
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	@Override
	public Map<String, Long> getNextAndPrevious(Long newsId,SearchCriteria searchCriteria)  {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(NewsVO.class);
		criteria.addOrder(Order.desc("modificationDate"));
		if(searchCriteria != null) {
			if(searchCriteria.getAuthorId() != null) {
				criteria.add(Restrictions.like("author.authorId", searchCriteria.getAuthorId()));
			}
		}
		criteria.setProjection(Projections.property("newsId"));
		List<Long> idList = criteria.list();
		int position = idList.indexOf(newsId);
		Map<String,Long> idMap = new HashMap<String,Long>();
		if(idList.size() != position+1) {
			idMap.put(PositionName.NEXT, idList.get(position+1));
		}
		if(position != 0) {
			idMap.put(PositionName.PREVIOUS, idList.get(position-1));
		} 
		return idMap;
	}

	@Override
	public void detachTagById(Long tagId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TagTO.class);
		criteria.add(Restrictions.eq("tagId", tagId));
		TagTO tag = (TagTO) criteria.uniqueResult();
		criteria = session.createCriteria(NewsVO.class);
		criteria.createAlias("tagList", "tagList");
		criteria.add(Restrictions.eq("tagList.tagId", tagId));
		List<NewsVO> newsList = criteria.list();
		for(NewsVO news : newsList) {
			news.getTagList().remove(tag);
			session.update(news);
		}
	}

}
