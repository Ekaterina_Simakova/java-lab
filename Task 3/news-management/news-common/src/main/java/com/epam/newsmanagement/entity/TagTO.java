package com.epam.newsmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "TAG")
public class TagTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "TAG_ID")
	@SequenceGenerator(name="TAG_ID_SEQ", sequenceName="TAG_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAG_ID_SEQ")
	private Long tagId;
	
	@Column(name = "TAG_NAME")
	@NotNull(message="Size of tag is from 1 to 20!")
	@Size(min=1, max=20, message="Size of tag is from 1 to 20!")
	private String tagName;
	
	public Long getTagId() {
		return tagId;
	}
	
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TagTO other = (TagTO) obj;
		if (tagId == null) {
			if (other.tagId != null) {
				return false;
			}
		} else {
			if (!tagId.equals(other.tagId)) {
				return false;
			}
		}
		if (tagName == null) {
			if (other.tagName != null) {
				return false;
			}
		} else {
			if (!tagName.equals(other.tagName)) {
				return false;
			}
		}
		return true;
	}
}
