package com.epam.newsmanagement.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * @see AuthorService
 * @author Katsiaryna_Simakova
 *
 */
@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
	
	@Resource(name="authorDaoImpl")
	private AuthorDao authorDao;

	@Override
	public List<AuthorTO> getAllAuthors()  {
		return authorDao.getAllAuthors();
	}

	@Override
	public Long addAuthor(AuthorTO author)  {
		return authorDao.addAuthor(author);
	}

	@Override
	public void expireAuthorById(Long authorId)  {
		authorDao.expireAuthorById(authorId);		
	}

	@Override
	public void editAuthor(AuthorTO author)  {
		authorDao.editAuthor(author);
	}

	@Override
	public Long countAuthors() {
		return authorDao.countAuthors();
	}

	@Override
	public List<AuthorTO> getAuthors(long start, long count) {
		return authorDao.getAuthors(start, count);
	}

}
