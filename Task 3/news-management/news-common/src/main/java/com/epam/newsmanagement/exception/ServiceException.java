package com.epam.newsmanagement.exception;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 356276362288193233L;

	public ServiceException(String message) {
		super(message);
	}
	
	public ServiceException(String message, Exception hidden) {
		super(message, hidden);
	}

}
