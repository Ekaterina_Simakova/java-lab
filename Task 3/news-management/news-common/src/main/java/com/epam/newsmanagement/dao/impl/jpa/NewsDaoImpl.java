package com.epam.newsmanagement.dao.impl.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.util.PositionName;
import com.epam.newsmanagement.util.QueryBuilder;

public class NewsDaoImpl implements NewsDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long addNews(NewsVO news)  {
		entityManager.persist(news);
		entityManager.flush();
		return news.getNewsId();
	}

	@Override
	public void deleteNewsListByIds(List<Long> newsIdList)  {
		CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
		CriteriaQuery<NewsVO> critQuery = criteriaBuilder.createQuery(NewsVO.class);
		Root<NewsVO> root = critQuery.from(NewsVO.class);
		critQuery.where(root.get("newsId").in(newsIdList));
		TypedQuery<NewsVO> query = entityManager.createQuery(critQuery);
		List<NewsVO> newsList = query.getResultList();
		for(NewsVO curNews : newsList) {
			entityManager.remove(curNews);
		}
	}

	@Override
	public void editNews(NewsVO news)  {
		entityManager.merge(news);		
	}

	@Override
	public List<NewsVO> getNews(SearchCriteria criteria, long start, long count)  {
		String queryString = QueryBuilder.buildQuerySearchNews(criteria);
		Query query = entityManager.createQuery(queryString);
		query.setFirstResult((int) start);
		query.setMaxResults((int) count);
		return query.getResultList();
	}

	@Override
	public NewsVO getNewsById(Long id)  {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<NewsVO> critQuery = critBuilder.createQuery(NewsVO.class);
		Root<NewsVO> root = critQuery.from(NewsVO.class);
		critQuery.where(critBuilder.equal(root.get("newsId"), id));
		TypedQuery<NewsVO> query = entityManager.createQuery(critQuery);
		return query.getSingleResult();
	}

	@Override
	public Long countNews(SearchCriteria criteria)  {
		String queryStr = QueryBuilder.buildQueryGetCount(criteria);
		Query query = entityManager.createQuery(queryStr);
		return (Long) query.getSingleResult();
	}

	@Override
	public Map<String, Long> getNextAndPrevious(Long newsId, SearchCriteria criteria)  {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<NewsVO> critQuery = critBuilder.createQuery(NewsVO.class);
		Root<NewsVO> root = critQuery.from(NewsVO.class);
		if(criteria != null) {
			if(criteria.getTagIdList() != null && !criteria.getTagIdList().isEmpty()) {
				Join<NewsVO,TagTO> join = root.join("tagList", JoinType.INNER);
				critQuery.where(join.get("tagId").in(criteria.getTagIdList()));
			}
			if(criteria.getAuthorId() != null) {
				critQuery.where(critBuilder.equal(root.get("author").get("authorId"), criteria.getAuthorId()));
			}
		}
		Expression<Integer> commentSize = critBuilder.size(root.get("commentList"));
		Expression<Object> date = root.get("modificationDate");
		critQuery.orderBy(critBuilder.desc(date), critBuilder.desc(commentSize));
		critQuery.select(root.get("newsId"));
		Query query = entityManager.createQuery(critQuery);
		List<Long> idList = query.getResultList();
		int position = idList.indexOf(newsId);
		Map<String,Long> idMap = new HashMap<String,Long>();
		if(idList.size() != position+1) {
			idMap.put(PositionName.NEXT, idList.get(position+1));
		}
		if(position != 0) {
			idMap.put(PositionName.PREVIOUS, idList.get(position-1));
		} 
		return idMap;
	}

	@Override
	public void detachTagById(Long tagId) {
		CriteriaBuilder critBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TagTO> tagCritQuery = critBuilder.createQuery(TagTO.class);
		Root<TagTO> tagRoot = tagCritQuery.from(TagTO.class);
		tagCritQuery.where(critBuilder.equal(tagRoot.get("tagId"), tagId));
		TypedQuery<TagTO> tagTypedQuery = entityManager.createQuery(tagCritQuery);
		TagTO tag = tagTypedQuery.getSingleResult();
		CriteriaQuery<NewsVO> critQuery = critBuilder.createQuery(NewsVO.class);
		Root<NewsVO> root = critQuery.from(NewsVO.class);
		critQuery.where(critBuilder.isMember(tag, root.get("tagList")));
		TypedQuery<NewsVO> typedQuery = entityManager.createQuery(critQuery);
		List<NewsVO> newsList = typedQuery.getResultList();
		for(NewsVO news : newsList) {
			news.getTagList().remove(tag);
			entityManager.merge(news);
		}
	}
	
}
