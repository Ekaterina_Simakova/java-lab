package com.epam.newsmanagement.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * @see TagService
 * @author Katsiaryna_Simakova
 *
 */
@Service
@Transactional
public class TagServiceImpl implements TagService {
	
	@Resource(name="tagDaoImpl")
	private TagDao tagDao;

	@Override
	public List<TagTO> getAllTags() {
		return tagDao.getAllTags();
	}

	@Override
	public void deleteTagById(Long tagId) {
		tagDao.deleteTagById(tagId);
	}

	@Override
	public Long addTag(TagTO tag) {
		return tagDao.addTag(tag);
	}

	@Override
	public void editTag(TagTO tag) {
		tagDao.editTag(tag);
	}

	@Override
	public List<TagTO> getTags(Long start, Long count) {
		return tagDao.getTags(start, count);
	}

	@Override
	public Long countTags() {
		return tagDao.countTags();
	}
	
}