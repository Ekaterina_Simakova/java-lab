package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.UserTO;

public interface UserDao {
	
	UserTO getUserByName(String name, String pasword);

}
