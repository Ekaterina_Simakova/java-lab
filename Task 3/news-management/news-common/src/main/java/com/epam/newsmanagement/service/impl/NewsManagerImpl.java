package com.epam.newsmanagement.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.UserService;

@Service
public class NewsManagerImpl implements NewsManager {

	@Autowired
	private TagService tagService;
	
	@Autowired 
	private AuthorService authorService;
	
	@Autowired 
	private NewsService newsService;
	
	@Autowired 
	private CommentService commentService;
	
	@Autowired
	private UserService userService;

	public List<TagTO> getAllTags() {
		return tagService.getAllTags();
	}

	@Override
	public List<AuthorTO> getAllAuthors()  {
		return authorService.getAllAuthors();
	}

	@Override
	public List<NewsVO> getNews(SearchCriteria criteria, Long start, Long count)  {
		return newsService.getNews(criteria, start, count);
	}

	@Override
	public Long countNews(SearchCriteria criteria)  {
		return newsService.countNews(criteria);
	}

	@Override
	public NewsVO getNewsById(Long newsId)  {
		return newsService.getNewsById(newsId);
	}

	@Override
	public Long addComment(CommentTO comment)  {
		return commentService.addComment(comment);
	}

	@Override
	public Map<String, Long> getNextAndPrevious(Long newsId, SearchCriteria criteria)  {
		return newsService.getNextAndPrevious(newsId, criteria);
	}

	@Override
	public void deleteTagById(Long tagId)  {
		newsService.detachTagById(tagId);
		tagService.deleteTagById(tagId);
	}

	@Override
	public Long addTag(TagTO tag)  {
		return tagService.addTag(tag);
	}

	@Override
	public void editTag(TagTO tag)  {
		tagService.editTag(tag);
	}

	@Override
	public Long addAuthor(AuthorTO author)  {
		return authorService.addAuthor(author);
	}

	@Override
	public void expireAuthorById(Long authorId)  {
		authorService.expireAuthorById(authorId);
	}

	@Override
	public void editAuthor(AuthorTO author)  {
		authorService.editAuthor(author);
	}

	@Override
	public void deleteComment(Long commentId)  {
		commentService.deleteCommentById(commentId);
	}

	@Override
	public void editNews(NewsVO news)  {
		newsService.editNews(news);
	}

	@Override
	public void deleteNewsList(List<Long> newsIdList)  {
		newsService.deleteNewsListByIds(newsIdList);
	}

	@Override
	public Long addNews(NewsVO news)  {
		return newsService.addNews(news);
	}

	@Override
	public List<TagTO> getTags(Long start, Long count) {
		return tagService.getTags(start, count);
	}

	@Override
	public Long countTags() {
		return tagService.countTags();
	}

	@Override
	public Long countAuthors() {
		return authorService.countAuthors();
	}

	@Override
	public List<AuthorTO> getAuthors(long start, long count) {
		return authorService.getAuthors(start, count);
	}

	@Override
	public UserTO getUserByName(String name, String password) {
		return userService.getUserByName(name, password);
	}

}
