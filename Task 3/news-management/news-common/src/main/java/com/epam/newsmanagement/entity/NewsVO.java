package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="NEWS")
@PrimaryKeyJoinColumn(name="NEWS_ID", referencedColumnName="NEWS_ID")
public class NewsVO extends NewsTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="NEWS_TAG", 
				joinColumns=@JoinColumn(name="NEWS_ID"),
				inverseJoinColumns={@JoinColumn(name="TAG_ID")})
	private List<TagTO> tagList;

	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="newsId")
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("creationDate DESC")
	private List<CommentTO> commentList;
	
	@NotNull(message = "Choose author!")
	@OneToOne
	@JoinTable(name="NEWS_AUTHOR",
				joinColumns=@JoinColumn(name="NEWS_ID"),
				inverseJoinColumns=@JoinColumn(name="AUTHOR_ID"))
	private AuthorTO author;

	public List<TagTO> getTagList() {
		return tagList;
	}

	public void setTagList(List<TagTO> tagList) {
		this.tagList = tagList;
	}

	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	public List<CommentTO> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<CommentTO> commentList) {
		this.commentList = commentList;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null) {
				return false;
			}
		} else {
			if (!author.equals(other.author)) {
				return false;
			}
		}
		if (commentList == null) {
			if (other.commentList != null) {
				return false;
			}
		} else {
			if (!commentList.equals(other.commentList)) {
				return false;
			}
		}
		if (tagList == null) {
			if (other.tagList != null) {
				return false;
			}
		} else { 
			if (!tagList.equals(other.tagList)) {
				return false;
			}
		}
		return true;
	}
	
}
