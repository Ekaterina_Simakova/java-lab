package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;

/**
 * @see TagDao
 */
/*@Component*/
/*@Profile("hibernate")*/
public class TagDaoImpl implements TagDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<TagTO> getAllTags() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TagTO.class);
		return criteria.list();
	}

	@Override
	public void deleteTagById(Long tagId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TagTO.class);
		criteria.add(Restrictions.eq("tagId", tagId));
		TagTO tag = (TagTO) criteria.uniqueResult();
		session.delete(tag);		
	}
	
	@Override
	public void editTag(TagTO tag) {
		Session session = sessionFactory.getCurrentSession();
		session.update(tag);
	}
	
	@Override
	public Long addTag(TagTO tag) {
		Session session = sessionFactory.getCurrentSession();
		return (Long) session.save(tag);
	}

	@Override
	public List<TagTO> getTags(long start, long count) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TagTO.class);
		criteria.setFirstResult((int)start);
		criteria.setMaxResults((int)count);
		return criteria.list();
	}

	@Override
	public Long countTags() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TagTO.class);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	
}
