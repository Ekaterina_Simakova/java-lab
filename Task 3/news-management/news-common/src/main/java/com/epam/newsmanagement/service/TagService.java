package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface TagService {
	
	/**
	 * Method adds tag.
	 * 
	 * @param tag TagTO object
	 * @return tags id
	 * @
	 */
	Long addTag(TagTO tag);
	
	/**
	 * Method edits tag data.
	 * 
	 * @param tag updated TagTO object
	 * @
	 */
	void editTag(TagTO tag);
	
	List<TagTO> getTags(Long start, Long count);
	
	Long countTags();
	
	/**
	 * It gets all tags.
	 * 
	 * @return the list of tags
	 * @
	 */
	List<TagTO> getAllTags();
	
	/**
	 * Method deletes tag by id.
	 * 
	 * @param tagId tag id
	 * @
	 */
	void deleteTagById(Long tagId);

}
