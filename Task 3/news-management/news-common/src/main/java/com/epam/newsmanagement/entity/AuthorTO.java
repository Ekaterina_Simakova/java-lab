package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="AUTHOR")
@SequenceGenerator(name="AUTHOR_ID_SEQ")
public class AuthorTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "AUTHOR_ID")
	@SequenceGenerator(name="AUTHOR_ID_SEQ", sequenceName="AUTHOR_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUTHOR_ID_SEQ")
	private Long authorId;
	
	@Column(name = "AUTHOR_NAME")
	@NotNull(message="Size of author's name is from 1 to 30!")
	@Size(min=1, max=30, message="Size of author's name is from 1 to 30!")
	private String authorName;
	
	@Column(name = "EXPIRED")
	private Timestamp expired;
	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AuthorTO other = (AuthorTO) obj;
		if(authorId != null) {
			if(!authorId.equals(other.getAuthorId())) {
				return false;
			}
		} else {
			if(other.getAuthorId() != null) {
				return false;
			}
		}
		if(authorName != null) {
			if(!authorName.equals(other.getAuthorName())) {
				return false;
			}
		} else {
			if(other.getAuthorName() != null) {
				return false;
			}
		}
		if(expired != null) {
			if(!expired.equals(other.getExpired())) {
				return false;
			}
		} else {
			if(other.getExpired() != null) {
				return false;
			}
		}
		return true;
	}
}
