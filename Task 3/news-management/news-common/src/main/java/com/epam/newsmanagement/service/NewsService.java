package com.epam.newsmanagement.service;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsService {

	/**
	 * Adds news.
	 * 
	 * @param news NewsTO object
	 * @return news id
	 * @
	 */
	Long addNews(NewsVO news) ;

	/**
	 * Edits news data.
	 * 
	 * @param news updated NewsTO object
	 * @
	 */
	void editNews(NewsVO news) ;
	
	/**
	 * Deletes the list of news.
	 * 
	 * @param newsIdList the list of news ids
	 * @
	 */
	void deleteNewsListByIds(List<Long> newsIdList) ;

	/**
	 * Gets news by id.
	 * 
	 * @param newsId news id
	 * @return NewsTO object
	 * @
	 */
	NewsVO getNewsById(Long newsId) ;

	/**
	 * Gets the count of news according to SearchCriteria object.
	 * 
	 * @return the count of news
	 * @
	 */
	Long countNews(SearchCriteria criteria) ;
	
	/**
	 * Gets sorted by modification date and number of comments the list of news.
	 * 
	 * @return the list of NewsTO objects
	 * @
	 */
	List<NewsVO> getNews(SearchCriteria criteria, long start, long count) ; 

	/**
	 * Checks whether there are a next news and a previous news.
	 * 
	 * @param newsId
	 * @return result of checking in map
	 * @
	 */
	Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) ;
	
	void detachTagById(Long tagId);

}
