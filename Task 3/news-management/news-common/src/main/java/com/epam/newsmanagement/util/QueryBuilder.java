package com.epam.newsmanagement.util;

import com.epam.newsmanagement.entity.SearchCriteria;

public class QueryBuilder {
	
	private static final String HQL_GET_COUNT = " select count(n) from NewsVO n ";
	private static final String HQL_GET_NEWS = " select distinct n from NewsVO n ";
	private static final String HQL_JOIN_TAGS= " join n.tagList tags ";
	private static final String HQL_AUTHOR_CONDITION = " n.author.authorId = ";
	private static final String HQL_WHERE = " where ";
	private static final String HQL_AND = " and ";
	private static final String HQL_NEWS_ORDER = " order by n.modificationDate desc, n.commentList.size desc ";
	private static final String HQL_TAG_CONDITION = " tags.tagId in (";
	private static final String HQL_GET_NEWS_IDS = " select distinct n.newsId from NewsVO n ";
	
	public static String buildQuerySearchNews(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(HQL_GET_NEWS);
		query.append(buildSearchCriteriaCondition(criteria));
		query.append(HQL_NEWS_ORDER);
		return query.toString();
	}	
	
	public static String buildQueryGetCount(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(HQL_GET_COUNT);
		query.append(buildSearchCriteriaCondition(criteria));
		return query.toString();
	}
	
	public static String buildQueryGetSortedNewsIds(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(HQL_GET_NEWS_IDS);
		query.append(buildSearchCriteriaCondition(criteria));
		query.append(HQL_NEWS_ORDER);
		return query.toString();
	}
	
	private static String buildSearchCriteriaCondition(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder();
		if(criteria != null) {
			if(criteria.getTagIdList() != null && !criteria.getTagIdList().isEmpty()) {
				query.append(HQL_JOIN_TAGS);
				query.append(HQL_WHERE);
				query.append(HQL_TAG_CONDITION);
				for(Long tagId:criteria.getTagIdList()) {
					query.append(tagId);
					query.append(",");
				}
				if(!criteria.getTagIdList().isEmpty()) {
					query.deleteCharAt(query.length()-1);
				}
				query.append(")");
			}
			if(criteria.getAuthorId() != null) {
				if(criteria.getTagIdList() != null && !criteria.getTagIdList().isEmpty()) {
					query.append(HQL_AND);
				} else {
					query.append(HQL_WHERE);
				}
				query.append(HQL_AUTHOR_CONDITION);
				query.append(criteria.getAuthorId());
			}
		}
		return query.toString();
	}
}
