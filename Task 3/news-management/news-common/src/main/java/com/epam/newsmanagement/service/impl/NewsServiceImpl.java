package com.epam.newsmanagement.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * @see NewsService
 * @author Katsiaryna_Simakova
 *
 */
@Service
@Transactional
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	private NewsDao newsDao;

	@Override
	public Long addNews(NewsVO news)  {
		return newsDao.addNews(news);
	}

	@Override
	public void editNews(NewsVO news)  {
		newsDao.editNews(news);
	}

	@Override
	public NewsVO getNewsById(Long newsId)  {
		return newsDao.getNewsById(newsId);
	}

	@Override
	public Long countNews(SearchCriteria criteria)  {
		return newsDao.countNews(criteria);
	}

	@Override
	public List<NewsVO> getNews(SearchCriteria criteria, long start, long count)  {
		return newsDao.getNews(criteria, start, count);
	}

	@Override
	public Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria)  {
		return newsDao.getNextAndPrevious(newsId, criteria);
	}

	@Override
	public void deleteNewsListByIds(List<Long> newsIdList)  {
		newsDao.deleteNewsListByIds(newsIdList);
	}

	@Override
	public void detachTagById(Long tagId) {
		newsDao.detachTagById(tagId);
	}

}
