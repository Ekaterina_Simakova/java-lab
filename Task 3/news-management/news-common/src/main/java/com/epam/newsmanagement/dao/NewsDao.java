package com.epam.newsmanagement.dao;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsDao {
	
	/**
	 * Adds news, i.e. <code>NewsTO</code> object.
	 * 
	 * @param news <code>NewsTO</code> object
	 * @return generated news id
	 * @
	 */
	Long addNews(NewsVO news) ;
	
	/**
	 * Deletes the list of news.
	 * 
	 * @param newsIdList the list of news ids
	 * @throws ServiceException
	 */
	void deleteNewsListByIds(List<Long> newsIdList) ;
	
	/**
	 * Edits news information.
	 * 
	 * @param news updated <code>NewsTO</code> object
	 * @
	 */
	void editNews(NewsVO news) ;
	
	/**
	 * Gets list of news accoding to SearchCriteria object.
	 * 
	 * @return list of <code>NewsTO</code> objects
	 * @
	 */
	List<NewsVO> getNews(SearchCriteria criteria, long start, long count) ;
	
	/**
	 * Gets news data by its id.
	 * 
	 * @param id required news id
	 * @return <code>NewsTO</code> object
	 * @ when getting will be failed
	 */
	NewsVO getNewsById(Long id) ;
	
	/**
	 * Gets the number of news accordin to SearchCriteria object.
	 * 
	 * @return the number of news
	 * @
	 */
	Long countNews(SearchCriteria criteria) ;
	
	/**
	 * Checks whether there are a next news and a previous news.
	 * 
	 * @param newsId
	 * @return result of checking in map
	 * @
	 */
	Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) ;
	
	void detachTagById(Long tagId);
	
}
