function enableForm(formId) {
	var disInputs = document.getElementById(formId).elements;
	for(var i=0; i<disInputs.length; i++) {
		if(disInputs[i].getAttribute("disabled") != undefined) {
			disInputs[i].removeAttribute("disabled");
		}
	}
	$('#' + formId + '_base_links').addClass("hide");
	$('#' + formId + '_addit_links').removeClass("hide");
	return false;
}

function disableForm(formId) {
	var inputs = document.getElementById(formId).elements;
	for(var i=0; i<inputs.length; i++) {
		if(inputs[i].getAttribute("disabled") == undefined) {
			inputs[i].setAttribute("disabled","disabled");
		}
	}
	$("#" + formId + "_base_links").removeClass("hide");
	$("#" + formId + "_addit_links").addClass("hide");
}

function updateTagName(tagId) {
	$("#tag_" + tagId + "_tagNameError").empty();
	name = $("#tag_" + tagId + "_tagName").val();
	nameLength = name.length;
	if( nameLength<1 || nameLength>20 ) {
		$("#tag_" + tagId + "_tagNameError").append("Size of tag is from 1 to 20!");
		$('#cancel_href_' + tagId).addClass("hide");
		$('#delete_href_' + tagId).addClass("hide");
		return false;
	} else {
		$("#tag_" + tagId).submit();
		return false;
	}
}

function updateAuthorName(authorId) {
	$("#author_" + authorId + "_authorNameError").empty();
	name = $("#author" + authorId + "_authorName").val();
	nameLength = name.length;
	if( nameLength<1 || nameLength>30 ) {
		$("#author_" + authorId + "_authorNameError").append("Size of author's name is from 1 to 30!");
		$('#cancel_href_' + authorId).addClass("hide");
		$('#delete_href_' + authorId).addClass("hide");
		return false;
	} else {
		$("#author_" + authorId).submit();
		return false;
	}
}

function showTagList(event) {
	$("#tagList").removeClass("hide");
	event.preventDefault();
	event.stopPropagation();
	return false;
}

function hideTagList(event) {
	$("#tagList").addClass("hide");
}

function showAuthorList(event) {
	$("#authorList").removeClass("hide");
	event.preventDefault();
	event.stopPropagation();
	return false;
}

function hideAuthorList(event) {
	$("#authorList").addClass("hide");
}

function criteriaOnLoad() {
	var selectAuthorId = $("#criteria_author").val();
	var selectTagId = document.getElementsByClassName("criteria_tags");
	$('#option_author_' + selectAuthorId).attr("selected", "selected");
	for (var i = 0; i < selectTagId.length; i++) {
		$('#check_tag_' + selectTagId[i].value).attr("checked", "checked");
	}
}

function disableAuthor(authorId) {
	var idForm = '#author_' + authorId;
	$(idForm + '_authorName').attr("disabled", "true");
	$('#addit_links_' + authorId).addClass("hide");
	$('#base_links_' + authorId).removeClass("hide");
}

function checkTags() {
	var newsTags = document.getElementsByClassName("news_tag");
	for (var i = 0; i < newsTags.length; i++) {
		$("#tag_" + newsTags[i].value).attr("checked", "checked");
	}
}

function validNews(validation_url) {
	var result = true;
	var lastErrors = document.getElementsByClassName("error");
	for(var i=0; i<lastErrors.length; i++) {
		while(lastErrors[i].firstChild) {
			lastErrors[i].removeChild(lastErrors[i].firstChild);
		}
	}
	var errors = validFormAjax("news",validation_url);
	if(errors.length != 0) {
		for(var i=0; i<errors.length; i++) {
			$("#" + errors[i].field + "_error").append(errors[i].defaultMessage);
		}
		result = false;
	}
	var text_date = $("#date_text").val();
	var date_format = $("#dateformat").val();
	var momentDate = moment(text_date, date_format);
	var date = momentDate.toDate();
	var month = date.getMonth()+1;
	if(month < 10) {
		month = '0'+month;
	}
	var day = date.getDate();
	if(day < 10) {
		day = '0'+day;
	}
	var str = date.getFullYear() + '-' + month + '-' + day;
	document.getElementById("input_date").value = str;
	if(document.getElementById("input_date").value == "") {
		$("#date_error").append("Invalid date!");
		result = false;
	}
	return result;
}

function validAndSubmitForm(validation_url, formId) {
	var lastErrors = document.getElementsByClassName("error");
	for(var i=0; i<lastErrors.length; i++) {
		while(lastErrors[i].firstChild) {
			lastErrors[i].removeChild(lastErrors[i].firstChild);
		}
	}
	var errors = validFormAjax(formId,validation_url);
	if(errors.length != 0) {
		for(var i=0; i<errors.length; i++) {
			$("#" + errors[i].field + "_error").append(errors[i].defaultMessage);
		}
		return false;
	} else {
		$("#" + formId).submit();
		return false;
	}
}

function validFormAjax(form_id, url) {
	var result;
	$.ajax({
		url : url,
		type : 'POST',
		data : $("#" + form_id).serialize(),
		dataType : "json",
		async: false,  
		success: function(data) {
			result = data; 
		}
	});
	return result;
}

function confirmDeleting(message) {
	var count = 0;
	$("input:checkbox[name=newsId]:checked").each(function(){
	    count++;
	});
	var result = false;
	if(count != 0) {
		result = confirm(message);
	}
	return result;
}

