<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<div class="header">
	<tiles:insertAttribute name="header" />
</div>
<div class="content_full_width">
	<tiles:insertAttribute name="content" />
</div>
<div class="footer">
	<tiles:insertAttribute name="footer" />
</div>
