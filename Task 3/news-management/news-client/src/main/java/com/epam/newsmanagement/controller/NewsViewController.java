package com.epam.newsmanagement.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.util.PositionName;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.SessionAttributeName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller is designed to work with newsView.jsp page.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class NewsViewController {

	@Autowired
	NewsManagerImpl newsManager;

	/**
	 * Method gets NewsTO object and assembles ModelAndView object.
	 * It puts NewsTO object, next and previous news ids and CommentTO object to model.
	 * 
	 * @param newsId news id
	 * @param session HttpSession object
	 * @return ModelAndView object
	 * @throws ServiceException
	 */
	@RequestMapping(value = "view/{newsId}")
	public ModelAndView showNews(@PathVariable Long newsId, HttpSession session) throws ServiceException {
		NewsVO news = newsManager.getNewsById(newsId);
		SearchCriteria criteria = (SearchCriteria) session.getAttribute(SessionAttributeName.SEARCH_CRITERIA);
		Map<String,Long> idMap = newsManager.getNextAndPrevious(news.getNewsId(), criteria);
		CommentTO comment = new CommentTO();
		comment.setNewsId(news.getNewsId());
		ModelAndView modelView = new ModelAndView(ViewName.NEWS_VIEW);
		modelView.addObject(ModelAttributeName.NEWS, news);
		modelView.addObject(ModelAttributeName.COMMENT, comment);
		modelView.addObject(ModelAttributeName.NEXT_ID, idMap.get(PositionName.NEXT));
		modelView.addObject(ModelAttributeName.PREVIOUS_ID, idMap.get(PositionName.PREVIOUS));
		return modelView;
	}

}
