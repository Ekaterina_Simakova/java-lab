package com.epam.newsmanagement.utils;

public class Calculator {
	
	public static Long getPageCount(Long newsOnPage, Long newsNumber) {
		long pageNumber = newsNumber/newsOnPage;
		if(newsNumber%newsOnPage != 0) {
			pageNumber++;
		}
		return pageNumber;
	}
	
	public static Long getStartNumber(long numberNewsOnPage, long pageNumber) {
		return numberNewsOnPage*(pageNumber-1);
	}

}
