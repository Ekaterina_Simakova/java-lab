package com.epam.newsmanagement.controller;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.utils.Calculator;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.PropertiesGetter;
import com.epam.newsmanagement.utils.SessionAttributeName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller is designed to work with newsList.jsp page.
 * It provides methods for operations with list of NewsTO objects and SearchCriteria object.
 * Available operations are getting the list of news for page according to SearchCriteria object, changing
 * SearchCriteria object.
 * Each method throws ServiceException.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class NewsListController {

	@Autowired
	private NewsManagerImpl newsManager;
	
	private static final long FIRST_PAGE_NUMBER = 1L;

	@RequestMapping(value={"newslist","/"}, method=RequestMethod.GET)
	public String showFirstPage(HttpSession session) throws ServiceException {
		return "redirect:/newslist/page/" + FIRST_PAGE_NUMBER;
	}

	@RequestMapping(value = "newslist", method=RequestMethod.POST)
	public ModelAndView filterNews(@ModelAttribute(ModelAttributeName.SEARCH_CRITERIA) SearchCriteria criteria,
									HttpSession session) throws ServiceException {
		session.setAttribute(SessionAttributeName.SEARCH_CRITERIA, criteria);
		return getNewsListPage(FIRST_PAGE_NUMBER, session);
	}
	
	@RequestMapping(value="newslist/page/{pageNumber}", method=RequestMethod.GET)
	public ModelAndView showNewsListPage(@PathVariable Long pageNumber, HttpSession session) throws ServiceException {
		return getNewsListPage(pageNumber, session);
	}
	
	@RequestMapping(value="newslist/reset", method=RequestMethod.GET)
	public String resetCriteria(HttpSession session) {
		session.setAttribute(SessionAttributeName.SEARCH_CRITERIA, new SearchCriteria());
		return "redirect:/newslist/page/" + FIRST_PAGE_NUMBER;
	}
	
	private ModelAndView getNewsListPage(Long page, HttpSession session) throws ServiceException {
		/*Long newsCount = (Long) session.getAttribute(SessionAttributeName.NUMBER_OF_NEWS_ON_PAGE);
		if(newsCount == null) {
			newsCount = PropertiesGetter.getNewsCount();
			session.setAttribute(SessionAttributeName.NUMBER_OF_NEWS_ON_PAGE, newsCount);
		}
		SearchCriteria criteria = (SearchCriteria) session.getAttribute(SessionAttributeName.SEARCH_CRITERIA);
		if(criteria == null) {
			criteria = new SearchCriteria();
		}
		session.setAttribute(SessionAttributeName.CURRENT_PAGE_NUMBER, pageNumber);
		long start = Calculator.getStartNumber(newsCount, pageNumber);
		List<NewsVO> newsList = newsManager.getNews(criteria, start, newsCount);
		Long allNewsCount = newsManager.countNews(criteria);
		Long numberOfPages = Calculator.getPageCount(newsCount, allNewsCount);
		List<AuthorTO> authorList = newsManager.getAllAuthors();
		List<TagTO> tagList = newsManager.getAllTags();
		ModelAndView model = new ModelAndView(ViewName.NEWS_LIST, ModelAttributeName.NEWS_LIST, newsList);
		model.addObject(ModelAttributeName.NUMBER_OF_PAGES, numberOfPages);
		model.addObject(ModelAttributeName.AUTHOR_LIST, authorList);
		model.addObject(ModelAttributeName.TAG_LIST, tagList);
		model.addObject(ModelAttributeName.SEARCH_CRITERIA, criteria);
		return model;*/
		long newsCount = PropertiesGetter.getNewsCount();
		long paginationSize = PropertiesGetter.getPagesCount();
		SearchCriteria criteria = (SearchCriteria) session.getAttribute(SessionAttributeName.SEARCH_CRITERIA);
		if(criteria == null) {
			criteria = new SearchCriteria();
		}
		session.setAttribute(SessionAttributeName.PAGE, page);
		long start = Calculator.getStartNumber(newsCount, page);
		List<NewsVO> newsList = newsManager.getNews(criteria, start, newsCount);
		Long allNewsCount = newsManager.countNews(criteria);
		Long numberOfPages = Calculator.getPageCount(newsCount, allNewsCount);
		List<AuthorTO> authorList = newsManager.getAllAuthors();
		List<TagTO> tagList = newsManager.getAllTags();
		ModelAndView model = new ModelAndView(ViewName.NEWS_LIST, ModelAttributeName.NEWS_LIST, newsList);
		model.addObject(ModelAttributeName.NUMBER_OF_PAGES, numberOfPages);
		model.addObject(ModelAttributeName.AUTHOR_LIST, authorList);
		model.addObject(ModelAttributeName.TAG_LIST, tagList);
		model.addObject(ModelAttributeName.SEARCH_CRITERIA, criteria);
		model.addObject(ModelAttributeName.PAGINATION_SIZE, paginationSize);
		return model;
	}	
}
