package com.epam.newsmanagement.utils;

public class ViewName {
	
	public static final String NEWS_LIST = "newslist";
	public static final String NEWS_VIEW = "view";
	public static final String ERROR = "error";

}
