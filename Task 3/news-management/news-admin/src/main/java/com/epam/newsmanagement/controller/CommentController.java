package com.epam.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;

/**
 * Controller is designed to operation with comments at newsView.jsp page.
 * It provides methods for operation with CommentTO object.
 * Available operations are adding and deleting.
 * Each method throws ServiceException. 
 * Each method returns the name of view.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class CommentController {
	
	@Autowired
	private NewsManagerImpl newsManager;
	
	@RequestMapping(value="view/addcomment")
	public String postComment(CommentTO comment) throws ServiceException {
		comment.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		newsManager.addComment(comment);
		return "redirect:/view/".concat(comment.getNewsId().toString());
	}

	@RequestMapping(value="view/{newsId}/deletecomment/{commentId}")
	public String deleteComment(@PathVariable Long newsId, @PathVariable Long commentId) throws ServiceException {
		newsManager.deleteComment(commentId);
		return "redirect:/view/".concat(newsId.toString());
	}

}
