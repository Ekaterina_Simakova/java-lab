package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.utils.ViewName;

@Controller
public class SecurityController {
	
	/**
	 * Method returns the name of login page.
	 * 
	 * @return view name
	 */
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String showLoginPage() {
		return ViewName.LOGIN;
	}

}
