package com.epam.newsmanagement.utils;

import java.util.ResourceBundle;

public class PropertiesGetter {
	
	public static Long getNewsCount() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("user");
		return Long.valueOf(resourceBundle.getString("news_count"));
	}
	
	public static Long getPagesCount() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("user");
		return Long.valueOf(resourceBundle.getString("pages_count"));
	}
	
	public static int getTagsCount() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("user");
		return Integer.valueOf(resourceBundle.getString("tags_count"));
	}
	
	public static int getAuthorsCount() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("user");
		return Integer.valueOf(resourceBundle.getString("authors_count"));
	}

}
