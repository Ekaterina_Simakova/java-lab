package com.epam.newsmanagement.utils;

public class SessionAttributeName {
	
	public static final String SEARCH_CRITERIA = "searchCriteria";
	public static final String PAGE = "page";
	
}
