package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.utils.Calculator;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.PropertiesGetter;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller is designed to work with tags.jsp page.
 * It provides methods for operating with TagTO object.
 * Available operations are getting, adding, updating, deleting.
 * Each method throws ServiceException.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class TagController {
	
	@Autowired
	NewsManagerImpl newsManager;
	
	private static final long FIRST_PAGE = 1l;
	
	@RequestMapping(value="tags")
	public String getTags() throws ServiceException {
		return "redirect:tags/page/" + FIRST_PAGE;
	}

	
	@RequestMapping(value="tags/page/{page}")
	public ModelAndView getTags(@PathVariable Long page) throws ServiceException {
		return getTagModelView(page);
	}
	
	@RequestMapping(value="tags/add")
	public String addTag(@ModelAttribute(ModelAttributeName.TAG) TagTO tag) throws ServiceException {
		newsManager.addTag(tag);
		return "redirect:/tags";
	}
	
	@RequestMapping(value="tags/page/{page}/update/{tagId}")
	public String updateTag(@PathVariable("tagId") Long tagId, @RequestParam("tagName") String tagName, @PathVariable Long page) throws ServiceException {
		TagTO tag = new TagTO();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		newsManager.editTag(tag);
		return "redirect:/tags/page/" + page;
	}
	
	@RequestMapping(value="tags/delete/{tagId}")
	public String deleteTag(@PathVariable Long tagId) throws ServiceException {
		newsManager.deleteTagById(tagId);
		return "redirect:/tags";
	}
	
	private ModelAndView getTagModelView(long page) {
		ModelAndView modelView = new ModelAndView(ViewName.TAGS);
		long count = PropertiesGetter.getTagsCount();
		long start = Calculator.getStartNumber(count, page);
		long paginationSize = PropertiesGetter.getPagesCount();
		List<TagTO> tagList = newsManager.getTags(start, count);
		long allTagsCount = newsManager.countTags();
		long pagesCount = Calculator.getPageCount(count, allTagsCount);
		modelView.addObject(ModelAttributeName.TAG_LIST, tagList);
		modelView.addObject(ModelAttributeName.TAG, new TagTO());
		modelView.addObject(ModelAttributeName.NUMBER_OF_PAGES, pagesCount);
		modelView.addObject(ModelAttributeName.PAGE, page);
		modelView.addObject(ModelAttributeName.PAGINATION_SIZE, paginationSize);
		return modelView;
	}

}
