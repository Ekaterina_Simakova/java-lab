package com.epam.newsmanagement.utils;

public class Calculator {
	
	public static long getPageCount(long countOnPage, long allCount) {
		long pageNumber = allCount/countOnPage;
		if(allCount%countOnPage != 0) {
			pageNumber++;
		}
		return pageNumber;
	}
	
	public static Long getStartNumber(long countOnPage, long page) {
		return countOnPage*(page-1);
	}
}
