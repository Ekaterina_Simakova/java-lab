<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>
<c:url value="/resources/js/moment-with-locales.js" var="moment" />
<script src="${moment}"></script>

<c:url var="validation_url" value="/news/validation" />
<spring:message var="momentFormat" code="locale.moment_format" />
<input type="hidden" id="dateformat" value="${momentFormat}" />

<c:forEach var="tag" items="${newsVO.tagList}">
	<input type="hidden" value="${tag.tagId}" class="news_tag" />
</c:forEach>

<spring:message var="dateformat" code="locale.dateformat" />

<c:if test="${empty news.newsId}" >
	<c:url value="/news-form/add" var="action" />
	<spring:message var="submit" code="locale.add" />
</c:if>
<c:if test="${not empty news.newsId}">
	<c:url value="/news-form/edit" var="action" />
	<spring:message var="submit" code="locale.edit" />
	<c:set var="date_value" value="${news.modificationDate}" />
</c:if>


<form:form modelAttribute="news" action="${action}" method="POST">
	<form:input path="newsId" type="hidden" />
	<c:if test="${not empty news.newsId}">
		<form:input path="creationDate" type="hidden" />
		<form:input path="modificationDate" type="hidden" />
	</c:if>
	<div class="row">
		<span class="col-xs-2">
			<a href="<c:url value='/newslist/page/${sessionScope.currentPageNumber}' /> ">
				<spring:message code="locale.back" />
			</a>
		</span>
		<span class="col-xs-8 nopadding">
			<span class="row">
				<span class="col-xs-5 nopadding error bottommargin" id="author_error"></span>
			</span>
			<span class="row">
				<span class="col-xs-5 pull-left nopadding">
					<span class="row">
						<button onclick="return showList(event, 'authorList');" class="select-button col-xs-12" >
							<span class="col-xs-11 nopadding pull-left">
								<spring:message code="locale.select_author" />
							</span>
							<span class="col-xs-1 nopadding text-right">&#9660</span>	
						</button>
					</span>
					<span class="row" onmouseover="showList(event, 'authorList');" onmouseout="hideList(event, 'authorList');">
						<span class="col-xs-12 hide" id="authorList">
							<form:radiobuttons path="author" items="${authorList}" itemValue="authorId" itemLabel="authorName" 
									element="span class='checkbox col-xs-12 nopadding pull-left text-left'"/>
						</span>
					</span>
				</span>
				<span class="col-xs-5 pull-right nopadding">
					<span class="row">
						<button onclick="return showList(event, 'tagList');" id="tagSelect" class="select-button col-xs-12" >
							<span class="col-xs-11 nopadding pull-left">
								<spring:message code="locale.select_tags" />
							</span>
							<span class="col-xs-1 nopadding text-right">&#9660</span>
						</button>
					</span>
					<span class="row" onmouseover="showList(event, 'tagList');" onmouseout="hideList(event, 'tagList');">
						<span class="col-xs-12 hide" id="tagList">
							<form:checkboxes path="tagList" items="${tagList}" itemLabel="tagName" itemValue="tagId"
									element="span class='checkbox col-xs-12 nopadding pull-left text-left'" />
						</span>
					</span>
				</span>
			</span>
		</span>
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="title_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.title" />:</span>
		<form:input type="text" path="title" class="col-xs-8" />
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="date_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.date" />:</span>
		<fmt:formatDate pattern="${dateformat}" value="${date_value}" var="formated_date"/>
		<input type="text" id="date_text" class="col-xs-4" value="${formated_date}" placeholder="${dateformat}" />
		<span class="col-xs-4">
			<p>${dateformat}</p>
		</span>
		<input type="date" class="hide" name="date" id="input_date" value="" />
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="shortText_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.shortText" />:</span>
		<form:textarea path="shortText" class="col-xs-8"/>
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="fullText_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.fullText" />:</span>
		<form:textarea path="fullText" class="col-xs-8"/>
	</div>
	<div class="row">
		<span class="col-xs-10 text-right nopadding">
			<input type="submit" onclick="return validNews('${validation_url}');" value="${submit}" />
		</span>
	</div>
</form:form>
</body>
