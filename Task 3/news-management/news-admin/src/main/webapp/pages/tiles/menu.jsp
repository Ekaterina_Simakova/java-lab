<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="row">
	<a href="<c:url value='/newslist' /> "><spring:message code='locale.newslist_menu_href' /></a>
</div>
<div class="row">
	<a href="<c:url value='/news-form' /> "><spring:message code='locale.addnews' /></a>
</div>
<div class="row">
	<a href="<c:url value='/authors' /> "><spring:message code='locale.add/update_author' /></a>
</div>
<div class="row">
	<a href="<c:url value='/tags' /> "><spring:message code='locale.add/update_tag' /></a>
</div>
