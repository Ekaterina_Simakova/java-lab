package com.epam.newsmanagement.utils;

public class RequestParameterName {
	
	public static final String AUTHOR_ID = "authorId";
	public static final String NEWS_ID = "newsId";
	public static final String TAG_ID = "tagId";
	public static final String TAG_ID_LIST = "tagIdList";
	public static final String DATE = "date";

}
