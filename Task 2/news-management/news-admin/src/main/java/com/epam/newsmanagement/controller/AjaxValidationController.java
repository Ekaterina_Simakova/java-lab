package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;

/**
 * Controller handles requests to validate beans.
 * Annotation @Valid and object of BindingResult class are used for validation.
 * Each method returns the list of FieldError objects.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class AjaxValidationController {
	
	@RequestMapping(value="author/validation", method=RequestMethod.POST) 
	public @ResponseBody List<FieldError> validAuthor(@Valid AuthorTO author, BindingResult bindingResult) {
		return bindingResult.getFieldErrors();
	}
	
	@RequestMapping(value="comment/validation", method=RequestMethod.POST) 
	public @ResponseBody List<FieldError> validComment(@Valid CommentTO comment, BindingResult bindingResult) {
		return bindingResult.getFieldErrors();
	}
	
	@RequestMapping(value="news/validation", method=RequestMethod.POST)
	public @ResponseBody List<FieldError> validNews(@Valid NewsTO news, BindingResult bindingResult) {
		return bindingResult.getFieldErrors();
	}

	@RequestMapping(value="tag/validation", method=RequestMethod.POST) 
	public @ResponseBody List<FieldError> validTag(@Valid TagTO tag, BindingResult bindingResult) {
		return bindingResult.getFieldErrors();
	}
}
