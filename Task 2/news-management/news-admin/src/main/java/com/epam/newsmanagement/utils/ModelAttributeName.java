package com.epam.newsmanagement.utils;

public class ModelAttributeName {

	public static final String NEWS_LIST = "newsList";
	public static final String NEWS = "news";
	public static final String NUMBER_OF_PAGES = "numberOfPages";
	public static final String SEARCH_CRITERIA = "searchCriteria";
	public static final String COMMENT = "comment";
	public static final String TAG_LIST = "tagList";
	public static final String AUTHOR_LIST = "authorList";
	public static final String NEXT_ID = "nextId";
	public static final String PREVIOUS_ID = "previousId";
	public static final String AUTHOR = "author";
	public static final String TAG = "tag";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String NEWS_VO = "newsVO";

}
