package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller is designed to work with tags.jsp page.
 * It provides methods for operating with TagTO object.
 * Available operations are getting, adding, updating, deleting.
 * Each method throws ServiceException.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class TagController {
	
	@Autowired
	NewsManagerImpl newsManager;
	
	@RequestMapping(value="tags")
	public ModelAndView getTags() throws ServiceException {
		ModelAndView modelView = new ModelAndView(ViewName.TAGS);
		List<TagTO> tagList = newsManager.getAllTags();
		modelView.addObject(ModelAttributeName.TAG_LIST, tagList);
		modelView.addObject(ModelAttributeName.TAG, new TagTO());
		return modelView;
	}
	
	@RequestMapping(value="tags/add")
	public String addTag(@ModelAttribute(ModelAttributeName.TAG) TagTO tag) throws ServiceException {
		newsManager.addTag(tag);
		return "redirect:/tags";
	}
	
	@RequestMapping(value="tags/update/{tagId}")
	public String updateTag(@PathVariable("tagId") Long tagId, @RequestParam("tagName") String tagName ) throws ServiceException {
		TagTO tag = new TagTO();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		newsManager.editTag(tag);
		return "redirect:/tags";
	}
	
	@RequestMapping(value="tags/delete/{tagId}")
	public String deleteTag(@PathVariable Long tagId) throws ServiceException {
		newsManager.deleteTagById(tagId);
		return "redirect:/tags";
	}

}
