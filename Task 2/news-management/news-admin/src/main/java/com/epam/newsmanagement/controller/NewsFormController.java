package com.epam.newsmanagement.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.RequestParameterName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller is designed to work with newsForm.jsp page.
 * It provides methods for some operations with NewsTO object.
 * Available operations are getting, adding, editing NewsTO object.
 * Each method throws ServiceException.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class NewsFormController {
	
	@Autowired 
	NewsManagerImpl newsManager;
	
	@RequestMapping(value="news-form")
	public ModelAndView openEmptyForm() throws ServiceException {
		ModelAndView modelView = new ModelAndView(ViewName.NEWS_FORM);
		List<TagTO> tagList = newsManager.getAllTags();
		List<AuthorTO> authorList = newsManager.getAllAuthors();
		modelView.addObject(ModelAttributeName.NEWS, new NewsTO());
		modelView.addObject(ModelAttributeName.TAG_LIST, tagList);
		modelView.addObject(ModelAttributeName.AUTHOR_LIST, authorList);
		return modelView;
	}
	
	@RequestMapping(value="news-form/{newsId}")
	public ModelAndView openNewsForm(@PathVariable Long newsId) throws ServiceException {
		ModelAndView modelView = new ModelAndView(ViewName.NEWS_FORM);
		NewsVO news = newsManager.getNewsById(newsId);
		List<TagTO> tagList = newsManager.getAllTags();
		List<AuthorTO> authorList = newsManager.getAllAuthors();
		modelView.addObject(ModelAttributeName.NEWS_VO, news);
		modelView.addObject(ModelAttributeName.NEWS, news.getNewsTO());
		modelView.addObject(ModelAttributeName.AUTHOR_LIST, authorList);
		modelView.addObject(ModelAttributeName.TAG_LIST, tagList);
		return modelView;
	}

	@RequestMapping(value="news-form/add", method=RequestMethod.POST)
	public String addNews(NewsTO news, @RequestParam(value=RequestParameterName.AUTHOR_ID, required=true) Long authorId,
			@RequestParam(value=RequestParameterName.TAG_ID, required=false) Long[] tagIdArray,
			@RequestParam(value=RequestParameterName.DATE, required=true) Date date) throws ServiceException {
		news.setCreationDate(date);
		news.setModificationDate(date);
		List<Long> tagIdList = null;
		if(tagIdArray != null) {
			tagIdList = new ArrayList<Long>(Arrays.asList(tagIdArray));
		}
		newsManager.addNews(news, tagIdList, authorId);
		return "redirect:/newslist";
	}
	
	@RequestMapping(value="news-form/edit", method=RequestMethod.POST)
	public String editNews(NewsTO news, @RequestParam(value=RequestParameterName.AUTHOR_ID, required=true) Long authorId,
			@RequestParam(value=RequestParameterName.TAG_ID, required=false) Long[] idTagArray,
			@RequestParam(value=RequestParameterName.DATE, required=true) Date date) throws ServiceException {
		news.setModificationDate(date);
		List<Long> idTagList = null;
		if(idTagArray != null) {
			idTagList = Arrays.asList(idTagArray);
		}
		newsManager.editNews(news, idTagList, authorId);
		return "redirect:/view/".concat(news.getNewsId().toString());
	}

}
