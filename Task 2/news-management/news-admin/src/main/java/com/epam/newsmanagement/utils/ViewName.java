package com.epam.newsmanagement.utils;

public class ViewName {
	
	public static final String NEWS_LIST = "newslist";
	public static final String NEWS_VIEW = "view";
	public static final String NEWS_FORM = "newsForm";
	public static final String AUTHORS = "authors";
	public static final String TAGS = "tags";
	public static final String ERROR = "error";
	public static final String LOGIN = "loginPage";

}
