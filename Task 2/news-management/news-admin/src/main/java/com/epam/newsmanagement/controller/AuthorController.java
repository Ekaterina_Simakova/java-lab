package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller handles requests from the authors.jsp page.
 * It provides methods for operations with AuthorTO object.
 * Available operations are getting, adding, updating, expiring (instead of deleting).
 * Each method throws ServiceException.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class AuthorController {
	
	@Autowired
	NewsManagerImpl newsManager;
	
	@RequestMapping(value="authors")
	public ModelAndView getAuthors() throws ServiceException {
		ModelAndView modelView = new ModelAndView(ViewName.AUTHORS);
		List<AuthorTO> authorList = newsManager.getAllAuthors();
		modelView.addObject(ModelAttributeName.AUTHOR_LIST, authorList);
		modelView.addObject(ModelAttributeName.AUTHOR, new AuthorTO());
		return modelView;
	}
	
	@RequestMapping(value="authors/update/{authorId}", method=RequestMethod.POST)
	public String updateAuthor(@ModelAttribute(ModelAttributeName.AUTHOR) AuthorTO author) throws ServiceException {
		newsManager.editAuthor(author);
		return "redirect:/authors";

	}
	
	@RequestMapping(value="authors/add")
	public String addAuthor(@ModelAttribute(ModelAttributeName.AUTHOR) AuthorTO author) throws ServiceException {
		newsManager.addAuthor(author);
		return "redirect:/authors";
	}
	
	@RequestMapping(value="authors/expire/{authorId}")
	public String expireAuthor(@PathVariable Long authorId) throws ServiceException {
		newsManager.expireAuthorById(authorId);
		return "redirect:/authors";
	}

}
