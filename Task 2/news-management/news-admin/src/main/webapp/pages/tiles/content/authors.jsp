<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/fonts.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<c:url value="/author/validation" var="validation_url" />

<c:forEach var="element" items="${authorList}">
	<c:url value="/authors/update/${element.authorId}" var="update_action" />
	<form method="POST" class="row" id="author_${element.authorId}" action="${update_action}">
		<span class="col-xs-2"><spring:message code="locale.author" />:</span>
		<input name="authorName" type="text" disabled id="author_${element.authorId}_authorName" value="${element.authorName}" class="col-xs-6"/>
		<span class="col-xs-4" id="author_${element.authorId}_base_links">
			<a href="#" onclick="return enableForm('author_${element.authorId}');"">
				<spring:message code="locale.edit" />
			</a>
		</span>
		<span class="col-xs-4 hide" id="author_${element.authorId}_addit_links">
			<a href="#" onclick="return updateAuthorName(${element.authorId});">
				<spring:message code="locale.update" />
			</a>
			<c:url value="/authors/expire/${element.authorId}" var="expire_url" />
			<spring:message var="confirm_message" code="locale.confirm" />
			<a href="${expire_url}" onclick="return confirm('${confirm_message}');">
				<spring:message code="locale.expire" />
			</a>
			<a href="#" onclick="return disableForm('author_${element.authorId}');">
				<spring:message code="locale.cancel" />
			</a>
		</span>
	</form>
</c:forEach>

<c:url var="add" value="/authors/add" />
<form:form modelAttribute="author" method="post" action="${add}" class="row">
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-6 nopadding error" id="authorName_error"></span> 
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.add_author" /></span>
		<form:input path="authorName" class="col-xs-6"/>
		<span class="col-xs-4">
			<a href="#" onclick="validAndSubmitForm('${validation_url}','author');"><spring:message code="locale.add" /></a>
		</span>
	</div>
</form:form>