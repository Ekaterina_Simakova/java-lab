<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>
<c:url value="/resources/js/moment-with-locales.js" var="moment" />
<script src="${moment}"></script>

<body onload="checkTags();" onclick="hideTagList();">

<c:url var="validation_url" value="/news/validation" />
<spring:message var="momentFormat" code="locale.moment_format" />
<input type="hidden" id="dateformat" value="${momentFormat}" />

<c:forEach var="tag" items="${newsVO.tagList}">
	<input type="hidden" value="${tag.tagId}" class="news_tag" />
</c:forEach>

<c:if test="${empty news.newsId}" >
	<c:url value="/news-form/add" var="action" />
	<spring:message var="submit" code="locale.add" />
	<c:set var="date" value="creationDate" />
	<c:set var="date_value" value="" />
</c:if>
<c:if test="${not empty news.newsId}">
	<c:url value="/news-form/edit" var="action" />
	<spring:message var="submit" code="locale.edit" />
	<c:set var="date" value="modificationDate" />
	<c:set var="date_value" value="${news.modificationDate}" />
</c:if>
<spring:message var="dateformat" code="locale.dateformat" />

<form:form modelAttribute="news" action="${action}" method="POST">
	<form:input path="newsId" type="hidden" />
	<div class="row">
		<span class="col-xs-2">
			<a href="<c:url value='/newslist/page/${sessionScope.currentPageNumber}' /> ">
				<spring:message code="locale.back" />
			</a>
		</span>
		<span class="col-xs-8 text-center nopadding">
			<span class="col-xs-5 pull-left nopadding">
				<select name="authorId" class="col-xs-12" id="authorSelect">
					<c:forEach var="authorOpt" items="${authorList}">
						<option value="${authorOpt.authorId}" <c:if test="${authorOpt.authorId eq newsVO.author.authorId}">selected</c:if> >
							${authorOpt.authorName}
						</option>
						
					</c:forEach>
				</select>
			</span>
			<span class="col-xs-5 pull-right nopadding" onclick="showTagList();">
				<span class="row">
					<select onclick="showTagList();" id="tagSelect" class="col-xs-12">
						<option value="" class="hide"><spring:message code="locale.select_tags" /></option>
					</select>
					</span>
				<span class="row">
					<span class="col-xs-12 tagList hide" id="tagList">
						<c:forEach var="tag" items="${tagList}">
							<span class="row">
								<span class="pull-left">
									<input type="checkbox" name="tagId" value="${tag.tagId}" id="tag_${tag.tagId}"/>
								</span>
								<span class="pull-left">${tag.tagName}</span>
							</span>
						</c:forEach>
					</span>
				</span>
			</span>
		</span>
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="title_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.title" />:</span>
		<form:input type="text" path="title" class="col-xs-8" />
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="date_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.date" />:</span>
		<fmt:formatDate pattern="${dateformat}" value="${date_value}" var="formated_date"/>
		<input type="text" id="date_text" class="col-xs-4" value="${formated_date}" placeholder="${dateformat}" />
		<span class="col-xs-4">
			<p>${dateformat}</p>
		</span>
		<input type="date" class="hide" name="date" id="input_date" value="" />
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="shortText_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.shortText" />:</span>
		<form:textarea path="shortText" class="col-xs-8"/>
	</div>
	<div class="row">
		<span class="col-xs-2" ></span>
		<span class="col-xs-8 error nopadding" id="fullText_error"></span>
	</div>
	<div class="row">
		<span class="col-xs-2"><spring:message code="locale.fullText" />:</span>
		<form:textarea path="fullText" class="col-xs-8"/>
	</div>
	<div class="row">
		<span class="col-xs-10 text-right nopadding">
			<input type="submit" onclick="return validNews('${validation_url}');" value="${submit}" />
		</span>
	</div>
</form:form>
</body>
