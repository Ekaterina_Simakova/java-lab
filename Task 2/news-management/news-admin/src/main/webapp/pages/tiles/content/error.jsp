<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />

<div class="row">
	<a href="<c:url value='/' />" ><spring:message code='locale.goToMain' /></a>
</div>
<div class="row">
	${errorMessage}
</div>
