<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
	
<div class="headerline">
	<h1>
		<spring:message code="locale.headerline" />
	</h1>
</div>
<div class="languagebox">
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<div class="row">
			<span class="nopadding text-right pull-right">
				<a href="<c:url value="/logout" />" > <spring:message code="locale.logout" /> </a>
			</span>
			<span class="col-xs-8 text-right pull-right">
				<spring:message code="locale.hello" />, <security:authentication property="principal.username" />
			</span>
		</div>
	</security:authorize>
	<div class="row text-right">
		<a href="?lang=en"> <u>EN</u> </a>
		<a href="?lang=ru"> <u>RU</u> </a>
	</div>
	
</div>
