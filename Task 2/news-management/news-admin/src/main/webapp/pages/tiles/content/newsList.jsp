<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/fonts.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<body onload="criteriaOnLoad();" onclick="hideTagList();">

<input type="hidden" value="${searchCriteria.authorId}" id="criteria_author" />
<c:forEach var="tag" items="${searchCriteria.tagIdList}">
	<input type="hidden" value="${tag}" class="criteria_tags"/>
</c:forEach>

<c:url var="searchAction" value="/newslist" />
<form action="${searchAction}" method="POST" class="row">
	<span class="col-xs-1"></span>
	<span class="col-xs-4 nopadding" onclick="showTagList();">
		<span class="row">
			<select onclick="showTagList();" id="tagSelect" class="col-xs-12">
				<option value="" class="hide"><spring:message code="locale.select_tags" /></option>
			</select>
		</span>
		<span class="row">
			<span class="col-xs-12 tagList hide" id="tagList">
				<c:forEach var="tag" items="${tagList}">
					<span class="row">
						<span class="pull-left nopadding"><input name="tagIdList" type="checkbox" id="check_tag_${tag.tagId}" value="${tag.tagId}"/></span>
						<span class="pull-left nopadding">${tag.tagName}</span>
					</span>
				</c:forEach>
			</span>
		</span>
	</span>
	<span class="col-xs-4">
		<select name="authorId" class="col-xs-12">
			<option value=""><spring:message code="locale.select_author" /></option>
			<c:forEach var="authorOpt" items="${authorList}">
				<option value="${authorOpt.authorId}" id="option_author_${authorOpt.authorId}">${authorOpt.authorName}</option>
			</c:forEach>
		</select>
	</span>
	<span class="col-xs-3">
		<input type="submit" value="<spring:message code="locale.filter" />" />
		<a href="<c:url value='/newslist/reset' />" ><spring:message code="locale.reset" /></a>
	</span>
</form>
<div class="newslistbox">
	<c:url var="deletenews" value="/newslist/delete/${sessionScope.currentPageNumber}" />
	<form method="post" action="${deletenews}" id="newsList">
		<c:forEach var="news" items="${newsList}">
			<div class="listelement">
				<div class="row notopmargin">
					<a href="<c:url value='/view/${news.newsTO.newsId}' />" class="col-xs-6 nopadding">
						<font class="gappy">${news.newsTO.title}</font>
					</a>
					<span class="col-xs-3">
						<font class="gappy">(by ${news.author.authorName})</font>
					</span>
					<span class="col-xs-2">
						<spring:message var="dateformat" code="locale.dateformat" />
						<fmt:formatDate pattern="${dateformat}" value="${news.newsTO.modificationDate}" />
					</span>
					<span class="col-xs-1">
						<input type="checkbox" name="newsId" value="${news.newsTO.newsId}" />
					</span>
				</div>
				<div class="row notopmargin">
					<font class="gappy">${news.newsTO.shortText}</font>
				</div>
				<p class="text-right">
					<c:forEach var="tag" items="${news.tagList}">
						<font id="tagsfont gappy">${tag.tagName},</font>
					</c:forEach>
					<font id="commentnumberfont">
						<spring:message code="locale.comments" />(${fn:length(news.commentList)})
					</font> 
					<a href="<c:url value='/news-form/${news.newsTO.newsId}' /> ">
						<spring:message code="locale.edit" />
					</a>
				</p>
			</div>
		</c:forEach>
		<spring:message var="message" code="locale.confirm" />
		<input type="submit" onclick="return confirmDeleting('${message}');" value="<spring:message code='locale.delete' /> " class="pull-right"/>
	</form>
</div>
<c:if test="${numberOfPages != 0}">
	<div class="pagination-div">
		<ul class="pagination">
			<c:forEach var="i" begin="1" end="${numberOfPages}">
				<li><a href="<c:url value='/newslist/page/${i}' /> ">${i}</a></li>
			</c:forEach>
		</ul>
	</div>
</c:if>
</body>
