<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />

<spring:url var="authUrl" value="/j_spring_security_check" />
<form method="post" action="${authUrl}" id="loginform">
	<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION.message}">
		<div class="row notopmargin">
			<span class="col-xs-4"></span>
			<span class="col-xs-8 error nopadding">${SPRING_SECURITY_LAST_EXCEPTION.message}</span>
		</div>
	</c:if>
	<div class="row notopmargin">
		<span class="col-xs-4 nopadding"><spring:message code="locale.login" />:</span>
		<input name="username" type="text" class="col-xs-8" />
	</div>
	<div class="row">
		<span class="col-xs-4 nopadding"><spring:message code="locale.password" />:</span>
		<input id="password" name="password" type="password" class="col-xs-8"/>
	</div>
	<div class="row text-right">
		<spring:message var="signIn" code="locale.sign_in" />
		<input name="commit" type="submit" value="${signIn}" />
	</div>
</form>