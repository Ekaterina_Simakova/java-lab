<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<div class="header">
	<tiles:insertAttribute name="header" />
</div>
<div class="menu">
	<tiles:insertAttribute name="menu" />
</div>
<div class="content">
	<tiles:insertAttribute name="content" />
</div>
<div class="footer">
	<tiles:insertAttribute name="footer" />
</div>
