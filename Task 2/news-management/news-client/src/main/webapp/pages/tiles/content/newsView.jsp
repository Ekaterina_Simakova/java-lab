<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<c:url var="validation_url" value="/comment/validation" />

<div class="row">
	<a href="<c:url value='/newslist/page/${sessionScope.currentPageNumber}' /> "><spring:message code="locale.back" /></a>
</div>
<div class="newsbox">
	<div class="row">
		<span class="col-xs-7 nopadding">
			<b class="gappy">${news.newsTO.title}</b>
		</span>
		<span class="col-xs-3">(by ${news.author.authorName})</span>
		<spring:message var="dateformat" code="locale.dateformat" />
		<span class="col-xs-2 nopadding text-right">
			<u><fmt:formatDate pattern="${dateformat}" value="${news.newsTO.modificationDate}" /></u>
		</span>
	</div>
	<div class="row">
		<font class="gappy">${news.newsTO.shortText}</font>
	</div>
	<div class="row">
		<font class="gappy">${news.newsTO.fullText}</font>
	</div>
</div>
<c:url var="addcomment" value="/view/addcomment" />
<div class="row nopadding">
	<div class="commentList">
		<c:forEach var="com" items="${news.commentList}">
			<u><fmt:formatDate pattern="${dateformat}" value="${com.creationDate}" /></u>
			<div class="row comment notopmargin">
				<font class="gappy">${com.commentText}</font>
			</div>
		</c:forEach>
	</div>
</div>
<div class="row nopadding">
	<form:form method="POST" modelAttribute="comment" action="${addcomment}" class="row">
		<form:textarea path="commentText"/>
		<form:input path="newsId" type="hidden"/>
		<input type="submit" class="pull-right" onclick="return validAndSubmitForm('${validation_url}','comment');" value="<spring:message code='locale.post_comment' />"/>
	</form:form>
	<div class="col-xs-4 notopmargin error" id="commentText_error"></div>
</div>
<div class="row">
	<span class="col-xs-6 nopadding text-left">
		<c:if test="${not empty previousId}" >
			<a href="<c:url value="/view/${previousId}" />" ><spring:message code="locale.previous"/></a>
		</c:if>
	</span>
	<span class="col-xs-6 nopadding text-right">
		<c:if test="${not empty nextId}">
			<a href="<c:url value="/view/${nextId}" />" ><spring:message code="locale.next"/></a>
		</c:if>
	</span>
</div>