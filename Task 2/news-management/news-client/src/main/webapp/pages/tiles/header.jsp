<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />

<div class="headercontent">
	<span class="nopadding pull-right">
		<div class="row text-right">
			<a href="?lang=en">EN</a> 
			<a href="?lang=ru">RU</a>
		</div>
	</span>
	<span class="nopadding pull-left">
		<h1><spring:message code="locale.headerline" /></h1>
	</span>
	
</div>
