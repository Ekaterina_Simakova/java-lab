<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/fonts.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<body onload="criteriaOnLoad();" onclick="hideTagList();">

<input type="hidden" value="${searchCriteria.authorId}" id="criteria_author" />
<c:forEach var="tag" items="${searchCriteria.tagIdList}">
	<input type="hidden" value="${tag}" class="criteria_tags"/>
</c:forEach>

<c:url var="searchAction" value="/newslist" />
<form action="${searchAction}" method="POST" class="row">
	<span class="col-xs-2"></span>
	<span class="col-xs-4 nopadding" onclick="showTagList();">
		<span class="row">
			<select onclick="showTagList();" id="tagSelect" class="col-xs-12">
				<option value="" class="hide"><spring:message code="locale.select_tags" /></option>
			</select>
		</span>
		<span class="row">
			<span class="col-xs-12 tagList hide" id="tagList">
				<c:forEach var="tag" items="${tagList}">
					<span class="row">
						<span class="pull-left nopadding"><input name="tagIdList" type="checkbox" id="check_tag_${tag.tagId}" value="${tag.tagId}"/></span>
						<span class="pull-left nopadding">${tag.tagName}</span>
					</span>
				</c:forEach>
			</span>
		</span>
	</span>
	<span class="col-xs-4">
		<select name="authorId" class="col-xs-12">
			<option value=""><spring:message code="locale.select_author" /></option>
			<c:forEach var="authorOpt" items="${authorList}">
				<option value="${authorOpt.authorId}" id="option_author_${authorOpt.authorId}">${authorOpt.authorName}</option>
			</c:forEach>
		</select>
	</span>
	<span class="pull-left col-xs-2 nopadding">
		<input type="submit" value="<spring:message code="locale.filter" />" />
		<a href="<c:url value='/newslist/reset' />" ><spring:message code="locale.reset" /></a>
	</span>
</form>
<div class="newslistbox">
	<c:forEach var="news" items="${newsList}">
		<div class="listelement">
			<div class="row">
				<span class="nopadding pull-right">
					<spring:message var="dateformat" code="locale.dateformat" />
					<u><fmt:formatDate pattern="${dateformat}" value="${news.newsTO.modificationDate}" /></u>
				</span>
				<span class="nopadding pull-left">
					<span class="nopadding pull-left"><b class="gappy">${news.newsTO.title}</b></span>
					<span class="leftmargin20"> (by ${news.author.authorName})</span>
				</span>
			</div>
			<div class="row notopmargin">
				<font class="gappy">${news.newsTO.shortText}</font>
			</div>
			<div class="row text-right">
				<c:forEach var="tag" items="${news.tagList}">
					<font id="tagsfont">${tag.tagName},</font>
				</c:forEach>
				<font id="commentnumberfont"><spring:message code="locale.comments" />(${fn:length(news.commentList)})
				</font> <a href="<c:url value='/view/${news.newsTO.newsId}' /> ">
					<spring:message code="locale.view" />
				</a>
			</div>
		</div>
	</c:forEach>
</div>
<c:if test="${numberOfPages != 0}">
	<div class="pagination-div">
		<ul class="pagination">
			<c:forEach var="i" begin="1" end="${numberOfPages}">
				<li><a href="<c:url value='/newslist/page/${i}' /> ">${i}</a></li>
			</c:forEach>
		</ul>
	</div>
</c:if>
</body>
