<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

${errorMessage}
<a href="<c:url value='/' />" ><spring:message code='locale.goToMain' /></a>
