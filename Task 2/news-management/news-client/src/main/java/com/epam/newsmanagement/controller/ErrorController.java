package com.epam.newsmanagement.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller handles exceptions.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@ControllerAdvice
public class ErrorController {
	
	/**
	 * Method handles exceptions of ServiceException class.
	 * 
	 * @param e exception
	 * @return ModelAndView object with error view name and message
	 */
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleServiceException(ServiceException e) {
		ModelAndView modelView = new ModelAndView(ViewName.ERROR);
		modelView.addObject(ModelAttributeName.ERROR_MESSAGE, e.getMessage());
		return modelView;
	}

}
