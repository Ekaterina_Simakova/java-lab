package com.epam.newsmanagement.controller;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.utils.Calculator;
import com.epam.newsmanagement.utils.ModelAttributeName;
import com.epam.newsmanagement.utils.RequestParameterName;
import com.epam.newsmanagement.utils.SessionAttributeName;
import com.epam.newsmanagement.utils.ViewName;

/**
 * Controller is designed to work with newsList.jsp page.
 * It provides methods for operations with list of NewsTO objects and SearchCriteria object.
 * Available operations are getting the list of news for page according to SearchCriteria object, changing
 * SearchCriteria object.
 * Each method throws ServiceException.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class NewsListController {
	@Autowired
	NewsManagerImpl newsManager;
	
	private static final long FIRST_PAGE_NUMBER = 1L;

	@RequestMapping(value={"newslist","/"})
	public ModelAndView showFirstPage(HttpSession session) throws ServiceException {
		return getNewsListPage(FIRST_PAGE_NUMBER, session);
	}

	@RequestMapping(value = "newslist", method=RequestMethod.POST)
	public ModelAndView filterNews(@RequestParam(value=RequestParameterName.TAG_ID_LIST, required=false) Long[] tagIdList,
									@RequestParam(value=RequestParameterName.AUTHOR_ID) Long authorId,
									HttpSession session) throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthorId(authorId);
		if(tagIdList != null) {
			criteria.setTagIdList(Arrays.asList(tagIdList));
		}
		session.setAttribute(SessionAttributeName.SEARCH_CRITERIA, criteria);
		return getNewsListPage(FIRST_PAGE_NUMBER, session);
	}
	
	@RequestMapping(value="newslist/page/{pageNumber}")
	public ModelAndView showNewsListPage(@PathVariable Long pageNumber, HttpSession session) throws ServiceException {
		return getNewsListPage(pageNumber, session);
	}
	
	@RequestMapping(value="newslist/reset", method=RequestMethod.GET)
	public String resetCriteria(HttpSession session) {
		session.setAttribute(SessionAttributeName.SEARCH_CRITERIA, new SearchCriteria());
		return "redirect:/newslist";
		
	}
	
	private ModelAndView getNewsListPage(Long currentPage, HttpSession session) throws ServiceException {
		long newsOnPage = (long) session.getAttribute(SessionAttributeName.NUMBER_OF_NEWS_ON_PAGE);
		SearchCriteria criteria = (SearchCriteria) session.getAttribute(SessionAttributeName.SEARCH_CRITERIA);
		if(criteria == null) {
			criteria = new SearchCriteria();
		}
		session.setAttribute(SessionAttributeName.CURRENT_PAGE_NUMBER, currentPage);
		List<NewsVO> newsList = newsManager.getNews(criteria,currentPage,newsOnPage);
		Long newsCount = newsManager.countNews(criteria);
		Long numberOfPages = Calculator.getPageCount(newsOnPage, newsCount);
		List<AuthorTO> authorList = newsManager.getAllAuthors();
		List<TagTO> tagList = newsManager.getAllTags();
		ModelAndView model = new ModelAndView(ViewName.NEWS_LIST, ModelAttributeName.NEWS_LIST, newsList);
		model.addObject(ModelAttributeName.NUMBER_OF_PAGES, numberOfPages);
		model.addObject(ModelAttributeName.AUTHOR_LIST, authorList);
		model.addObject(ModelAttributeName.TAG_LIST, tagList);
		model.addObject(ModelAttributeName.SEARCH_CRITERIA, criteria);
		return model;
	}	
}
