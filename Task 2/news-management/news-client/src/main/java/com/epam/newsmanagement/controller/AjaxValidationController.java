package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.newsmanagement.entity.CommentTO;

@Controller
public class AjaxValidationController {
	
	/**
	 * Method handles request for validation CommentTO object.
	 * It returns the list of validation error. If there are not errors it returns null.
	 * 
	 * @param comment CommentTO object
	 * @param bindingResult BindingResult object
	 * @return the list of validation errors
	 */
	@RequestMapping(value="comment/validation", method=RequestMethod.POST) 
	public @ResponseBody List<FieldError> validComment(@Valid CommentTO comment, BindingResult bindingResult) {
		return bindingResult.getFieldErrors();
	}

}
