package com.epam.newsmanagement.utils;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.epam.newsmanagement.entity.SearchCriteria;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		session.setAttribute(SessionAttributeName.NUMBER_OF_NEWS_ON_PAGE, 5l);
		session.setAttribute(SessionAttributeName.CURRENT_PAGE_NUMBER, 1L);
		session.setAttribute(SessionAttributeName.SEARCH_CRITERIA, new SearchCriteria());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		// TODO Auto-generated method stub

	}

}
