package com.epam.newsmanagement.utils;

public class SessionAttributeName {
	
	public static final String CURRENT_PAGE_NUMBER = "currentPageNumber";
	public static final String NUMBER_OF_NEWS_ON_PAGE = "newsNumber";
	public static final String SEARCH_CRITERIA = "searchCriteria";
	
}
