package com.epam.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;

/**
 * Controller is designed to operation with comments.
 * Available operation is posting comment.
 * 
 * @author Katsiaryna_Simakova
 *
 */
@Controller
public class CommentController {
	
	@Autowired
	private NewsManagerImpl newsManager;
	
	/**
	 * Method is suitable for adding CommentTO object.
	 * 
	 * @param comment CommentTO object
	 * @return view name
	 * @throws ServiceException
	 */
	@RequestMapping(value="view/addcomment")
	public String postComment(CommentTO comment) throws ServiceException {
		comment.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		newsManager.addComment(comment);
		return "redirect:/view/".concat(comment.getNewsId().toString());
	}

}
