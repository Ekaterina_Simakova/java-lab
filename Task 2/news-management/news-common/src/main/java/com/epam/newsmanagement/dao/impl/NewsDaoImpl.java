package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.DBUtil;
import com.epam.newsmanagement.util.PositionName;
import com.epam.newsmanagement.util.QueryBuilder;

/**
 * @see NewsDao
 * @author Katsiaryna_Simakova
 *
 */
public class NewsDaoImpl implements NewsDao {

	private BasicDataSource dataSource;

	private static final String COLUMN_ID = "NEWS_ID";
	private static final String COLUMN_TITLE = "TITLE";
	private static final String COLUMN_SHORT_TEXT = "SHORT_TEXT";
	private static final String COLUMN_FULL_TEXT = "FULL_TEXT";
	private static final String COLUMN_CREATION_DATE = "CREATION_DATE";
	private static final String COLUMN_MODIFICATION_DATE = "MODIFICATION_DATE";
	private static final String COLUMN_COUNT = "COUNT";
	private static final String COLUMN_NEXT_ID = "NEXT_ID";
	private static final String COLUMN_PREVIOUS_ID = "PREV_ID";

	private static final String SQL_INSERT_NEWS = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, "
			+ "FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_ID_SEQ.NEXTVAL,?,?,?,?,?)";
	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE=?, SHORT_TEXT=?, FULL_TEXT=?, "
			+ "MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private static final String SQL_GET_NEWS_BY_ID = "SELECT TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, "
			+ "MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=?";
	
	/**
	 * Sets the <code>BasicDataSource</code> object for access to the database.
	 * 
	 * @param dataSource <code>BasicDataSource</code> object
	 * @see org.apache.commons.dbcp.BasicDataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long addNews(NewsTO news) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			Long newsId = null;
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_INSERT_NEWS,new String[] { COLUMN_ID });
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setDate(4, news.getCreationDate());
			pst.setDate(5, news.getModificationDate());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs.next()) {
				newsId = rs.getLong(1);
			}
			return newsId;
		} catch (SQLException e) {
			throw new DaoException("Inserting news failed, " + news, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public void editNews(NewsTO news) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_UPDATE_NEWS);
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setDate(4, news.getModificationDate());
			pst.setLong(5, news.getNewsId());
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Updating news failed, " + news, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public List<NewsTO> getNews(SearchCriteria criteria, long firstNewsNumber, long lastNewsNumber) throws DaoException {
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		String query = QueryBuilder.buildQuerySearchNews(criteria);
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(query);
			pst.setLong(1, firstNewsNumber);
			pst.setLong(2, lastNewsNumber);
			rs = pst.executeQuery();
			while(rs.next()) {
				NewsTO news = new NewsTO();
				news.setNewsId(rs.getLong(COLUMN_ID));
				news.setTitle(rs.getString(COLUMN_TITLE));
				news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
				news.setFullText(rs.getString(COLUMN_FULL_TEXT));
				news.setCreationDate(rs.getDate(COLUMN_CREATION_DATE));
				news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
				newsList.add(news);
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException("Searching news ids by criteria " + criteria + "failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public NewsTO getNewsById(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			NewsTO news = new NewsTO();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_NEWS_BY_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if (rs.next()) {
				news.setTitle(rs.getString(COLUMN_TITLE));
				news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
				news.setFullText(rs.getString(COLUMN_FULL_TEXT));
				news.setCreationDate(rs.getDate(COLUMN_CREATION_DATE));
				news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
				news.setNewsId(newsId);
			}
			return news;
		} catch (SQLException e) {
			throw new DaoException("Selecting news failed, id is " + newsId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public Long countNews(SearchCriteria criteria) throws DaoException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			Long count = null;
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			String query = QueryBuilder.buildQueryCountNews(criteria);
			rs = st.executeQuery(query);
			if (rs.next()) {
				count = rs.getLong(COLUMN_COUNT);
			}
			return count;
		} catch (SQLException e) {
			throw new DaoException("Counting news failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, st, rs);
		}
	}

	@Override
	public Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			Map<String,Long> results = new HashMap<String,Long>();
			conn = DataSourceUtils.getConnection(dataSource);
			String query = QueryBuilder.buildQueryGetNextAndPreviousIds(criteria);
			pst = conn.prepareStatement(query);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if(rs.next()) {
				String next = rs.getString(COLUMN_NEXT_ID);
				String previous = rs.getString(COLUMN_PREVIOUS_ID);
				if(next == null) {
					results.put(PositionName.NEXT, null);
				} else {
					results.put(PositionName.NEXT, Long.valueOf(next));
				}
				if(previous == null) {
					results.put(PositionName.PREVIOUS, null);
				} else {
					results.put(PositionName.PREVIOUS, Long.valueOf(previous));
				}
			}
			return results;
		} catch (SQLException e) {
			throw new DaoException("Selecting results of determination neighbors rows id " + newsId + " failed",e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public void deleteNewsListByIds(List<Long> newsIdList) throws DaoException {
		Connection conn = null;
		Statement st = null;
		try {
			String query = QueryBuilder.buildQueryDeleteNewsList(newsIdList);
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			st.execute(query);
		} catch (SQLException e) {
			throw new DaoException("Deleting the list of news failed",e);
		} finally {
			DBUtil.closeResources(dataSource, conn, st);
		}
	}
	
}
