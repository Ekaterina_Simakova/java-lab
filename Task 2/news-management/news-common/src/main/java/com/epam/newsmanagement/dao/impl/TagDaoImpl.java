package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.DBUtil;
import com.epam.newsmanagement.util.QueryBuilder;

/**
 * @see TagDao
 */
public class TagDaoImpl implements TagDao {
	
	private BasicDataSource dataSource;
	
	private static final String COLUMN_TAG_ID = "TAG_ID";
	private static final String COLUMN_TAG_NAME = "TAG_NAME";
	
	private static final String SQL_INSERT_TAG = "INSERT INTO TAG (TAG_ID,TAG_NAME) "
			+ "VALUES(tag_id_seq.NEXTVAL,?)";
	private static final String SQL_DELETE_TAG_BY_ID = "DELETE FROM TAG WHERE TAG_ID=?";
	private static final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME=? WHERE TAG_ID=?";
	private static final String SQL_GET_TAG_BY_ID = "SELECT TAG_NAME FROM TAG WHERE TAG_ID=?";
	private static final String SQL_ATTACH_TAG_NEWS = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES(?,?)";
	private static final String SQL_DETACH_TAG = "DELETE FROM NEWS_TAG WHERE TAG_ID=?";
	private static final String SQL_GET_NEWS_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID IN "
			+ "(SELECT TAG_ID FROM NEWS_TAG WHERE NEWS_ID=?)";
	private static final String SQL_DETACH_ALL_TAG_NEWS = "DELETE FROM NEWS_TAG WHERE NEWS_ID=?";
	private static final String SQL_GET_ALL_TAGS = "SELECT TAG_ID,TAG_NAME FROM TAG";
	
	/**
	 * Sets the <code>BasicDataSource</code> object for access to the database.
	 * 
	 * @param dataSource <code>BasicDataSource</code> object
	 * @see org.apache.commons.dbcp.BasicDataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long addTag(TagTO tag) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			Long tagId = null;
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_INSERT_TAG,new String[]{COLUMN_TAG_ID});
			pst.setString(1, tag.getTagName());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if(rs.next()) {
				tagId = rs.getLong(1);
			}
			return tagId;
		} catch (SQLException e) {
			throw new DaoException("Inserting tag failed " + tag, e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst, rs);
		}
	}

	@Override
	public void deleteTagById(Long tagId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_DELETE_TAG_BY_ID);
			pst.setLong(1, tagId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Deleting tag failed, id is " + tagId, e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst);
		}
	}

	@Override
	public void editTag(TagTO tag) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_UPDATE_TAG);
			pst.setString(1, tag.getTagName());
			pst.setLong(2, tag.getTagId());
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Updating tag failed, " + tag, e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst);
		}
	}

	@Override
	public TagTO getTagById(Long tagId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			TagTO tag = new TagTO();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_TAG_BY_ID);
			pst.setLong(1, tagId);
			rs = pst.executeQuery();
			if(rs.next()) {
				tag.setTagName(rs.getString(COLUMN_TAG_NAME));
				tag.setTagId(tagId);
			}
			return tag;
		} catch (SQLException e) {
			throw new DaoException("Selecting tag failed, id is " + tagId, e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst, rs);
		}
	}

//	@Override
//	public void attachTagToNewsById(Long tagId, Long newsId) throws DaoException {
//		Connection conn = null;
//		PreparedStatement pst = null;
//		try {
//			conn = DataSourceUtils.getConnection(dataSource);
//			pst = conn.prepareStatement(SQL_LINK_TAG_NEWS);
//			pst.setLong(1, newsId);
//			pst.setLong(2, tagId);
//			pst.executeUpdate();
//		} catch (SQLException e) {
//			throw new DaoException("Inserting tag id " + tagId + " to news id " + newsId + " failed", e);
//		} finally {
//			DBUtil.closeResources(dataSource,conn, pst);
//		}
//	}

//	@Override
//	public void detachTagFromNewsById(Long tagId, Long newsId) throws DaoException {
//		Connection conn = null;
//		PreparedStatement pst = null;
//		try {
//			conn = DataSourceUtils.getConnection(dataSource);
//			pst = conn.prepareStatement(SQL_DETACH_TAG_NEWS);
//			pst.setLong(1, newsId);
//			pst.setLong(2, tagId);
//			pst.executeUpdate();
//		} catch (SQLException e) {
//			throw new DaoException("Deleting tag id " + tagId + "from news id " + newsId + " failed", e);
//		} finally {
//			DBUtil.closeResources(dataSource,conn, pst);
//		}
//	}

	@Override
	public List<TagTO> getTagListByNewsId(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			List<TagTO> tagList = new ArrayList<TagTO>();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_NEWS_TAGS);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			while(rs.next()) {
				TagTO tag = new TagTO();
				tag.setTagId(rs.getLong(COLUMN_TAG_ID));
				tag.setTagName(rs.getString(COLUMN_TAG_NAME));
				tagList.add(tag);
			}
			return tagList;
		} catch (SQLException e) {
			throw new DaoException("Selecting tags by news id " + newsId + " failed", e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst, rs);
		}
	}

	@Override
	public void detachAllTagsFromNews(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_DETACH_ALL_TAG_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Deleting tags by news id " + newsId + " failed", e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst);
		}
	}

	@Override
	public void attachTagListToNewsById(List<Long> tagList, Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_ATTACH_TAG_NEWS);
			for(Long tagId : tagList) {
				pst.setLong(1, newsId);
				pst.setLong(2, tagId);
				pst.addBatch();
			}
			pst.executeBatch();
		} catch (SQLException e) {
			throw new DaoException("Inserting tag list to news id " + newsId + " failed", e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst);
		}
	}

	@Override
	public List<TagTO> getAllTags() throws DaoException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			List<TagTO> tagList = new ArrayList<TagTO>();
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			rs = st.executeQuery(SQL_GET_ALL_TAGS);
			while(rs.next()) {
				TagTO tag = new TagTO();
				tag.setTagId(rs.getLong(COLUMN_TAG_ID));
				tag.setTagName(rs.getString(COLUMN_TAG_NAME));
				tagList.add(tag);
			}
			return tagList;
		} catch (SQLException e) {
			throw new DaoException("Selecting all tags failed", e);
		} finally {
			DBUtil.closeResources(dataSource,conn, st, rs);
		}
	}

	@Override
	public void detachAllTagsByNewsIdList(List<Long> newsIdList) throws DaoException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			String query = QueryBuilder.buildQueryDetachTagsByNewsList(newsIdList);
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			rs = st.executeQuery(query);
		} catch (SQLException e) {
			throw new DaoException("Detaching tags by news ids failed", e);
		} finally {
			DBUtil.closeResources(dataSource,conn, st, rs);
		}
	}

	@Override
	public void detachTagById(Long tagId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_DETACH_TAG);
			pst.setLong(1, tagId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Detaching tags by news ids failed", e);
		} finally {
			DBUtil.closeResources(dataSource,conn, pst, rs);
		}
		
	}

}
