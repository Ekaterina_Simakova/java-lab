package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewsTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long newsId;
	
	@NotNull(message="Enter the title!")
	@Size(min=1, max=30, message="The size of title is from 1 to 30!")
	private String title;
	
	@NotNull(message="Enter the short text!")
	@Size(min=1, max=100, message="The size of short text is from 1 to 100!")
	private String shortText;
	
	@NotNull(message="Enter the full text!")
	@Size(min=1, max=2000, message="The size of full text is from 1 to 2000!")
	private String fullText;
	
	private Date creationDate;
	
	private Date modificationDate;
	
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	
	public Long getNewsId() {
		return this.newsId;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	
	public String getShortText() {
		return this.shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NewsTO other = (NewsTO) obj;
		if (creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else {
			if (!creationDate.equals(other.creationDate)) {
				return false;
			}
		}
		if (fullText == null) {
			if (other.fullText != null) {
				return false;
			}
		} else {
			if (!fullText.equals(other.fullText)) {
				return false;
			}
		}
		if (modificationDate == null) {
			if (other.modificationDate != null) {
				return false;
			}
		} else {
			if (!modificationDate.equals(other.modificationDate)) {
				return false;
			}
		}
		if (newsId == null) {
			if (other.newsId != null) {
				return false;
			}
		} else {
			if (!newsId.equals(other.newsId)) {
				return false;
			}
		}
		if (shortText == null) {
			if (other.shortText != null) {
				return false;
			}
		} else {
			if (!shortText.equals(other.shortText)) {
				return false;
			}
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else {
			if (!title.equals(other.title)) {
				return false;
			}
		}
		return true;
	}
}
