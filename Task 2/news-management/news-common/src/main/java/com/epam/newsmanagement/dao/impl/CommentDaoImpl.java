package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.DBUtil;
import com.epam.newsmanagement.util.QueryBuilder;

/**
 * @see CommentDao
 * @author Katsiaryna Simakova
 *
 */
public class CommentDaoImpl implements CommentDao {

	private BasicDataSource dataSource;

	private static final String COLUMN_NEWS_ID = "NEWS_ID";

	private static final String COLUMN_COMMENT_ID = "COMMENT_ID";
	private static final String COLUMN_COMMENT_TEXT = "COMMENT_TEXT";
	private static final String COLUMN_COMMENT_CREATION_DATE = "CREATION_DATE";

	private static final String SQL_GET_COMMENT_BY_ID = "SELECT NEWS_ID, COMMENT_TEXT, "
			+ "CREATION_DATE FROM COMMENTS WHERE COMMENT_ID=?";
	private static final String SQL_INSERT_COMMENT = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID,"
			+ "COMMENT_TEXT, CREATION_DATE) VALUES(COMMENT_ID_SEQ.NEXTVAL,?,?,?)";
	private static final String SQL_DELETE_COMMENT_BY_ID = "DELETE FROM COMMENTS WHERE COMMENT_ID=?";
	private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET COMMENT_TEXT=? WHERE COMMENT_ID=?";
	private static final String SQL_GET_COMMENTS_BY_NEWS_ID = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE "
			+ "FROM COMMENTS WHERE NEWS_ID=? ORDER BY CREATION_DATE DESC";

	/**
	 * Sets the <code>BasicDataSource</code> object for access to the database.
	 * 
	 * @param dataSource
	 *            <code>BasicDataSource</code> object
	 * @see org.apache.commons.dbcp.BasicDataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long addComment(CommentTO comment) throws DaoException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			Long commentId = null;
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_INSERT_COMMENT,
					new String[] { COLUMN_COMMENT_ID });
			pst.setLong(1, comment.getNewsId());
			pst.setString(2, comment.getCommentText());
			pst.setTimestamp(3, comment.getCreationDate());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs.next()) {
				commentId = rs.getLong(1);
			}
			return commentId;
		} catch (SQLException e) {
			throw new DaoException("Inserting comment failed " + comment, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public void deleteCommentById(Long commentId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_DELETE_COMMENT_BY_ID);
			pst.setLong(1, commentId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Deleting comment failed, id is " + commentId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public void editComment(CommentTO comment) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_UPDATE_COMMENT);
			pst.setString(1, comment.getCommentText());
			pst.setLong(2, comment.getCommentId());
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Updating comment failed, " + comment, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst);
		}
	}

	@Override
	public CommentTO getCommentById(Long commentId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			CommentTO comment = new CommentTO();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_COMMENT_BY_ID);
			pst.setLong(1, commentId);
			rs = pst.executeQuery();
			if (rs.next()) {
				comment.setCommentId(commentId);
				comment.setCommentText(rs.getString(COLUMN_COMMENT_TEXT));
				comment.setCreationDate(rs
						.getTimestamp(COLUMN_COMMENT_CREATION_DATE));
				comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
			}
			return comment;
		} catch (SQLException e) {
			throw new DaoException("Selecting comment failed, id is " + commentId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public List<CommentTO> getCommentListByNewsId(Long newsId) throws DaoException {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			List<CommentTO> commentList = new ArrayList<CommentTO>();
			conn = DataSourceUtils.getConnection(dataSource);
			pst = conn.prepareStatement(SQL_GET_COMMENTS_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			while (rs.next()) {
				CommentTO comment = new CommentTO();
				comment.setCommentId(rs.getLong(COLUMN_COMMENT_ID));
				comment.setCommentText(rs.getString(COLUMN_COMMENT_TEXT));
				comment.setCreationDate(rs
						.getTimestamp(COLUMN_COMMENT_CREATION_DATE));
				comment.setNewsId(newsId);
				commentList.add(comment);
			}
			return commentList;
		} catch (SQLException e) {
			throw new DaoException("Selecting comments failed, news id is " + newsId, e);
		} finally {
			DBUtil.closeResources(dataSource, conn, pst, rs);
		}
	}

	@Override
	public void deleteAllCommentsByNewsList(List<Long> newsIdList) throws DaoException {
		Connection conn = null;
		Statement st = null;
		try {
			String query = QueryBuilder.buildQueryDeleteCommentsByNewsList(newsIdList);
			conn = DataSourceUtils.getConnection(dataSource);
			st = conn.createStatement();
			st.executeUpdate(query);
		} catch (SQLException e) {
			throw new DaoException("Deleting comments by news list failed", e);
		} finally {
			DBUtil.closeResources(dataSource, conn, st);
		}
	}

}
