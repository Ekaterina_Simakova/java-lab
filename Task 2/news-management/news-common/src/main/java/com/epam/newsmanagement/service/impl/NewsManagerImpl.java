package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * @see NewsManager
 * @author Katsiaryna_Simakova
 *
 */
public class NewsManagerImpl implements NewsManager {
	
	private static final Logger logger = Logger.getLogger("serviceLogger");
	
	private AuthorService authorService;
	private CommentService commentService;
	private NewsService newsService;
	private TagService tagService;

	public void setAuthorService(AuthorServiceImpl authorService) {
		this.authorService = authorService;
	}
	
	public void setCommentService(CommentServiceImpl commentService) {
		this.commentService = commentService;
	}
	
	public void setNewsService(NewsServiceImpl newsService) {
		this.newsService = newsService;
	}
	
	public void setTagService(TagServiceImpl tagService) {
		this.tagService = tagService;
	}

	/**
	 * Gets tags and author by news id and composes new NewsVO object.
	 * 
	 * @param newsTO NewsTO object
	 * @return NewsVO object
	 * @throws ServiceException if some Service operations failed
	 */
	private NewsVO composeNews(Long newsId) throws ServiceException {
		List<TagTO> tagList = tagService.getTagListByNewsId(newsId);
		List<CommentTO> commentList = commentService.getCommentsByNewsId(newsId);
		AuthorTO author = authorService.getAuthorByNewsId(newsId);
		NewsVO newsVO = new NewsVO();
		newsVO.setAuthor(author);
		newsVO.setTagList(tagList);
		newsVO.setCommentList(commentList);
		return newsVO;
	}
	
	/**
	 * Conducts the transaction. Transaction contains adding news, attaching tags 
	 * and author to this news.
	 * 
	 * @exception ServiceException if some Service operations failed
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=ServiceException.class)
	public Long addNews(NewsTO news, List<Long> tagIdList, Long authorId) throws ServiceException {
		try {
			news.setModificationDate(news.getCreationDate());
			Long newsId = newsService.addNews(news);
			if(tagIdList != null) {
				tagService.attachTagListToNews(tagIdList, newsId);
			}
			authorService.attachAuthorToNewsById(authorId, newsId);
			return newsId;
		} catch(ServiceException e) {
			logger.error("Adding news with tags and author failed", e);
			throw e;
		}
	}

	/**
	 * @see NewsService#editNews(NewsTO)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=ServiceException.class)
	public void editNews(NewsTO news, List<Long> idTagList, Long authorId) throws ServiceException {
		try {
			tagService.detachTagListFromNews(news.getNewsId());
			authorService.detachAuthorFromNewsByNewsId(news.getNewsId());
			if(idTagList != null) {
				tagService.attachTagListToNews(idTagList, news.getNewsId());
			}
			authorService.attachAuthorToNewsById(authorId, news.getNewsId());
			newsService.editNews(news);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Conducts the transaction. Transaction contains deleting news, unbinding tags 
	 * and author of this news.
	 * 
	 * @exception ServiceException if some Service operations failed
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=ServiceException.class)
	public void deleteNewsList(List<Long> newsIdList) throws ServiceException {
		try {
			tagService.detachTagsFromNewsList(newsIdList);
			commentService.deleteAllCommentsByNewsList(newsIdList);
			authorService.detachAuthorsByNewsList(newsIdList);
			newsService.deleteNewsListByIds(newsIdList);
		} catch(ServiceException e) {
			logger.error("Deleting news failed", e);
			throw e;
		}
	}

	@Override
	public List<NewsVO> getNews(SearchCriteria criteria, Long pageNumber, Long numberNewsOnPage) throws ServiceException {
		try {
			List<NewsTO> newsTOList = newsService.getNews(criteria, pageNumber, numberNewsOnPage);
			List<NewsVO> newsVOList = new ArrayList<NewsVO>();
			for(NewsTO newsTO : newsTOList) {
				NewsVO newsVO = composeNews(newsTO.getNewsId());
				newsVO.setNewsTO(newsTO);
				newsVOList.add(newsVO);
			}
			return newsVOList;
		} catch(ServiceException e) {
			logger.error("Searching news failed", e);
			throw e;
		}
	}

	@Override
	public NewsVO getNewsById(Long newsId) throws ServiceException {
		try {
			NewsTO newsTO = newsService.getNewsById(newsId);
			NewsVO newsVO = composeNews(newsTO.getNewsId());
			newsVO.setNewsTO(newsTO);
			return newsVO;
		} catch(ServiceException e) {
			logger.error("Getting news failed", e);
			throw e;
		}
	}

	/**
	 * @see NewsService#countNews()
	 */ 
	@Override
	public Long countNews(SearchCriteria criteria) throws ServiceException {
		try {
			return newsService.countNews(criteria);
		} catch(ServiceException e) {
			logger.error("Counting news failed", e);
			throw e;
		}
	}

	/**
	 * @see AuthorService#addAuthor(AuthorTO)
	 */ 
	@Override
	public Long addAuthor(AuthorTO author) throws ServiceException {
		try {
			return authorService.addAuthor(author);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see AuthorService#expireAuthorById(Long)
	 */ 
	@Override
	public void expireAuthorById(Long authorId) throws ServiceException {
		try {
			authorService.expireAuthorById(authorId);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see CommentService#addComment(CommentTO)
	 */ 
	@Override
	public Long addComment(CommentTO comment) throws ServiceException {
		try {
			
			return commentService.addComment(comment);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * @see CommentService#deleteCommentById(Long)
	 */ 
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentService.deleteCommentById(commentId);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

//	/**
//	 * @see CommentService#editComment(CommentTO)
//	 */ 
//	@Override
//	public void editComment(CommentTO comment) throws ServiceException {
//		try {
//			commentService.editComment(comment);
//		} catch(ServiceException e) {
//			logger.error(e.getMessage(), e);
//			throw e;
//		}
//	}

	/**
	 * @see TagService#addTag(TagTO)
	 */ 
	@Override
	public Long addTag(TagTO tag) throws ServiceException {
		try {
			return tagService.addTag(tag);
		} catch(ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void editTag(TagTO tag) throws ServiceException {
		try {
			tagService.editTag(tag);
		} catch(ServiceException e) {
			logger.error("Editing tag failed", e);
			throw e;
		}
	}

	@Override
	public List<TagTO> getAllTags() throws ServiceException {
		return tagService.getAllTags();
	}

	@Override
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		return authorService.getAllAuthors();
	}

	@Override
	public Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) throws ServiceException {
		return newsService.getNextAndPrevious(newsId, criteria);
	}

	@Override
	public AuthorTO getAuthorById(Long authorId) throws ServiceException {
		return authorService.getAuthorById(authorId);
	}

	@Override
	public void editAuthor(AuthorTO author) throws ServiceException {
		authorService.editAuthor(author);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=ServiceException.class)
	public void deleteTagById(Long tagId) throws ServiceException {
		tagService.detachTagById(tagId);
		tagService.deleteTagById(tagId);
	}

}
