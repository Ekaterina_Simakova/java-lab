package com.epam.newsmanagement.util;

import java.util.List;

import com.epam.newsmanagement.entity.SearchCriteria;

public class QueryBuilder {
	
	private static final String SQL_GET_NEWS_START = "Select IDS.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, "
			+ "N.CREATION_DATE, N.MODIFICATION_DATE FROM (SELECT NEWS_ID, RN FROM "
			+ "(SELECT SORT_IDS.NEWS_ID, ROWNUM AS RN FROM (SELECT N.NEWS_ID FROM ( ";
	private static final String SQL_GET_NEWS_END = ") N LEFT JOIN (SELECT COUNT(COMMENT_ID) AS NUMB, NEWS_ID "
			+ "FROM COMMENTS GROUP BY NEWS_ID) COM ON N.NEWS_ID=COM.NEWS_ID ORDER BY N.MODIFICATION_DATE DESC, "
			+ "COM.NUMB DESC) SORT_IDS) ALL_IDS WHERE ALL_IDS.RN>=? AND ALL_IDS.RN<=? ) IDS INNER JOIN NEWS N "
			+ "ON IDS.NEWS_ID=N.NEWS_ID ORDER BY IDS.RN";
	
	private static final String SQL_COUNT_NEWS_START = "SELECT COUNT(NEWS_ID) AS COUNT FROM (";
	private static final String SQL_COUNT_NEWS_END = ")";
	
	private static final String SQL_GET_NEXT_AND_PREVIOUS_START = "SELECT NEWS_ID, NEXT_ID, PREV_ID FROM "
			+ "(SELECT NEWS_ID, LEAD(NEWS_ID,1) OVER (ORDER BY RN) AS NEXT_ID, LAG (NEWS_ID,1) OVER(ORDER BY RN) "
			+ "AS PREV_ID FROM (SELECT SORT_IDS.NEWS_ID, ROWNUM AS RN FROM (SELECT N.NEWS_ID FROM (";
	private static final String SQL_GET_NEXT_AND_PREVIOUS_END = ") N LEFT JOIN (SELECT COUNT(COMMENT_ID) AS "
			+ "NUMB, NEWS_ID FROM COMMENTS GROUP BY NEWS_ID) COM ON N.NEWS_ID=COM.NEWS_ID ORDER BY "
			+ "N.MODIFICATION_DATE DESC, COM.NUMB DESC) SORT_IDS)) ALL_IDS WHERE NEWS_ID=?";
	
	private static final String GET_NEWS_IDS_BY_CRITERIA = "SELECT DISTINCT N.NEWS_ID, N.MODIFICATION_DATE FROM NEWS N "
			+ "LEFT JOIN NEWS_AUTHOR NA ON N.NEWS_ID=NA.NEWS_ID LEFT JOIN NEWS_TAG NT ON N.NEWS_ID=NT.NEWS_ID";
	private static final String SQL_AND = " AND ";
	private static final String SQL_WHERE = " WHERE ";
	private static final String SQL_AUTHOR_CONDITION = "NA.AUTHOR_ID=";
	private static final String SQL_TAGS_CONDITION = "NT.TAG_ID IN";
	
	private static final String SQL_NEWS_ID_LIST_CONDITION = "WHERE NEWS_ID IN(";
	
	private static final String SQL_DETACH_TAGS_BY_NEWS_LIST = "DELETE FROM NEWS_TAG ";
	
	private static final String SQL_DELETE_COMMENTS_BY_NEWS_LIST = "DELETE FROM COMMENTS ";
	
	private static final String SQL_DETACH_AUTHORS_BY_NEWS_LIST = "DELETE FROM NEWS_AUTHOR ";
	
	private static final String SQL_DELETE_NEWS_LIST = "DELETE FROM NEWS ";
	
	public static String buildQuerySearchNews(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(SQL_GET_NEWS_START);
		query.append(buildQueryGetNewsIdsByCriteria(criteria));
		query.append(SQL_GET_NEWS_END);
		return query.toString();
	}	
	
	public static String buildQueryCountNews(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(SQL_COUNT_NEWS_START);
		query.append(buildQueryGetNewsIdsByCriteria(criteria));
		query.append(SQL_COUNT_NEWS_END);
		return query.toString();
	}
	
	public static String buildQueryGetNextAndPreviousIds(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(SQL_GET_NEXT_AND_PREVIOUS_START);
		query.append(buildQueryGetNewsIdsByCriteria(criteria));
		query.append(SQL_GET_NEXT_AND_PREVIOUS_END);
		return query.toString();
	}
	
	public static String buildQueryDetachTagsByNewsList(List<Long> newsIdList) {
		StringBuilder query = new StringBuilder().append(SQL_DETACH_TAGS_BY_NEWS_LIST);
		query.append(buildQueryOfNewsListCondition(newsIdList));
		return query.toString();
	}
	
	public static String buildQueryDeleteCommentsByNewsList(List<Long> newsIdList) {
		StringBuilder query = new StringBuilder().append(SQL_DELETE_COMMENTS_BY_NEWS_LIST);
		query.append(buildQueryOfNewsListCondition(newsIdList));
		return query.toString();
	}
	
	public static String buildQueryDetachAuthorsByNewsList(List<Long> newsIdList) {
		StringBuilder query = new StringBuilder().append(SQL_DETACH_AUTHORS_BY_NEWS_LIST);
		query.append(buildQueryOfNewsListCondition(newsIdList));
		return query.toString();
	}
	
	public static String buildQueryDeleteNewsList(List<Long> newsIdList) {
		StringBuilder query = new StringBuilder().append(SQL_DELETE_NEWS_LIST);
		query.append(buildQueryOfNewsListCondition(newsIdList));
		return query.toString();
	}
	
	private static String buildQueryGetNewsIdsByCriteria(SearchCriteria criteria) {
		StringBuilder query = new StringBuilder().append(GET_NEWS_IDS_BY_CRITERIA);
		if(criteria != null) {
			Long authorId = criteria.getAuthorId();
			List<Long> tagIdList = criteria.getTagIdList();
			if(authorId != null || tagIdList != null) {
				query.append(SQL_WHERE);
				if(authorId != null) {
					query.append(SQL_AUTHOR_CONDITION);
					query.append(authorId);
				}
				if(tagIdList != null && tagIdList.size() != 0) {
					if(authorId != null) {
						query.append(SQL_AND);
					}
					query.append(SQL_TAGS_CONDITION);
					query.append("(");
					for(Long tagId : tagIdList) {
						query.append(tagId);
						query.append(",");
					}
					query.setCharAt(query.length()-1, ')');
				}
			}
		}
		return query.toString();
	}
	
	private static String buildQueryOfNewsListCondition(List<Long> newsIdList) {
		StringBuilder query = new StringBuilder().append(SQL_NEWS_ID_LIST_CONDITION);
		for(Long newsId : newsIdList) {
			query.append(newsId).append(",");
		}
		if(newsIdList != null && newsIdList.size() != 0) {
			query.setCharAt(query.length()-1, ')');
		} else {
			query.append(')');
		}
		return query.toString();
	}
}
