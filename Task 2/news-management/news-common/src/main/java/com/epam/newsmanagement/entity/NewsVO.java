package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

public class NewsVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private NewsTO newsTO;
	private List<TagTO> tagList;
	private List<CommentTO> commentList;
	private AuthorTO author;
	
	public NewsTO getNewsTO() {
		return newsTO;
	}

	public void setNewsTO(NewsTO newsTO) {
		this.newsTO = newsTO;
	}

	public List<TagTO> getTagList() {
		return tagList;
	}

	public void setTagList(List<TagTO> tagList) {
		this.tagList = tagList;
	}

	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	public List<CommentTO> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<CommentTO> commentList) {
		this.commentList = commentList;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result + ((newsTO == null) ? 0 : newsTO.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null) {
				return false;
			}
		} else {
			if (!author.equals(other.author)) {
				return false;
			}
		}
		if (commentList == null) {
			if (other.commentList != null) {
				return false;
			}
		} else {
			if (!commentList.equals(other.commentList)) {
				return false;
			}
		}
		if (newsTO == null) {
			if (other.newsTO != null) {
				return false;
			}
		} else {
			if (!newsTO.equals(other.newsTO)) {
				return false;
			}
		}
		if (tagList == null) {
			if (other.tagList != null) {
				return false;
			}
		} else { 
			if (!tagList.equals(other.tagList)) {
				return false;
			}
		}
		return true;
	}
	
}
