package com.epam.newsmanagement.service;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsManager {

	/**
	 * Adds news with author and the list of tags.
	 * 
	 * @param news NewsVO object
	 * @return news id
	 * @throws ServiceException
	 */
	Long addNews(NewsTO news, List<Long> tagIdList, Long authorId) throws ServiceException;

	/**
	 * Edits news data.
	 * 
	 * @param news updated NewsTO object
	 * @throws ServiceException
	 */
	void editNews(NewsTO news, List<Long> idTagList, Long authorId) throws ServiceException;

	/**
	 * Deletes news by ids.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void deleteNewsList(List<Long> newsIdList) throws ServiceException;

	/**
	 * Gets the list of news sorted accoding to SearchCriteria object and the number of page.
	 * 
	 * @return the list of NewsVO objects
	 * @throws ServiceException
	 */
	List<NewsVO> getNews(SearchCriteria criteria, Long pageNumber, Long numberNewsOnPage) throws ServiceException;

	/**
	 * Gets full data of required news by its id.
	 * 
	 * @param newsId news id
	 * @return NewsVO object
	 * @throws ServiceException
	 */
	NewsVO getNewsById(Long newsId) throws ServiceException;
	
	/**
	 * Gets the number of pages according to number of news at one page.
	 * 
	 * @param newsOnPage number of news at one page
	 * @return number of pages
	 * @throws ServiceException
	 */
	 Long countNews(SearchCriteria criteria) throws ServiceException;

	/**
	 * Adds new author.
	 * 
	 * @param author AuthorTO object
	 * @return authors id
	 * @throws ServiceException
	 */
	Long addAuthor(AuthorTO author) throws ServiceException;

	/**
	 * Makes required author expireded, it replaces removing him.
	 * 
	 * @param authorId author id
	 * @throws ServiceException
	 */
	void expireAuthorById(Long authorId) throws ServiceException;

	/**
	 * Adds new comment.
	 * 
	 * @param comment CommentTO object
	 * @param newsId news id
	 * @return comments id
	 * @throws ServiceException
	 */
	Long addComment(CommentTO comment) throws ServiceException;

	/**
	 * Deletes required comment by its id.
	 * 
	 * @param commentId comment id
	 * @throws ServiceException
	 */
	void deleteComment(Long commentId) throws ServiceException;
	
//	/**
//	 * Edits comment data.
//	 * 
//	 * @param comment updated CommentTO object
//	 * @throws ServiceException
//	 */
//	void editComment(CommentTO comment) throws ServiceException;

	/**
	 * Adds tag.
	 * 
	 * @param tag TagTO object
	 * @param newsId news id
	 * @return tags id
	 * @throws ServiceException
	 */
	Long addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Edits tag.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	void editTag(TagTO tag) throws ServiceException;
	
	/**
	 * Gets all tags.
	 * 
	 * @return the list of tags
	 * @throws ServiceException
	 */
	List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Gets all authors.
	 * 
	 * @return the list of authors
	 * @throws ServiceException
	 */
	List<AuthorTO> getAllAuthors() throws ServiceException;
	
	/**
	 * Checks whether there are a next news and a previous news.
	 * 
	 * @param newsId
	 * @return result of checking in map
	 * @throws ServiceException
	 */
	Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) throws ServiceException;
	
	/**
	 * It gets AuthorTO object by author id.
	 * 
	 * @param authorId
	 * @return
	 * @throws ServiceException
	 */
	AuthorTO getAuthorById(Long authorId) throws ServiceException;
	
	/**
	 * It edits author.
	 * 
	 * @param author AuthorTO object
	 * @throws ServiceException
	 */
	void editAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * It deletes tag by id.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	void deleteTagById(Long tagId) throws ServiceException;

}
