package com.epam.newsmanagement.service.impl;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * @see NewsService
 * @author Katsiaryna_Simakova
 *
 */
public class NewsServiceImpl implements NewsService {
	
	private NewsDao newsDao;
	
	/**
	 * Sets the data access object.
	 * 
	 * @param newsDao NewsDao implementation
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	@Override
	public Long addNews(NewsTO news) throws ServiceException {
		try {
			return newsDao.addNews(news);
		} catch (DaoException e) {
			throw new ServiceException("Adding news failed",e);
		}
	}

	@Override
	public void editNews(NewsTO news) throws ServiceException {
		try {
			newsDao.editNews(news);
		} catch (DaoException e) {
			throw new ServiceException("Editing news failed",e);
		}
	}

	@Override
	public NewsTO getNewsById(Long newsId) throws ServiceException {
		try {
			return newsDao.getNewsById(newsId);
		} catch (DaoException e) {
			throw new ServiceException("Getting news failed",e);
		}
	}

	@Override
	public Long countNews(SearchCriteria criteria) throws ServiceException {
		try {
			return newsDao.countNews(criteria);
		} catch (DaoException e) {
			throw new ServiceException("Counting news failed",e);
		}
	}

	@Override
	public List<NewsTO> getNews(SearchCriteria criteria, long pageNumber, long numberNewsOnPage) throws ServiceException {
		try {
			long firstNewsNumber = numberNewsOnPage*(pageNumber-1)+1;
			long lastNewsNumber = firstNewsNumber + numberNewsOnPage - 1;
			return newsDao.getNews(criteria, firstNewsNumber, lastNewsNumber);
		} catch (DaoException e) {
			throw new ServiceException("Searching news failed",e);
		}
	}

	@Override
	public Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) throws ServiceException {
		try {
			return newsDao.getNextAndPrevious(newsId, criteria);
		} catch (DaoException e) {
			throw new ServiceException("Checking of having next and previous news failed",e);
		}
	}

	@Override
	public void deleteNewsListByIds(List<Long> newsIdList) throws ServiceException {
		try {
			newsDao.deleteNewsListByIds(newsIdList);
		} catch (DaoException e) {
			throw new ServiceException("Deleting the list of news failed",e);
		}
	}

}
