package com.epam.newsmanagement.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class NewsFormVO {
	
	private static final long serialVersionUID = 1L;

	@Valid
	private NewsTO newsTO;
	
	@NotNull(message="Tag list is null")
	private List<Long> tagIdList;
	
	@NotNull(message="Choose the author!")
	private Long authorId;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((newsTO == null) ? 0 : newsTO.hashCode());
		result = prime * result
				+ ((tagIdList == null) ? 0 : tagIdList.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsFormVO other = (NewsFormVO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (newsTO == null) {
			if (other.newsTO != null)
				return false;
		} else if (!newsTO.equals(other.newsTO))
			return false;
		if (tagIdList == null) {
			if (other.tagIdList != null)
				return false;
		} else if (!tagIdList.equals(other.tagIdList))
			return false;
		return true;
	}

	/**
	 * @return the newsTO
	 */
	public NewsTO getNewsTO() {
		return newsTO;
	}

	/**
	 * @param newsTO the newsTO to set
	 */
	public void setNewsTO(NewsTO newsTO) {
		this.newsTO = newsTO;
	}

	/**
	 * @return the tagIdList
	 */
	public List<Long> getTagIdList() {
		return tagIdList;
	}

	/**
	 * @param tagIdList the tagIdList to set
	 */
	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	/**
	 * @return the authorId
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * @param authorId the authorId to set
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
