package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface CommentService {
	
	/**
	 * Adds new comment.
	 * 
	 * @param comment CommentTO object
	 * @return comment id
	 * @throws ServiceException
	 */
	Long addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Deletes comment by id.
	 * 
	 * @param commentId comment id
	 * @throws ServiceException
	 */
	void deleteCommentById(Long commentId) throws ServiceException;
	
//	/**
//	 * Edits comments data.
//	 * 
//	 * @param comment updated CommentTO object
//	 * @throws ServiceException
//	 */
//	void editComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Deletes all comments by list of news ids.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void deleteAllCommentsByNewsList(List<Long> newsIdList) throws ServiceException;
	
	/**
	 * Gets all comments by news id.
	 * 
	 * @param newsId news id
	 * @return the list of CommentTO objects
	 * @throws ServiceException
	 */
	List<CommentTO> getCommentsByNewsId(Long newsId) throws ServiceException;
	
}
