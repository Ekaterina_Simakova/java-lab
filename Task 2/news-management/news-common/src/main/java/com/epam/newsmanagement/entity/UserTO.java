package com.epam.newsmanagement.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserTO {
	
	private Long userId;
	
	@NotNull
	@Size(min=1, max=20, message="Name is from 1 to 20 characters!")
	private String userName;
	
	@NotNull
	@Size(min=1, max=20, message="Login is from 1 to 20 characters!")
	private String login;
	
	@NotNull
	@Size(min=1, max=30, message="Password is from 1 to 20 characters!")
	private String password;
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	

}
