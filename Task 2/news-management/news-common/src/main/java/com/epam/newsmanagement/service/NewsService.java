package com.epam.newsmanagement.service;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsService {

	/**
	 * Adds news.
	 * 
	 * @param news NewsTO object
	 * @return news id
	 * @throws ServiceException
	 */
	Long addNews(NewsTO news) throws ServiceException;

	/**
	 * Edits news data.
	 * 
	 * @param news updated NewsTO object
	 * @throws ServiceException
	 */
	void editNews(NewsTO news) throws ServiceException;
	
	/**
	 * Deletes the list of news.
	 * 
	 * @param newsIdList the list of news ids
	 * @throws ServiceException
	 */
	void deleteNewsListByIds(List<Long> newsIdList) throws ServiceException;

	/**
	 * Gets news by id.
	 * 
	 * @param newsId news id
	 * @return NewsTO object
	 * @throws ServiceException
	 */
	NewsTO getNewsById(Long newsId) throws ServiceException;

	/**
	 * Gets the count of news according to SearchCriteria object.
	 * 
	 * @return the count of news
	 * @throws ServiceException
	 */
	Long countNews(SearchCriteria criteria) throws ServiceException;
	
	/**
	 * Gets sorted by modification date and number of comments the list of news.
	 * 
	 * @return the list of NewsTO objects
	 * @throws ServiceException
	 */
	List<NewsTO> getNews(SearchCriteria criteria, long pageNumber, long numberNewsOnPage) throws ServiceException; 

	/**
	 * Checks whether there are a next news and a previous news.
	 * 
	 * @param newsId
	 * @return result of checking in map
	 * @throws ServiceException
	 */
	Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) throws ServiceException;

}
