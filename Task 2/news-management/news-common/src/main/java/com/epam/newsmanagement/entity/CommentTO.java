package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.Size;

public class CommentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long commentId;
	
	// @NotNull(message="Unknown news.")
	private Long newsId;
	
	// @NotNull(message="Comment is empty.")
	@Size(min=1, max=100, message="Size of comment from 1 to 100 letters.")
	private String commentText;
	
	private Timestamp creationDate;

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CommentTO other = (CommentTO) obj;
		if (commentId != null) {
			if (commentId != other.getCommentId()) {
				return false;
			}
		} else {
			if (other.getCommentId() != null) {
				return false;
			}
		}
		if (newsId != null) {
			if(!newsId.equals(other.getNewsId())) {
				return false;
			}
		} else {
			if (other.getNewsId() != null) {
				return false;
			}
		}
		if (commentText != null) {
			if (!commentText.equals(other.getCommentText())) {
				return false;
			} 
		} else {
			if (other.getCommentText() != null) {
				return false;
			}
		}
		if(creationDate != null) {
			if(!creationDate.equals(other.creationDate)) {
				return false;
			}
		} else {
			if(other.getCreationDate() != null) {
				return false;
			}
		}
		return true;
	}

}
