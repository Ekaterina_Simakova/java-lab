package com.epam.newsmanagement.dao;

import java.util.List;
import java.util.Map;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsDao {
	
	/**
	 * Adds news, i.e. <code>NewsTO</code> object.
	 * 
	 * @param news <code>NewsTO</code> object
	 * @return generated news id
	 * @throws DaoException
	 */
	Long addNews(NewsTO news) throws DaoException;
	
	/**
	 * Deletes the list of news.
	 * 
	 * @param newsIdList the list of news ids
	 * @throws ServiceException
	 */
	void deleteNewsListByIds(List<Long> newsIdList) throws DaoException;
	
	/**
	 * Edits news information.
	 * 
	 * @param news updated <code>NewsTO</code> object
	 * @throws DaoException
	 */
	void editNews(NewsTO news) throws DaoException;
	
	/**
	 * Gets list of news accoding to SearchCriteria object.
	 * 
	 * @return list of <code>NewsTO</code> objects
	 * @throws DaoException
	 */
	List<NewsTO> getNews(SearchCriteria criteria, long firstNewsNumber, long lastNewsNumber) throws DaoException;
	
	/**
	 * Gets news data by its id.
	 * 
	 * @param id required news id
	 * @return <code>NewsTO</code> object
	 * @throws DaoException when getting will be failed
	 */
	NewsTO getNewsById(Long id) throws DaoException;
	
	/**
	 * Gets the number of news accordin to SearchCriteria object.
	 * 
	 * @return the number of news
	 * @throws DaoException
	 */
	Long countNews(SearchCriteria criteria) throws DaoException;
	
	/**
	 * Checks whether there are a next news and a previous news.
	 * 
	 * @param newsId
	 * @return result of checking in map
	 * @throws DaoException
	 */
	Map<String,Long> getNextAndPrevious(Long newsId, SearchCriteria criteria) throws DaoException;
	
}
