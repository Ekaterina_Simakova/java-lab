package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface TagService {
	
	/**
	 * Method adds tag.
	 * 
	 * @param tag TagTO object
	 * @return tags id
	 * @throws ServiceException
	 */
	Long addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Method edits tag data.
	 * 
	 * @param tag updated TagTO object
	 * @throws ServiceException
	 */
	void editTag(TagTO tag) throws ServiceException;
	
	/**
	 * Method attaches tag list to news by ids.
	 * 
	 * @param idTagList list of tag ids
	 * @param newsId news id
	 */
	void attachTagListToNews(List<Long> idTagList, Long newsId) throws ServiceException;
	
	/**
	 * Method detaches the list of tags from news by news id.
	 * 
	 * @param newsId news id
	 * @throws ServiceException
	 */
	void detachTagListFromNews(Long newsId) throws ServiceException;
	
	/**
	 * Method detaches the list of tags by news list.
	 * 
	 * @param newsIdList list of news ids
	 * @throws ServiceException
	 */
	void detachTagsFromNewsList(List<Long> newsIdList) throws ServiceException;
	
	/**
	 * It gets all tags by news id.
	 * 
	 * @param newsId news id
	 * @return the list of TagTO objects
	 * @throws ServiceException
	 */
	List<TagTO> getTagListByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * It gets all tags.
	 * 
	 * @return the list of tags
	 * @throws ServiceException
	 */
	List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Method deletes tag by id.
	 * 
	 * @param tagId tag id
	 * @throws ServiceException
	 */
	void deleteTagById(Long tagId) throws ServiceException;
	
	/**
	 * Method detaches tag form all news.
	 * 
	 * @param tagId tag id
	 * @throws ServiceException
	 */
	void detachTagById(Long tagId) throws ServiceException;

}
