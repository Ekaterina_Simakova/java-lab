package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.NewsDaoImpl;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:testContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("dataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class NewsDaoImplTest {
	
	@Autowired
	private NewsDaoImpl newsDao;
	
	@Test
	public void addNewsTest() throws DaoException {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setTitle("title");
		expectedNews.setShortText("short");
		expectedNews.setFullText("full");
		expectedNews.setCreationDate(Date.valueOf("2015-02-03"));
		expectedNews.setModificationDate(Date.valueOf("2015-02-03"));
		Long newsId = newsDao.addNews(expectedNews);
		expectedNews.setNewsId(newsId);
		NewsTO actualNews = newsDao.getNewsById(newsId);
		assertEquals(expectedNews, actualNews);	
	}
	
	@Test
	public void deleteNewsListByIdsTest() throws DaoException {
		NewsTO expectedNews = new NewsTO();
		newsDao.deleteNewsListByIds(new ArrayList<Long>(Arrays.asList(1L)));
		NewsTO actualNews = newsDao.getNewsById(1L);
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void getNewsByIdTest() throws DaoException {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setNewsId(1L);
		expectedNews.setTitle("firstNews");
		expectedNews.setShortText("short");
		expectedNews.setFullText("full");
		expectedNews.setCreationDate(Date.valueOf("2010-02-15"));
		expectedNews.setModificationDate(Date.valueOf("2010-02-15"));
		NewsTO actualNews = newsDao.getNewsById(expectedNews.getNewsId());
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void editNewsTest() throws DaoException {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setNewsId(1L);
		expectedNews.setTitle("firstNewsEdit");
		expectedNews.setShortText("shortEdit");
		expectedNews.setFullText("fullEdit");
		expectedNews.setCreationDate(Date.valueOf("2010-02-15"));
		expectedNews.setModificationDate(Date.valueOf("2010-02-15"));
		newsDao.editNews(expectedNews);
		NewsTO actualNews = newsDao.getNewsById(expectedNews.getNewsId());
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void countNewsTest() throws DaoException {
		Long expectedCount = 1L;
		Long actualCount = newsDao.countNews(new SearchCriteria());
		assertEquals(expectedCount, actualCount);
	}
	
	@Test
	public void getNextAndPreviousTest() throws DaoException {
		Collection<Long> actualIds = newsDao.getNextAndPrevious(1L, new SearchCriteria()).values();
		assertTrue(actualIds.containsAll(Arrays.asList(new Long[] {null, null})));
	}
}
