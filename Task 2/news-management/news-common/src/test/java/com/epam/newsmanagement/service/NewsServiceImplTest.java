package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {
	
	@Mock
	private NewsServiceImpl newsService;

	@Test
	public void addNewsTest() throws ServiceException {
		Long expectedId = 1L;
		NewsTO news = new NewsTO();
		Mockito.when(newsService.addNews(news)).thenReturn(expectedId);
		Long actualId = newsService.addNews(news);
		Mockito.verify(newsService, Mockito.times(1)).addNews(news);
		assertEquals(expectedId,actualId);
	}
	
	@Test
	public void editNewsTest() throws ServiceException {
		NewsTO news = new NewsTO();
		newsService.editNews(news);
		Mockito.verify(newsService, Mockito.times(1)).editNews(news);
	}
	
	@Test
	public void getNewsByIdTest() throws ServiceException {
		NewsTO expectedNews = new NewsTO();
		Mockito.when(newsService.getNewsById(1L)).thenReturn(expectedNews);
		NewsTO actualNews = newsService.getNewsById(1L);
		Mockito.verify(newsService, Mockito.times(1)).getNewsById(1L);
		assertEquals(expectedNews,actualNews);
	}
	
	public void deleteNewsListByIdsTest() throws ServiceException {
		List<Long> list = new ArrayList<Long>(Arrays.asList(1L));
		newsService.deleteNewsListByIds(list);
		Mockito.verify(newsService, Mockito.times(1)).deleteNewsListByIds(list);
	}
	
	@Test
	public void countNewsTest() throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		newsService.countNews(criteria);
		Mockito.verify(newsService, Mockito.times(1)).countNews(criteria);
	}
	
	@Test
	public void getNewsTest() throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		newsService.getNews(criteria, 1l,1l);
		Mockito.verify(newsService, Mockito.times(1)).getNews(criteria,1l,1l);
	}
	
	@Test
	public void getNextAndPreviousTest() throws ServiceException {
		Map<String,Long> expectedMap = new HashMap<String, Long>();
		SearchCriteria criteria = new SearchCriteria();
		Mockito.when(newsService.getNextAndPrevious(1L, criteria)).thenReturn(expectedMap);
		Map<String, Long> actualMap = newsService.getNextAndPrevious(1L, criteria);
		Mockito.verify(newsService, Mockito.times(1)).getNextAndPrevious(1L, criteria);
		assertEquals(expectedMap,actualMap);
	}
	
}
