package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.CommentDaoImpl;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:testContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("dataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class CommentDaoImplTest {

	@Autowired
	private CommentDaoImpl commentDao;
	
	@Test
	public void addCommentTest() throws DaoException {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setCommentText("testComment");
		expectedComment.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		expectedComment.setNewsId(1L);
		Long commentId = commentDao.addComment(expectedComment);
		expectedComment.setCommentId(commentId);
		CommentTO actualComment = commentDao.getCommentById(commentId);
		assertEquals(expectedComment,actualComment);
	}
	
	@Test
	public void deleteCommentByIdTest() throws DaoException {
		commentDao.deleteCommentById(1L);
		CommentTO expectedComment = new CommentTO();
		CommentTO actualComment = commentDao.getCommentById(1L);
		assertEquals(expectedComment, actualComment);
	}
	
	@Test
	public void deleteAllCommentsByNewsListTest() throws DaoException {
		commentDao.deleteAllCommentsByNewsList(new ArrayList<Long>(Arrays.asList(new Long[]{1L})));
		List<CommentTO> expectedList = new ArrayList<CommentTO>();
		List<CommentTO> actualList = commentDao.getCommentListByNewsId(1L);
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void editCommentTest() throws DaoException {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setCommentId(1L);
		expectedComment.setCommentText("editComment");
		expectedComment.setNewsId(1L);
		expectedComment.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		commentDao.editComment(expectedComment);
		CommentTO actualComment = commentDao.getCommentById(1L);
		assertEquals(expectedComment, actualComment);
	}
	
	@Test
	public void getCommentByIdTest() throws DaoException {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setCommentId(1L);
		expectedComment.setNewsId(1L);
		expectedComment.setCommentText("firstComment");
		expectedComment.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		CommentTO actualComment = commentDao.getCommentById(1L);
		assertEquals(expectedComment,actualComment);
	}
	
	@Test
	public void getCommentListByNewsIdTest() throws DaoException {
		CommentTO comment = new CommentTO();
		comment.setCommentId(1L);
		comment.setNewsId(1L);
		comment.setCommentText("firstComment");
		comment.setCreationDate(Timestamp.valueOf("2010-02-15 19:00:00"));
		List<CommentTO> expectedList = new ArrayList<CommentTO>(Arrays.asList(new CommentTO[] {comment}));
		List<CommentTO> actualList = commentDao.getCommentListByNewsId(1L);
		assertEquals(expectedList, actualList);
	} 
	
	
	
	
	
	

}
