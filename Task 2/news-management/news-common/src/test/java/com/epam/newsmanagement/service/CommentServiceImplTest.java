package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {
	
	@Mock
	private CommentServiceImpl commentService;

	@Test
	public void addCommentTest() throws ServiceException {
		CommentTO comment = new CommentTO();
		Long expectedId = 1L;
		Mockito.when(commentService.addComment(comment)).thenReturn(expectedId);
		Long actualId = commentService.addComment(comment);
		Mockito.verify(commentService, Mockito.times(1)).addComment(comment);
		assertEquals(expectedId,actualId);
	}
	
	@Test
	public void deleteCommentByIdTest() throws ServiceException {
		commentService.deleteCommentById(1L);
		Mockito.verify(commentService, Mockito.times(1)).deleteCommentById(1L);
	}
	
	@Test
	public void deleteAllCommentsByNewsListTest() throws ServiceException {
		List<Long> newsList = new ArrayList<Long>(Arrays.asList(1L));
		commentService.deleteAllCommentsByNewsList(newsList);
		Mockito.verify(commentService, Mockito.times(1)).deleteAllCommentsByNewsList(newsList);
	}
	
	@Test
	public void getCommentsByNewsIdTest() throws ServiceException {
		List<CommentTO> expectedList = new ArrayList<CommentTO>();
		Mockito.when(commentService.getCommentsByNewsId(1L)).thenReturn(expectedList);
		List<CommentTO> actualList = commentService.getCommentsByNewsId(1L);
		Mockito.verify(commentService, Mockito.times(1)).getCommentsByNewsId(1L);
		assertEquals(expectedList,actualList);
	}
	
}
