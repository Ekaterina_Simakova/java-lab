package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.TagDaoImpl;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:testContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("dataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class TagDaoImplTest {

	@Autowired
	private TagDaoImpl tagDao;

	@Test
	public void addTagTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagName("tag");
		Long tagId = tagDao.addTag(expectedTag);
		expectedTag.setTagId(tagId);
		TagTO actualTag = tagDao.getTagById(tagId);
		assertEquals(expectedTag,actualTag);
	}
	
	@Test
	public void deleteTagByIdTest() throws DaoException {
		tagDao.deleteTagById(2L);
		TagTO expectedTag = new TagTO();
		TagTO actualTag = tagDao.getTagById(2L);
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void editTagTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagId(1L);
		expectedTag.setTagName("editTag");
		tagDao.editTag(expectedTag);
		TagTO actualTag = tagDao.getTagById(expectedTag.getTagId());
		assertEquals(expectedTag,actualTag);
	}
	
	@Test
	public void getTagByIdTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagId(1L);
		expectedTag.setTagName("firstTag");
		TagTO actualTag = tagDao.getTagById(1L);
		assertEquals(expectedTag,actualTag);
	}
	
	@Test
	public void attachTagListToNewsByIdTest() throws DaoException {
		TagTO expectedTag = tagDao.getTagById(2L); 
		List<Long> idTagList = new ArrayList<Long>(Arrays.asList(2L));
		tagDao.attachTagListToNewsById(idTagList, 1L);
		List<TagTO> actualList = tagDao.getTagListByNewsId(1L);
		assertTrue(actualList.contains(expectedTag));
	}
	
	@Test
	public void detachTagByIdTest() throws DaoException {
		tagDao.detachTagById(1L);
		TagTO deletedTag = tagDao.getTagById(1L);
		List<TagTO> actualList = tagDao.getTagListByNewsId(1L);
		assertTrue(!actualList.contains(deletedTag));		
	}
	
	@Test
	public void detachAllTagsFromNewsTest() throws DaoException {
		tagDao.detachAllTagsFromNews(1L);
		List<TagTO> expectedList = new ArrayList<TagTO>();
		List<TagTO> actualList = tagDao.getTagListByNewsId(1L);
		assertEquals(expectedList,actualList);
	}

}
