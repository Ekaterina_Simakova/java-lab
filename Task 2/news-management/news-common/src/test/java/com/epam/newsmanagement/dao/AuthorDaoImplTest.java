package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.impl.AuthorDaoImpl;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:testContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("dataSet.xml")
@DatabaseTearDown(value="emptyDataSet.xml",type=DatabaseOperation.DELETE_ALL)
public class AuthorDaoImplTest {

	@Autowired
	private AuthorDaoImpl authorDao;

	@Test
	public void addAuthorTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorName("testAuthor");
		Long authorId = authorDao.addAuthor(expectedAuthor);
		expectedAuthor.setAuthorId(authorId);
		AuthorTO actualAuthor = authorDao.getAuthorById(authorId);
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void expireAuthorByIdTest() throws DaoException {
		authorDao.expireAuthorById(1L);
		AuthorTO expectedAuthor = authorDao.getAuthorById(1L);
		assertTrue(expectedAuthor.getExpired() != null);
	}

	@Test
	public void editAuthorTest() throws DaoException {
		AuthorTO expectedAuthor = authorDao.getAuthorById(1L);
		expectedAuthor.setAuthorName("editName");
		authorDao.editAuthor(expectedAuthor);
		AuthorTO actualAuthor = authorDao.getAuthorById(expectedAuthor.getAuthorId());
		assertEquals(expectedAuthor, actualAuthor);
	}

	@Test
	public void getAuthorByIdTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("first");
		AuthorTO actualAuthor = authorDao.getAuthorById(expectedAuthor.getAuthorId());
		assertEquals(expectedAuthor, actualAuthor);
	}

	@Test
	public void attachAuthorToNewsByIdTest() throws DaoException {
		authorDao.attachAuthorToNewsById(1L, 1L);
	}

	@Test
	public void detachAuthorFromNewsByIdTest() throws DaoException {
		authorDao.detachAuthorFromNewsById(1L);
		AuthorTO expectedAuthor = new AuthorTO();
		AuthorTO actualAuthor = authorDao.getAuthorByNewsId(1L);
		assertEquals(expectedAuthor, actualAuthor);
	}

	@Test
	public void getAuthorByNewsIdTest() throws DaoException {
		Long expectedId = 1L;
		AuthorTO actualAuthor = authorDao.getAuthorByNewsId(1L);
		assertEquals(expectedId, actualAuthor.getAuthorId());
	}

}
