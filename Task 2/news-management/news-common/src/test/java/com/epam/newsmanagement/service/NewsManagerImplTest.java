package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;
import com.epam.newsmanagement.service.impl.NewsManagerImpl;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagerImplTest {
	
	@Mock
	private NewsServiceImpl newsService;
	
	@Mock
	private AuthorServiceImpl authorService;
	
	@Mock
	private CommentServiceImpl commentService;
	
	@Mock
	private TagServiceImpl tagService;
	
	@InjectMocks
	private NewsManagerImpl newsManager;
	
	@Test(expected=ServiceException.class)
	public void addNewsTest() throws ServiceException {
		NewsTO news = new NewsTO();
		List<Long> tagList = new ArrayList<Long>();
		Long authorId = 1L;
		Mockito.doThrow(ServiceException.class).when(tagService).attachTagListToNews(Mockito.anyList(), Mockito.anyLong());
		Long expectedNewsId = newsManager.addNews(news,tagList,authorId);
		Mockito.verify(newsService, Mockito.times(1)).addNews(news);
		Mockito.verify(tagService, Mockito.times(1)).attachTagListToNews(tagList, Mockito.eq(expectedNewsId));
		Mockito.verify(authorService, Mockito.never()).attachAuthorToNewsById(authorId, expectedNewsId);
	}
	
	@Test
	public void editNewsTest() throws ServiceException {
		NewsTO news = new NewsTO();
		List<Long> tagList = new ArrayList<Long>();
		Long authorId = 1L;
		newsManager.editNews(news,tagList,authorId);
		Mockito.verify(newsService, Mockito.times(1)).editNews(news);
		Mockito.verify(tagService, Mockito.times(1)).detachTagListFromNews(news.getNewsId());
		Mockito.verify(tagService, Mockito.times(1)).attachTagListToNews(tagList, news.getNewsId());
		Mockito.verify(authorService, Mockito.times(1)).detachAuthorFromNewsByNewsId(news.getNewsId());
		Mockito.verify(authorService, Mockito.times(1)).attachAuthorToNewsById(authorId, news.getNewsId());
	}
	
	@Test(expected=ServiceException.class)
	public void deleteNewsListTest() throws ServiceException{
		List<Long> newsList = new ArrayList<Long>();
		Mockito.doThrow(new ServiceException("")).when(commentService).deleteAllCommentsByNewsList(newsList);
		newsManager.deleteNewsList(newsList);
		Mockito.verify(tagService, Mockito.times(1)).detachTagsFromNewsList(newsList);
		Mockito.verify(commentService, Mockito.times(1)).deleteAllCommentsByNewsList(newsList);
		Mockito.verify(authorService, Mockito.never()).detachAuthorsByNewsList(newsList);
		Mockito.verify(newsService, Mockito.never()).deleteNewsListByIds(newsList);
	}
	
	@Test
	public void getNewsTest() throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		int expectedSize = (newsManager.getNews(criteria, 1L, 10L).size());
		Mockito.verify(newsService, Mockito.times(1)).getNews(criteria, 1L, 10L);
		Mockito.verify(authorService, Mockito.times(expectedSize)).getAuthorByNewsId(Mockito.anyLong());
		Mockito.verify(commentService, Mockito.times(expectedSize)).getCommentsByNewsId(Mockito.anyLong());
		Mockito.verify(tagService, Mockito.times(expectedSize)).getTagListByNewsId(Mockito.anyLong());
	}
	
	@Test
	public void getNewsByIdTest() throws ServiceException {
		Long expectedId = 1L;
		NewsTO expectedNews = new NewsTO();
		expectedNews.setNewsId(expectedId);
		Mockito.when(newsService.getNewsById(expectedId)).thenReturn(expectedNews);
		newsManager.getNewsById(expectedId);
		Mockito.verify(newsService, Mockito.times(1)).getNewsById(expectedId);
		Mockito.verify(authorService, Mockito.times(1)).getAuthorByNewsId(expectedId);
		Mockito.verify(commentService, Mockito.times(1)).getCommentsByNewsId(expectedId);
		Mockito.verify(tagService, Mockito.times(1)).getTagListByNewsId(expectedId);
	}
	
	@Test
	public void countNewsTest() throws ServiceException {
		SearchCriteria criteria = new SearchCriteria();
		newsManager.countNews(criteria);
		Mockito.verify(newsService, Mockito.times(1)).countNews(criteria);
	}
	
	@Test
	public void addAuthorTest() throws ServiceException {
		AuthorTO author = new AuthorTO();
		newsManager.addAuthor(author);
		Mockito.verify(authorService, Mockito.times(1)).addAuthor(author);
	}
	
	@Test
	public void expireAuthorByIdTest() throws ServiceException {
		newsManager.expireAuthorById(1L);
		Mockito.verify(authorService, Mockito.times(1)).expireAuthorById(1L);
	}
	
	@Test
	public void addCommentTest() throws ServiceException {
		CommentTO comment = new CommentTO();
		newsManager.addComment(comment);
		Mockito.verify(commentService, Mockito.times(1)).addComment(comment);
	}
	
	@Test
	public void deleteCommentTest() throws ServiceException {
		newsManager.deleteComment(1L);
		Mockito.verify(commentService, Mockito.times(1)).deleteCommentById(1L);
	}
	
	@Test
	public void addTagTest() throws ServiceException {
		TagTO tag = new TagTO();
		newsManager.addTag(tag);
		Mockito.verify(tagService, Mockito.times(1)).addTag(tag);
	}
	
	public void editTag() throws ServiceException {
		
	}
	
}
