package com.epam.newsmanagement.entity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="newsCatalog")
public class NewsCatalog {
	
	private List<NewsTO> newsList;
	
	public NewsCatalog() {
		this.newsList = new ArrayList<NewsTO>();
	}

	public List<NewsTO> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<NewsTO> newsList) {
		this.newsList = newsList;
	}
	
	// @PostConstruct
	public void loadList() {
		/*URL url;
	    HttpURLConnection connection = null;  
	    try {
	      //Create connection
	      url = new URL("http://localhost:8080/serviceREST/newslist");
	      connection = (HttpURLConnection)url.openConnection();
	      connection.setRequestMethod("GET");
	      connection.setRequestProperty("count", "10");
	      connection.setRequestProperty("page", "1");
				
	      connection.setUseCaches (false);
	      connection.setDoInput(true);
	      connection.setDoOutput(true);

	      //Send request
	      DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());
	      // wr.writeBytes(urlParameters);
	      wr.flush ();
	      wr.close ();

	      //Get Response	
	      InputStream is = connection.getInputStream();
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	      String line;
	      StringBuffer response = new StringBuffer(); 
	      while((line = rd.readLine()) != null) {
	        response.append(line);
	        response.append('\r');
	      }
	      rd.close();
	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	    	
	    }
		/*ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target("http://localhost:8080/serviceREST/newslist");
        target.property("count", 10L);
		target.property("start", 1L);
        Response response = target.request().get();
        String value = response.readEntity(String.class);
        response.close();  // You should close connections!*/
		/*Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080/serviceREST/newslist");
		target.property("count", 10L);
		target.property("start", 1L);
        Response response = target.request().get();
        String value = response.readEntity(String.class);
        response.close();  // You should close connections!*/

	}
}
