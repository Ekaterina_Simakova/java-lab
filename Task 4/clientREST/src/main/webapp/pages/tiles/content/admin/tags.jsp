<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/fonts.css' />" />

<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<c:url var="validation_url" value="/tag/validation" />
<c:url value="/tags/page/${page}/update/${element.tagId}" var="update_action" />

<div class="height-without-pagination">
	<c:forEach var="element" items="${tagList}">
		<div class="row">
			<span class="col-xs-2"></span>
			<span class="col-xs-6 nopadding error" id="tag_${element.tagId}_tagNameError"></span>
		</div>
		<form method="POST" class="row notopmargin" id="tag_${element.tagId}" action="${update_action}">
			<span class="col-xs-2"><spring:message code="locale.tag" />:</span>
			<input name="tagName" type="text" disabled id="tag_${element.tagId}_tagName" value="${element.tagName}" class="col-xs-6"/>
			<span class="col-xs-4" id="tag_${element.tagId}_base_links">
				<a href="#" onclick="return enableForm('tag_${element.tagId}');">
					<spring:message code="locale.edit" />
				</a>
			</span>
			<span class="col-xs-4 hide" id="tag_${element.tagId}_addit_links">
				<a href="#" onclick="return updateTagName('${element.tagId}');">
					<spring:message code="locale.update" />
				</a>
				<c:url value="/tags/delete/${element.tagId}" var="delete_url" />
				<spring:message var="confirm_message" code="locale.confirm" />
				<a href="${delete_url}" onclick="return confirm('${confirm_message}');">
					<spring:message code="locale.delete" />
				</a>
				<a href="#" onclick="return disableForm('tag_${element.tagId}');" >
					<spring:message code="locale.cancel" />
				</a>
			</span>
		</form>
	</c:forEach>
	
	<c:url var="add" value="/tags/add" />
	<form:form modelAttribute="tag" method="post" action="${add}" class="row nopadding">
		<div class="row">
			<span class="col-xs-2" ></span>
			<span class="col-xs-6 nopadding error" id="tagName_error"></span>
		</div>
		<div class="row">
			<span class="col-xs-2"><spring:message code="locale.add_tag" /></span>
			<form:input path="tagName" class="col-xs-6"/>
			<span class="col-xs-4">
				<a href="#" onclick="return validAndSubmitForm('${validation_url}','tag');"><spring:message code="locale.add" /></a>
			</span>
		</div>
	</form:form>
</div>
<c:if test="${numberOfPages != 0 and numberOfPages <= paginationSize}">
	<div class="pagination-div">
			<div class="pagination">
				<c:forEach var="i" begin="1" end="${numberOfPages}">
					<c:url var="action" value="/tags/page/${i}" />
					<form action="${action}" class="pagination-form">
						<input type="submit" value="${i}" class="page-button" />
					</form>
				</c:forEach>
			</div>
		</div>
</c:if>
<c:if test="${numberOfPages > paginationSize}">
	<c:if test="${paginationSize%2 == 0}">
		<c:set var="half" value="${paginationSize/2 - 1}" />
	</c:if>
	<c:if test="${paginationSize%2 != 0}">
		<c:set var="half" value="${(paginationSize-1)/2}" />
	</c:if>
	<div class="pagination-div">
		<div class="pagination">
			<c:forEach var="i" begin="1" end="${half}">
				<c:url var="action" value="/tags/page/${i}" />
				<form action="${action}" class="pagination-form">
					<input type="submit" value="${i}" class="page-button" />
				</form>
			</c:forEach>
			<fmt:formatNumber type="number" groupingUsed="false" var="start" value="${half+1}"  maxFractionDigits="0" />
			<fmt:formatNumber type="number" groupingUsed="false" var="end" value="${numberOfPages-half}" maxFractionDigits="0" />
			<div class="pagination-form">
				<div class="empty-height" >
					<div id="pageList" onmouseover="showList(event, 'pageList');" onmouseout="hideList(event, 'pageList');"  class="hide">
						<c:forEach var="i" begin="${half+1}" end="${numberOfPages-half}">
							<span class="col-xs-12">
								<a href="<c:url value='/tags/page/${i}' /> ">${i}</a>
							</span>
						</c:forEach>
					</div>
				</div>
				<button onclick="return showList(event, 'pageList');" class="pagination-select">
					<span class="col-xs-10 nopadding pull-left">${start} - ${end}</span>
					<span class="col-xs-2 nopadding pull-right">&#9660</span>
				</button>
			</div>
			<c:forEach var="i" begin="${numberOfPages-half+1}" end="${numberOfPages}">
				<c:url var="action" value="/tags/page/${i}" />
				<form action="${action}" class="pagination-form">
					<input type="submit" value="${i}" class="page-button" />
				</form>
			</c:forEach>
		</div>
	</div>
</c:if>

