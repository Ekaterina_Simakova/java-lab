<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/fonts.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />

<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<c:url var="deletenews" value="/newslist/delete/${sessionScope.page}" />
<c:url var="filter" value="/newslist/page/1" />

<form:form modelAttribute="searchCriteria" action="${filter}" method="POST" class="criteriaLine">
	<span class="col-xs-1" ></span>
	<span class="col-xs-4">
		<span class="col-xs-12 nopadding">
			<span class="row">
				<button onclick="return showList(event, 'authorList');" class="select-button col-xs-12" >
					<span class="col-xs-11 nopadding pull-left">
						<spring:message code="locale.select_author" />
					</span>
					<span class="col-xs-1 nopadding text-right">&#9660</span>	
				</button>
			</span>
			<span class="row" onmouseover="showList(event, 'authorList');" onmouseout="hideList(event, 'authorList');">
				<span class="col-xs-12 hide" id="authorList">
					<form:radiobuttons path="authorId" items="${authorList}" itemValue="authorId" itemLabel="authorName" 
							element="span class='checkbox col-xs-12 nopadding pull-left text-left'"/>
				</span>
			</span>
		</span>
	</span>
	<span class="col-xs-4">
		<span class="col-xs-12 nopadding">
			<span class="row">
				<button onclick="return showList(event, 'tagList');" id="tagSelect" class="select-button col-xs-12" >
					<span class="col-xs-11 nopadding pull-left">
						<spring:message code="locale.select_tags" />
					</span>
					<span class="col-xs-1 nopadding text-right">&#9660</span>
				</button>
			</span>
			<span class="row" onmouseover="showList(event, 'tagList');" onmouseout="hideList(event, 'tagList');">
				<span class="col-xs-12 hide" id="tagList">  <!--  onclick="showTagList();"> -->
					<form:checkboxes path="tagIdList" items="${tagList}" itemLabel="tagName" itemValue="tagId"
							element="span class='checkbox col-xs-12 nopadding pull-left text-left'" />
				</span>
			</span>
		</span>
	</span>
	<span class="col-xs-3"> 
		<input type="submit" value="<spring:message code="locale.filter" />" /> 
		<a href="<c:url value='/newslist/reset' />">
			<spring:message code="locale.reset" />
		</a>
	</span> 
</form:form>
<div class="newslistbox">
	<form method="post" action="${deletenews}" id="newsList">
		<c:forEach var="news" items="${newsList}">
			<div class="listelement">
				<div class="row notopmargin">
					<a href="<c:url value='/view/${news.newsId}' />"
						class="col-xs-5 nopadding"> <font class="gappy">${news.title}</font>
					</a> 
					<span class="col-xs-4"> 
						<font class="gappy">(by	${news.author.authorName})</font>
					</span> 
					<span class="col-xs-2"> 
						<spring:message var="dateformat" code="locale.dateformat" /> 
						<fmt:formatDate pattern="${dateformat}" value="${news.modificationDate}" />
					</span> 
					<span class="col-xs-1"> 
						<input type="checkbox" name="newsId" value="${news.newsId}" />
					</span>
				</div>
				<div class="row notopmargin">
					<font class="gappy">${news.shortText}</font>
				</div>
				<p class="text-right">
					<c:forEach var="tag" items="${news.tagList}">
						<font id="tagsfont gappy">${tag.tagName},</font>
					</c:forEach>
					<font id="commentnumberfont"> 
						<spring:message code="locale.comments" />(${fn:length(news.commentList)})
					</font> 
					<a href="<c:url value='/news-form/${news.newsId}' /> "> 
						<spring:message code="locale.edit" />
					</a>
				</p>
			</div>
		</c:forEach>
		<spring:message var="message" code="locale.confirm" />
		<input type="submit" onclick="return confirmDeleting('${message}');" value="<spring:message code='locale.delete' /> " class="pull-right" />
	</form>
</div>
<c:if test="${numberOfPages != 0 and numberOfPages <= paginationSize}">
	<div class="pagination-div">
		<div class="pagination">
			<c:forEach var="i" begin="1" end="${numberOfPages}">
				<c:url var="action" value="/newslist/page/${i}" />
				<form action="${action}" class="pagination-form">
					<input type="submit" value="${i}" class="page-button" />
				</form>
			</c:forEach>
		</div>
	</div>
</c:if>
<c:if test="${numberOfPages > paginationSize}">
	<c:if test="${paginationSize%2 == 0}">
		<c:set var="half" value="${paginationSize/2 - 1}" />
	</c:if>
	<c:if test="${paginationSize%2 != 0}">
		<c:set var="half" value="${(paginationSize-1)/2}" />
	</c:if>
	<div class="pagination-div">
		<div class="pagination">
			<c:forEach var="i" begin="1" end="${half}">
				<c:url var="action" value="/newslist/page/${i}" />
				<form action="${action}" class="pagination-form">
					<input type="submit" value="${i}" class="page-button" />
				</form>
			</c:forEach>
			<fmt:formatNumber type="number" groupingUsed="false" var="start" value="${half+1}"  maxFractionDigits="0" />
			<fmt:formatNumber type="number" groupingUsed="false" var="end" value="${numberOfPages-half}" maxFractionDigits="0" />
			<div class="pagination-form">
				<div class="empty-height" >
					<div id="pageList" onmouseover="showList(event, 'pageList');" onmouseout="hideList(event, 'pageList');"  class="hide">
						<c:forEach var="i" begin="${half+1}" end="${numberOfPages-half}">
							<span class="col-xs-12">
								<a href="<c:url value='/newslist/page/${i}' /> ">${i}</a>
							</span>
						</c:forEach>
					</div>
				</div>
				<button onclick="return showList(event, 'pageList');" class="pagination-select">
					<span class="col-xs-10 nopadding pull-left">${start} - ${end}</span>
					<span class="col-xs-2 nopadding pull-right">&#9660</span>
				</button>
			</div>
			<c:forEach var="i" begin="${numberOfPages-half+1}" end="${numberOfPages}">
				<c:url var="action" value="/newslist/page/${i}" />
				<form action="${action}" class="pagination-form">
					<input type="submit" value="${i}" class="page-button" />
				</form>
			</c:forEach>
		</div>
	</div>
</c:if>
