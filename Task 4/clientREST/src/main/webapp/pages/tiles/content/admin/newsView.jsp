<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link rel="stylesheet"
	href="<c:url value='/resources/bootstrap/css/bootstrap.min.css' /> " />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/layout.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/forms.css' />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/fonts.css' />" />
	
<c:url value="/resources/jquery/jquery-1.11.3.min.js" var="jquery" />
<script src="${jquery}"></script>	
<c:url value="/resources/js/news-management.js" var="js" />
<script src="${js}"></script>

<c:url var="validation_url" value="/comment/validation" />
<c:if test="${not empty sessionScope.page}">
	<c:set var="page" value="${sessionScope.page}" />
</c:if>
<c:if test="${empty sessionScope.page}">
	<c:set var="page" value="1" />
</c:if>

<div class="row">
	<a href="<c:url value='/newslist/page/${page}' /> "><spring:message code="locale.back" /></a>
</div>
<div class="newsbox">
	<div class="row">
		<span class="col-xs-7 nopadding">
			<b class="gappy">${news.title}</b>
		</span>
		<span class="col-xs-3">
			<font class="gappy">(by ${news.author.authorName})</font>
		</span>
		<spring:message var="dateformat" code="locale.dateformat" />
		<span class="col-xs-2 nopadding text-right">
			<u><fmt:formatDate pattern="${dateformat}" value="${news.modificationDate}" /></u>
		</span>
	</div>
	<div class="row">
		<font class="gappy">${news.shortText}</font>
	</div>
	<div class="row">
		<font class="gappy">${news.fullText}</font> 
	</div>
</div>
<c:url var="addcomment" value="/view/addcomment" />
<div class="row">
	<span class="col-xs-6 nopadding comments">
		<c:forEach var="com" items="${news.commentList}">
			<u><fmt:formatDate pattern="${dateformat}" value="${com.creationDate}" /></u>
			<div class="row comment notopmargin">
				<span class="col-xs-10 nopadding">
					<font class="gappy">${com.commentText}</font>
				</span>
				<span class="col-xs-2 text-right nopadding">
					<spring:message var="message" code="locale.confirm" />
					<a href="<c:url value='/view/${com.newsId}/deletecomment/${com.commentId}' /> " onclick="return confirm('${message}');">
						<img src="<c:url value='/resources/images/delete.png' />" class="deleteicon" />
					</a>
				</span>
			</div>
		</c:forEach>
	</span>
</div>
<div class="row nopadding">
	<form:form method="POST" modelAttribute="comment" action="${addcomment}" class="col-xs-6 nopadding">
		<form:textarea path="commentText"/>
		<form:input path="newsId" type="hidden"/>
		<input type="submit" class="pull-right" onclick="return validAndSubmitForm('${validation_url}','comment');" value="<spring:message code='locale.add' />"/>
	</form:form>
	<div class="col-xs-5 notopmargin error" id="commentText_error"></div>
</div>
<div class="row">
	<span class="col-xs-6 text-left nopadding">
		<c:if test="${not empty previousId}" >
			<a href="<c:url value="/view/${previousId}" />" ><spring:message code="locale.previous"/></a>
		</c:if>
	</span>
	<span class="col-xs-6 text-right nopadding">
		<c:if test="${not empty nextId}">
			<a href="<c:url value="/view/${nextId}" />" ><spring:message code="locale.next"/></a>
		</c:if>
	</span>
</div>