package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class AuthorTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long authorId;

	private String authorName;

	private Timestamp expired;
	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AuthorTO other = (AuthorTO) obj;
		if(authorId != null) {
			if(!authorId.equals(other.getAuthorId())) {
				return false;
			}
		} else {
			if(other.getAuthorId() != null) {
				return false;
			}
		}
		if(authorName != null) {
			if(!authorName.equals(other.getAuthorName())) {
				return false;
			}
		} else {
			if(other.getAuthorName() != null) {
				return false;
			}
		}
		if(expired != null) {
			if(!expired.equals(other.getExpired())) {
				return false;
			}
		} else {
			if(other.getExpired() != null) {
				return false;
			}
		}
		return true;
	}
}
