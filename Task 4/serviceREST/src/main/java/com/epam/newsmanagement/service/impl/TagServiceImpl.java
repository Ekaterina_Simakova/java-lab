package com.epam.newsmanagement.service.impl;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.TagService;

@Path("/TagService")
public class TagServiceImpl implements TagService {

	@Autowired
	private NewsManager newsManager;
	
	@GET
	@Path("/tags")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TagTO> getTagsPart(@RequestParam(name="start") Long start, @RequestParam(name="count") Long count) {
		return newsManager.getTags(start, count);
	}

	@GET
	@Path("/tags")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TagTO> getAllTags() {
		return newsManager.getAllTags();
	}

	@PUT
	@Path("/tag")
	@Produces(MediaType.APPLICATION_JSON)
	public void updateTag(TagTO tag) {
		newsManager.editTag(tag);
	}

	@POST
	@Path("/tag")
	@Produces(MediaType.APPLICATION_JSON)
	public Long addTag(TagTO tag) {
		return newsManager.addTag(tag);
	}

	@DELETE
	@Path("/tag/{tagId}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteTag(@PathVariable Long tagId) {
		newsManager.deleteTagById(tagId);
	}

}
