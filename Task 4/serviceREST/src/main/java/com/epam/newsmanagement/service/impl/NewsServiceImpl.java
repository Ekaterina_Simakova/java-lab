package com.epam.newsmanagement.service.impl;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;

@Path("/TagService")
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	NewsManager newsManager;

	@GET
	@Path("/news")
	@Produces(MediaType.APPLICATION_JSON)
	public List<NewsVO> getNewsPart(@RequestParam(name="start") Long start, @RequestParam(name="count") Long count) {
		return newsManager.getNews(null, start, count);
	}

	public void updateNews(NewsVO news) {
		// TODO Auto-generated method stub

	}

	public Long addNews(NewsVO news) {
		// TODO Auto-generated method stub
		return null;
	}

	public void deleteNews(Long newsId) {
		// TODO Auto-generated method stub

	}

}
