package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.NewsVO;

public interface NewsService {
	
	List<NewsVO> getNewsPart(Long start, Long count);
	void updateNews(NewsVO news);
	Long addNews(NewsVO news);
	void deleteNews(Long newsId);

}
