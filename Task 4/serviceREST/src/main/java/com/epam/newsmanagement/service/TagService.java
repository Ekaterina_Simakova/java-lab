package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;

public interface TagService {
	
	List<TagTO> getTagsPart(Long start, Long count);
	List<TagTO> getAllTags();
	void updateTag(TagTO tag);
	Long addTag(TagTO tag);
	void deleteTag(Long tagId);
	
}
